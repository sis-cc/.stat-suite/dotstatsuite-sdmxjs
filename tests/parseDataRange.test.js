import { describe, expect, test } from 'vitest';
import { parseDataRange } from '../src/';

describe('parseDataRange tests', () => {
  test('basic test', () => {
    expect(parseDataRange({ headers: { 'content-range': 'values 0-999/10000' } })).toEqual({
      count: 1000,
      total: 10000,
    });
  });
  test('not a number test', () => {
    expect(parseDataRange({ headers: { 'content-range': 'values 0-ABACAB/10000' } })).toEqual({});
  });
  test('no range header', () => {
    expect(parseDataRange({ headers: {} })).toEqual({});
  });
  test('incorrect range header', () => {
    expect(parseDataRange({ headers: { 'content-range': 'random' } })).toEqual({});
  });
});
