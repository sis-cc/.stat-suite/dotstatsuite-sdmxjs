import { describe, expect, test } from 'vitest';
import { parseStubLists } from '../src';

describe('parseStubLists tests', () => {
  test('blank', () => {
    expect(parseStubLists({})).toEqual({});
  });
  test('one type test', () => {
    const sdmxJson = {
      data: {
        dataflows: [
          { id: 'DF1', version: 'V1', agencyID: 'A1', name: 'dataflow1', isFinal: false },
          {
            id: 'DF2',
            version: 'V2',
            agencyID: 'A2',
            isFinal: true,
            links: [
              { rel: 'whatever', urn: 'some urn' },
              { rel: 'self', urn: 'my.artefact.urn' },
            ],
          },
        ],
      },
    };
    expect(parseStubLists(sdmxJson)).toEqual({
      dataflow: [
        {
          agencyId: 'A1',
          annotations: [],
          sdmxId: 'A1:DF1(V1)',
          code: 'DF1',
          label: 'dataflow1',
          version: 'V1',
          isFinal: false,
          type: 'dataflow',
          links: null,
        },
        {
          agencyId: 'A2',
          annotations: [],
          sdmxId: 'A2:DF2(V2)',
          code: 'DF2',
          label: undefined,
          version: 'V2',
          isFinal: true,
          type: 'dataflow',
          links: [
            { rel: 'whatever', urn: 'some urn' },
            { rel: 'self', urn: 'my.artefact.urn' },
          ],
        },
      ],
    });
  });
});
