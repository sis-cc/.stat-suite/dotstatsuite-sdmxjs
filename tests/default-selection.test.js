import { describe, expect, test } from 'vitest';
import { getDefaultSelection } from '../src';

describe('default selection', () => {
  test('simple case', () => {
    const structure = {
      data: {
        dataStructures: [
          {
            dataStructureComponents: {
              dimensionList: {
                dimensions: [
                  { id: 'ALPHA' },
                  { id: 'GAMMA' },
                  { id: 'PHI' },
                  { id: 'EPSILON' },
                  { id: 'TAU' },
                ],
              },
            },
          },
        ],
        dataflows: [
          {
            annotations: [
              {
                type: 'DEFAULT',
                title: 'TAU=TAU_1+TAU_3,PHI=PHI_1,TIME_PERIOD_START=2013-01',
              },
            ],
          },
        ],
        contentConstraints: [
          {
            type: 'Actual',
            cubeRegions: [
              {
                isIncluded: true,
                keyValues: [
                  {
                    id: 'PHI',
                    values: ['PHI_1'],
                  },
                  {
                    id: 'TAU',
                    values: ['TAU_1', 'TAU_3'],
                  },
                ],
              },
            ],
          },
        ],
      },
    };
    expect(getDefaultSelection(structure)).toEqual({ PHI: ['PHI_1'], TAU: ['TAU_1', 'TAU_3'] });
  });
});
