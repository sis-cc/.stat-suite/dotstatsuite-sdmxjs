import { describe, expect, test } from 'vitest';
import { parseDataParams } from '../src/parseDataParams';

describe('dataflow', () => {
  test('simple case', () => {
    const params = {
      random: 'random',
      dimensionAtObservation: 'AllDimensions',
      startPeriod: 'period',
      lastNObservations: '0',
      lastNPeriods: '5',
    };

    const expected = {
      dimensionAtObservation: 'AllDimensions',
      startPeriod: 'period',
      lastNPeriods: '5',
    };
    expect(parseDataParams(params)).toEqual(expected);
  });
});
