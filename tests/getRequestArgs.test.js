import { describe, expect, test } from 'vitest';
import { getRequestArgs } from '../src';

describe('getRequestArgs tests', () => {
  test('null lastNObs', () => {
    const params = { lastNObservations: '0' };
    const args = {
      datasource: { url: 'a' },
      dataquery: 'e',
      identifiers: { agencyId: 'b', code: 'c', version: 'd' },
      locale: 'f',
      params,
      type: 'data',
    };
    expect(getRequestArgs(args)).toEqual({
      url: 'a/data/b,c,d/e',
      params: { dimensionAtObservation: 'AllDimensions' },
      headers: {
        Accept: 'application/vnd.sdmx.data+json;version=1.0.0-wd;urn=true',
        'Accept-Language': 'f',
      },
    });
  });
  test('range header test', () => {
    const args = {
      datasource: { url: 'endpoint' },
      identifiers: { agencyId: 'A', code: 'C', version: 'V' },
      locale: 'L',
      type: 'data',
      range: 25,
    };

    const expected = {
      url: 'endpoint/data/A,C,V/all',
      params: { dimensionAtObservation: 'AllDimensions' },
      headers: {
        Accept: 'application/vnd.sdmx.data+json;version=1.0.0-wd;urn=true',
        'Accept-Language': 'L',
        'x-range': 'values=0-24',
      },
    };

    expect(getRequestArgs(args)).toEqual(expected);
  });
  test('parents references', () => {
    expect(
      getRequestArgs({
        datasource: { supportsReferencePartial: true, url: 'endpoint' },
        identifiers: { agencyId: 'A', code: 'DF', version: '1' },
        type: 'dataflow',
        withParents: true,
        overview: true,
        locale: 'l',
      }),
    ).toEqual({
      url: 'endpoint/dataflow/A/DF/1',
      headers: {
        Accept: 'application/vnd.sdmx.structure+json;version=1.0;urn=true',
        'Accept-Language': 'l',
      },
      params: { references: 'parents', detail: 'allcompletestubs' },
    });
  });
  test('parentsandsiblings references', () => {
    expect(
      getRequestArgs({
        datasource: { supportsReferencePartial: true, url: 'endpoint' },
        identifiers: { agencyId: 'A', code: 'DF', version: '1' },
        type: 'dataflow',
        withParentsAndSiblings: true,
        locale: 'l',
      }),
    ).toEqual({
      url: 'endpoint/dataflow/A/DF/1',
      headers: {
        Accept: 'application/vnd.sdmx.structure+json;version=1.0;urn=true',
        'Accept-Language': 'l',
      },
      params: { references: 'parentsandsiblings' },
    });
  });

  test('children references', () => {
    expect(
      getRequestArgs({
        datasource: { supportsReferencePartial: true, url: 'endpoint' },
        identifiers: { agencyId: 'A', code: 'DF', version: '1' },
        type: 'dataflow',
        withChildren: true,
        locale: 'l',
      }),
    ).toEqual({
      url: 'endpoint/dataflow/A/DF/1',
      headers: {
        Accept: 'application/vnd.sdmx.structure+json;version=1.0;urn=true',
        'Accept-Language': 'l',
      },
      params: { references: 'children' },
    });
  });
  test('reference partial request', () => {
    expect(
      getRequestArgs({
        datasource: { url: 'endpoint', supportsReferencePartial: true },
        identifiers: { agencyId: 'A', code: 'DF', version: '1' },
        type: 'dataflow',
        locale: 'l',
        withPartialReferences: true,
      }),
    ).toEqual({
      url: 'endpoint/dataflow/A/DF/1',
      headers: {
        Accept: 'application/vnd.sdmx.structure+json;version=1.0;urn=true',
        'Accept-Language': 'l',
      },
      params: { references: 'all', detail: 'referencepartial' },
    });
  });
  test('zip request', () => {
    expect(
      getRequestArgs({
        datasource: { url: 'endpoint' },
        identifiers: { agencyId: 'A', code: 'DF', version: '1' },
        type: 'dataflow',
        locale: 'l',
        asZip: true,
      }),
    ).toEqual({
      url: 'endpoint/dataflow/A/DF/1',
      headers: {
        Accept: 'application/vnd.sdmx.structure+json;version=1.0;urn=true;zip=true',
        'Accept-Language': 'l',
      },
      params: { references: 'all' },
    });
  });
  test('metadata tech xml data request', () => {
    expect(
      getRequestArgs({
        datasource: { url: 'endpoint', headers: { data: { xml: 'customHeaderFormat' } } },
        identifiers: { agencyId: 'A', code: 'DF', version: '1' },
        type: 'data',
        locale: 'l',
        format: 'xml',
      }),
    ).toEqual({
      url: 'endpoint/data/A,DF,1/all',
      headers: {
        Accept: `customHeaderFormat;urn=true`,
        'Accept-Language': 'l',
      },
      params: { dimensionAtObservation: 'AllDimensions' },
    });
  });
  test('no artefact version in structure request', () => {
    expect(
      getRequestArgs({
        datasource: { url: 'endpoint' },
        identifiers: { agencyId: 'A', code: 'DF' },
        type: 'dataflow',
        locale: 'en',
      }),
    ).toEqual({
      url: 'endpoint/dataflow/A/DF/',
      headers: {
        Accept: 'application/vnd.sdmx.structure+json;version=1.0;urn=true',
        'Accept-Language': 'en',
      },
      params: { references: 'all' },
    });
  });
  test('no dataflow version in data request', () => {
    expect(
      getRequestArgs({
        datasource: { url: 'endpoint' },
        identifiers: { agencyId: 'A', code: 'DF' },
        type: 'data',
        locale: 'en',
      }),
    ).toEqual({
      url: 'endpoint/data/A,DF,/all',
      headers: {
        Accept: 'application/vnd.sdmx.data+json;version=1.0.0-wd;urn=true',
        'Accept-Language': 'en',
      },
      params: { dimensionAtObservation: 'AllDimensions' },
    });
  });
});
