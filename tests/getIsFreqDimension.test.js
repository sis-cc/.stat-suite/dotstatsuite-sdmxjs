import { describe, expect, test } from 'vitest';
import { isFrequencyDimension } from '../src/';

describe('getIsFreqDimension', () => {
  test('complete case', () => {
    expect(isFrequencyDimension({ id: 'FREQ' })).toEqual(true);
    expect(isFrequencyDimension({ id: 'TOTO' })).toEqual(false);
  });
});
