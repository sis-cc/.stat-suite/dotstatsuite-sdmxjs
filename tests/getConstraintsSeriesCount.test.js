import { describe, expect, test } from 'vitest';
import { getConstraintsSeriesCount } from '../src';

describe('getConstraintsSeriesCount tests', () => {
  test('basic test', () => {
    const sdmxJson = {
      data: {
        contentConstraints: [
          {
            annotations: [{ id: 'series_count', type: 'sdmx_metrics', title: '50' }],
            type: 'Actual',
          },
        ],
      },
    };
    expect(getConstraintsSeriesCount(sdmxJson)).toEqual(50);
  });
  test('no constraint annotations test', () => {
    const sdmxJson = {
      data: {
        contentConstraints: [
          {
            annotations: [],
            type: 'Actual',
          },
        ],
      },
    };
    expect(getConstraintsSeriesCount(sdmxJson)).toEqual(undefined);
  });
  test('no series_count annotation test', () => {
    const sdmxJson = {
      data: {
        contentConstraints: [
          {
            annotations: [{ id: 'obs_count', type: 'sdmx_metrics', title: '50' }],
            type: 'Actual',
          },
        ],
      },
    };
    expect(getConstraintsSeriesCount(sdmxJson)).toEqual(undefined);
  });
  test('validity date test', () => {
    const sdmxJson = {
      data: {
        contentConstraints: [
          {
            annotations: [{ id: 'series_count', type: 'sdmx_metrics', title: '0' }],
            validTo: '1950-01-01T00:00:00',
            type: 'Actual',
          },
          {
            annotations: [{ id: 'series_count', type: 'sdmx_metrics', title: '10' }],
            validFrom: '1950-01-01T00:00:00',
            validTo: '1975-01-01T00:00:00',
            type: 'Actual',
          },
          {
            annotations: [{ id: 'series_count', type: 'sdmx_metrics', title: '20' }],
            validFrom: '1975-01-01T00:00:00',
            validTo: '2050-01-01T00:00:00',
            type: 'Actual',
          },
          {
            annotations: [{ id: 'series_count', type: 'sdmx_metrics', title: '30' }],
            validFrom: '2050-01-01T00:00:00',
            type: 'Actual',
          },
        ],
      },
    };
    expect(getConstraintsSeriesCount(sdmxJson)).toEqual(20);
  });
});
