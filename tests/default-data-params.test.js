import { describe, expect, test } from 'vitest';
import { getDefaultDataParams } from '../src';

describe('dataflow', () => {
  test('simple case', () => {
    const structure = {
      data: {
        dataflows: [
          {
            annotations: [
              { type: 'RANDOM' },
              {
                type: 'DEFAULT',
                title:
                  'DIM3=CODE1+CODE2,DIM6=CODE,TIME_PERIOD_START=2013-01,TIME_PERIOD_END=2018-12,LASTNOBSERVATIONS,LASTNPERIODS=5',
              },
              { type: 'DEFAULT', title: 'IGNORED=IGNORED' },
            ],
          },
        ],
      },
    };
    const expected = {
      startPeriod: '2013-01',
      endPeriod: '2018-12',
      lastNObservations: '1',
      lastNPeriods: '5',
    };
    expect(getDefaultDataParams(structure)).toEqual(expected);
  });
});
