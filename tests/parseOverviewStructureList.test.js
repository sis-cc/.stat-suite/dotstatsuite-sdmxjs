import { describe, expect, test } from 'vitest';
import { parseOverviewStructureList } from '../src';

describe('parseOverviewStructureList tests', () => {
  test('one type to parse case', () => {
    const sdmxJson = {
      data: {
        dataflows: [
          { id: 'DF1', version: 'V1', agencyID: 'A1', name: 'dataflow1', isFinal: false },
          { id: 'DF2', version: 'V2', agencyID: 'A2', isFinal: true },
        ],
      },
    };

    const parseOptions = { space: { id: 'sp' }, type: 'dataflow' };

    const expected = {
      'sp:dataflow:A1:DF1(V1)': {
        code: 'DF1',
        label: 'dataflow1',
        isFinal: false,
        agencyId: 'A1',
        sdmxId: 'A1:DF1(V1)',
        space: { id: 'sp' },
        id: 'sp:dataflow:A1:DF1(V1)',
        type: 'dataflow',
        version: 'V1',
      },
      'sp:dataflow:A2:DF2(V2)': {
        code: 'DF2',
        label: undefined,
        isFinal: true,
        agencyId: 'A2',
        sdmxId: 'A2:DF2(V2)',
        space: { id: 'sp' },
        id: 'sp:dataflow:A2:DF2(V2)',
        type: 'dataflow',
        version: 'V2',
      },
    };

    expect(parseOverviewStructureList(parseOptions)(sdmxJson)).toEqual(expected);
  });
  test('filter regarding agencies', () => {
    const sdmxJson = {
      data: {
        dataflows: [
          { id: 'DF1', version: 'V1', agencyID: 'A1', name: 'dataflow1', isFinal: true },
          { id: 'DF2', version: 'V2', agencyID: 'A2', name: 'dataflow2', isFinal: true },
          { id: 'DF3', version: 'V3', agencyID: 'A3', name: 'dataflow3', isFinal: true },
          { id: 'DF4', version: 'V4', agencyID: 'A4', name: 'dataflow4', isFinal: true },
          { id: 'DF5', version: 'V5', agencyID: 'A5', name: 'dataflow5', isFinal: true },
        ],
      },
      meta: { contentLanguages: ['l'] },
    };

    const parseOptions = {
      space: { id: 'sp' },
      type: 'dataflow',
      fromCategory: true,
      agencyIds: ['A2', 'A4'],
    };

    const expected = {
      'sp:dataflow:A2:DF2(V2)': {
        code: 'DF2',
        label: 'dataflow2',
        isFinal: true,
        agencyId: 'A2',
        sdmxId: 'A2:DF2(V2)',
        space: { id: 'sp' },
        id: 'sp:dataflow:A2:DF2(V2)',
        type: 'dataflow',
        version: 'V2',
      },
      'sp:dataflow:A4:DF4(V4)': {
        code: 'DF4',
        label: 'dataflow4',
        isFinal: true,
        agencyId: 'A4',
        sdmxId: 'A4:DF4(V4)',
        space: { id: 'sp' },
        id: 'sp:dataflow:A4:DF4(V4)',
        type: 'dataflow',
        version: 'V4',
      },
    };

    expect(parseOverviewStructureList(parseOptions)(sdmxJson)).toEqual(expected);
  });
});
