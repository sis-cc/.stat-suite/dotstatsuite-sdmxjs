import { describe, expect, test } from 'vitest';
import { constrainDimensions } from '../src/constrainDimensions';

describe('constrainDimensions tests', () => {
  test('complete case', () => {
    const dimensions = [
      { id: 'd0', values: [{ id: 'v0' }, { id: 'v1' }, { id: 'v2' }] },
      { id: 'd1', values: [{ id: 'v0' }, { id: 'v1' }, { id: 'v2' }] },
      { id: 'd2', values: [{ id: 'v0' }, { id: 'v1' }, { id: 'v2' }] },
    ];

    const constraints = {
      d0: ['v0', 'v2'],
      d1: [],
      d4: ['v0', 'v1', 'v2'],
    };

    expect(constrainDimensions(dimensions, constraints)).toEqual([
      {
        id: 'd0',
        hasData: true,
        values: [
          { id: 'v0', hasData: true },
          { id: 'v1', hasData: false },
          { id: 'v2', hasData: true },
        ],
      },
      {
        id: 'd1',
        hasData: false,
        values: [
          { id: 'v0', hasData: false },
          { id: 'v1', hasData: false },
          { id: 'v2', hasData: false },
        ],
      },
      {
        id: 'd2',
        hasData: true,
        values: [
          { id: 'v0', hasData: true },
          { id: 'v1', hasData: true },
          { id: 'v2', hasData: true },
        ],
      },
    ]);
  });
});
