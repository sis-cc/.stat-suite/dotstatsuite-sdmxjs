import { describe, expect, test } from 'vitest';
import * as R from 'ramda';
import { getDimensions } from '../src/';

describe('getDimensions', () => {
  test('complete case', () => {
    const structure = {
      data: {
        codelists: [
          {
            links: [{ rel: 'self', urn: 'CL_1' }],
            description: 'CL_1',
            codes: [
              { id: '1.1', name: '1v1', description: '1v1' },
              { id: '1.2', name: '1v2', parent: '1.1' },
              { id: '1.3', name: '1v3' },
              { id: '1.4', name: '1v4' },
            ],
          },
          {
            links: [{ rel: 'self', urn: 'CL_2' }],
            codes: [
              { id: '2.1', annotations: [{ text: '1', type: 'ORDER' }] },
              { id: '2.2', annotations: [{ text: '0', type: 'ORDER' }] },
              { id: '2.3', annotations: [{ text: '3', type: 'ORDER' }] },
              { id: '2.4', annotations: [{ text: '2', type: 'ORDER' }] },
            ],
          },
          {
            links: [{ rel: 'self', urn: 'CL_3' }],
            codes: [{ id: '3.1' }, { id: '3.2' }, { id: '3.3' }, { id: '3.4' }],
          },
          {
            links: [{ rel: 'self', urn: 'CL_4' }],
            annottations: [{ id: 'NOT_DISPLAYED' }],
            codes: [{ id: '4.1' }, { id: '4.2' }, { id: '4.3' }, { id: '4.4' }],
          },
          {
            links: [{ rel: 'self', urn: 'CL_5' }],
            annotations: [{ type: 'NOT_DISPLAYED' }, { type: 'test' }],
            codes: [{ id: '5.1' }, { id: '5.2' }, { id: '5.3' }, { id: '5.4' }],
          },
          {
            links: [{ rel: 'self', urn: 'CL_6' }],
            codes: [
              { id: '6.1' },
              { id: '6.2', annotations: [{ type: 'NOT_DISPLAYED' }] },
              { id: '6.3' },
              { id: '6.4' },
            ],
          },
          {
            links: [{ rel: 'self', urn: 'CL_7' }],
            codes: [
              { id: '7.1', name: '7v1' },
              { id: '7.2', name: '7v2' },
              { id: '7.3', name: '7v3' },
              { id: '7.4', name: '7v4' },
            ],
          },
        ],
        conceptSchemes: [
          { concepts: [{ name: 'dim1', links: [{ rel: 'self', urn: 'CPT_1' }] }] },
          { concepts: [{ links: [{ rel: 'self', urn: 'CPT_2' }] }] },
          { concepts: [{ links: [{ rel: 'self', urn: 'CPT_3' }] }] },
          { concepts: [{ links: [{ rel: 'self', urn: 'CPT_4' }] }] },
          { concepts: [{ links: [{ rel: 'self', urn: 'CPT_5' }] }] },
          { concepts: [{ links: [{ rel: 'self', urn: 'CPT_6' }] }] },
          { concepts: [{ name: 'dim7', links: [{ rel: 'self', urn: 'CPT_7' }] }] },
        ],
        dataflows: [
          { annotations: [{ type: 'NOT_DISPLAYED', title: 'D1,D2=2.1+2.2,D3=,D4=4.3' }] },
        ],
        dataStructures: [
          {
            dataStructureComponents: {
              dimensionList: {
                dimensions: [
                  {
                    id: 'D1',
                    conceptIdentity: 'CPT_1',
                    localRepresentation: { enumeration: 'CL_1' },
                  },
                  {
                    id: 'D2',
                    conceptIdentity: 'CPT_2',
                    conceptRoles: ['ROLE'],
                    localRepresentation: { enumeration: 'CL_2' },
                  },
                  {
                    id: 'D3',
                    conceptIdentity: 'CPT_3',
                    localRepresentation: { enumeration: 'CL_3' },
                  },
                  {
                    id: 'D4',
                    conceptIdentity: 'CPT_4',
                    localRepresentation: { enumeration: 'CL_4' },
                  },
                  {
                    id: 'D5',
                    conceptIdentity: 'CPT_5',
                    localRepresentation: { enumeration: 'CL_5' },
                  },
                  {
                    id: 'D6',
                    conceptIdentity: 'CPT_6',
                    localRepresentation: { enumeration: 'CL_6' },
                  },
                  {
                    id: 'D7',
                    annotations: [{ type: 'NOT_DISPLAYED' }],
                    conceptIdentity: 'CPT_7',
                    localRepresentation: { enumeration: 'CL_7' },
                  },
                ],
              },
            },
          },
        ],
      },
    };
    const expected = [
      {
        id: 'D1',
        index: 0,
        display: false,
        label: 'dim1',
        role: undefined,
        description: 'CL_1',
        values: [
          {
            id: '1.1',
            display: true,
            label: '1v1',
            order: -1,
            parentId: undefined,
            position: 0,
            description: '1v1',
          },
          { id: '1.2', display: true, label: '1v2', order: -1, parentId: '1.1', position: 1 },
          { id: '1.3', display: true, label: '1v3', order: -1, parentId: undefined, position: 2 },
          { id: '1.4', display: true, label: '1v4', order: -1, parentId: undefined, position: 3 },
        ],
      },
      {
        id: 'D2',
        index: 1,
        display: true,
        label: undefined,
        role: 'ROLE',
        values: [
          {
            id: '2.2',
            display: false,
            label: undefined,
            order: 0,
            parentId: undefined,
            position: 1,
          },
          {
            id: '2.1',
            display: false,
            label: undefined,
            order: 1,
            parentId: undefined,
            position: 0,
          },
          {
            id: '2.4',
            display: true,
            label: undefined,
            order: 2,
            parentId: undefined,
            position: 3,
          },
          {
            id: '2.3',
            display: true,
            label: undefined,
            order: 3,
            parentId: undefined,
            position: 2,
          },
        ],
      },
      {
        id: 'D3',
        index: 2,
        display: false,
        label: undefined,
        role: undefined,
        values: [
          {
            id: '3.1',
            display: true,
            label: undefined,
            order: -1,
            parentId: undefined,
            position: 0,
          },
          {
            id: '3.2',
            display: true,
            label: undefined,
            order: -1,
            parentId: undefined,
            position: 1,
          },
          {
            id: '3.3',
            display: true,
            label: undefined,
            order: -1,
            parentId: undefined,
            position: 2,
          },
          {
            id: '3.4',
            display: true,
            label: undefined,
            order: -1,
            parentId: undefined,
            position: 3,
          },
        ],
      },
      {
        id: 'D4',
        index: 3,
        display: true,
        label: undefined,
        role: undefined,
        values: [
          {
            id: '4.1',
            display: true,
            label: undefined,
            order: -1,
            parentId: undefined,
            position: 0,
          },
          {
            id: '4.2',
            display: true,
            label: undefined,
            order: -1,
            parentId: undefined,
            position: 1,
          },
          {
            id: '4.3',
            display: false,
            label: undefined,
            order: -1,
            parentId: undefined,
            position: 2,
          },
          {
            id: '4.4',
            display: true,
            label: undefined,
            order: -1,
            parentId: undefined,
            position: 3,
          },
        ],
      },
      {
        id: 'D5',
        index: 4,
        display: false,
        label: undefined,
        role: undefined,
        values: [
          {
            id: '5.1',
            display: true,
            label: undefined,
            order: -1,
            parentId: undefined,
            position: 0,
          },
          {
            id: '5.2',
            display: true,
            label: undefined,
            order: -1,
            parentId: undefined,
            position: 1,
          },
          {
            id: '5.3',
            display: true,
            label: undefined,
            order: -1,
            parentId: undefined,
            position: 2,
          },
          {
            id: '5.4',
            display: true,
            label: undefined,
            order: -1,
            parentId: undefined,
            position: 3,
          },
        ],
      },
      {
        id: 'D6',
        index: 5,
        display: true,
        label: undefined,
        role: undefined,
        values: [
          {
            id: '6.1',
            display: true,
            label: undefined,
            order: -1,
            parentId: undefined,
            position: 0,
          },
          {
            id: '6.2',
            display: false,
            label: undefined,
            order: -1,
            parentId: undefined,
            position: 1,
          },
          {
            id: '6.3',
            display: true,
            label: undefined,
            order: -1,
            parentId: undefined,
            position: 2,
          },
          {
            id: '6.4',
            display: true,
            label: undefined,
            order: -1,
            parentId: undefined,
            position: 3,
          },
        ],
      },
      {
        id: 'D7',
        index: 6,
        display: false,
        label: 'dim7',
        role: undefined,
        values: [
          { id: '7.1', display: true, label: '7v1', order: -1, parentId: undefined, position: 0 },
          { id: '7.2', display: true, label: '7v2', order: -1, parentId: undefined, position: 1 },
          { id: '7.3', display: true, label: '7v3', order: -1, parentId: undefined, position: 2 },
          { id: '7.4', display: true, label: '7v4', order: -1, parentId: undefined, position: 3 },
        ],
      },
    ];
    expect(getDimensions(structure, 'en')).toEqual(expected);
  });
});
