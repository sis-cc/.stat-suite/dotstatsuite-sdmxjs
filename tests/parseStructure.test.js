import { describe, expect, test } from 'vitest';
import { parseStructure } from '../src/';
import classic from './mocks/dataflow-OECD.CFE-DF_OUTBOUND_TOURISM-5.0.json';
import microdata from './mocks/dataflow-OECD-DF_CRS1-1.0.json';

const requestedLocale = 'en';

describe('parseStructure', () => {
  test('should parse, classic usecase', () => {
    expect(parseStructure(classic, requestedLocale)).toMatchSnapshot();
  });

  test('should parse, microdata usecase', () => {
    expect(parseStructure(microdata, requestedLocale)).toMatchSnapshot();
  });
});
