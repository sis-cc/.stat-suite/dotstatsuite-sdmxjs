import { describe, expect, test } from 'vitest';
import { getDataflowName } from '../src/';

describe('getDataflowName', () => {
  test('basic test', () => {
    const structure = { data: { dataflows: [{ id: 'DF', name: 'name' }] } };
    expect(getDataflowName(structure)).toEqual('name');
  });
  test('no name', () => {
    const structure = { data: { dataflows: [{ id: 'DF' }] } };
    expect(getDataflowName(structure)).toBeUndefined();
  });
});
