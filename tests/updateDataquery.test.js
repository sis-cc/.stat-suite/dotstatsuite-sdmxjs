import { describe, expect, test } from 'vitest';
import { updateDataquery } from '../src/';

describe('updateDataquery tests', () => {
  const dimensions = [
    { id: '1', values: [{ id: '1V1' }, { id: '1V2' }, { id: '1V3' }] },
    { id: '2', values: [{ id: '2V1' }, { id: '2V2' }, { id: '2V3' }, { id: '2V5' }] },
    { id: 'FREQ', values: [{ id: 'F1' }, { id: 'F2' }, { id: 'F3' }] },
    { id: '3', values: [{ id: '3V1' }, { id: '3V2' }, { id: '3V3' }] },
    {},
    { id: '4', values: [] },
  ];

  test('append new value', () => {
    expect(updateDataquery(dimensions, '1V1..F2.3V2+3V3..', { 2: ['2V3'] })).toEqual(
      '1V1.2V3.F2.3V2+3V3..',
    );
  });
  test('remove existing value', () => {
    expect(updateDataquery(dimensions, '1V1..F2.3V2+3V3..', { 3: ['3V3'] })).toEqual(
      '1V1..F2.3V2..',
    );
  });
  test('append new value and remove existing values', () => {
    expect(
      updateDataquery(dimensions, '1V1.2V2+2V5.F2.3V2+3V3..', {
        2: ['2V3', '2V2', '2V1', '2V5'],
      }),
    ).toEqual('1V1.2V3+2V1.F2.3V2+3V3..');
  });
  test('append new value and remove existing values with multiple dimensions', () => {
    expect(
      updateDataquery(dimensions, '1V1.2V2+2V5.F2.3V2+3V3..', {
        2: ['2V3', '2V2', '2V1', '2V5'],
        3: ['3V3'],
      }),
    ).toEqual('1V1.2V3+2V1.F2.3V2..');
  });
  test('reset except frequency', () => {
    expect(updateDataquery(dimensions, '1V1..F2.3V2+3V3..', {})).toEqual('..F2...');
  });
  test('reset except frequency', () => {
    expect(updateDataquery(dimensions, '1V1..F2.3V2+3V3..', null)).toEqual('..F2...');
  });
  test('cannot erase frequency', () => {
    expect(updateDataquery(dimensions, '1V1..F2.3V2+3V3..', { FREQ: [] })).toEqual(
      '1V1..F2.3V2+3V3..',
    );
  });
  test('cannot change to a wrong frequency', () => {
    expect(updateDataquery(dimensions, '1V1..F2.3V2+3V3..', { FREQ: ['TOTO'] })).toEqual(
      '1V1..F2.3V2+3V3..',
    );
  });
  test('wrong and multi frequency ', () => {
    expect(updateDataquery(dimensions, '1V1..F2.3V2+3V3..', { FREQ: ['F5', 'F3'] })).toEqual(
      '1V1..F2.3V2+3V3..',
    );
  });
  test('erase one dimension', () => {
    expect(updateDataquery(dimensions, '1V1..F2.3V2+3V3..', { 3: [] })).toEqual('1V1..F2...');
  });
  test('erase two dimensions', () => {
    expect(updateDataquery(dimensions, '1V1+1V3.2V1.F2.3V2+3V3..', { 3: [], 1: [] })).toEqual(
      '.2V1.F2...',
    );
  });
  test('ignored wrong dimension', () => {
    expect(updateDataquery(dimensions, '1V1..F2.3V2+3V3..', { random: ['2V1'] })).toEqual(
      '1V1..F2.3V2+3V3..',
    );
  });
  test('ignored wrong value', () => {
    expect(updateDataquery(dimensions, '1V1..F2.3V2+3V3..', { 2: ['2V4'] })).toEqual(
      '1V1..F2.3V2+3V3..',
    );
  });
  test('the *tot* case', () => {
    const totDimensions = [
      {
        id: 'VAR',
        values: [{ id: 'TOT' }, { id: 'TOT_CAP' }, { id: 'TOT_GDP' }, { id: 'INDEX_2000' }],
      },
    ];
    expect(updateDataquery(totDimensions, 'TOT+TOT_CAP+TOT_GDP', { VAR: ['TOT_GDP'] })).toEqual(
      'TOT+TOT_CAP',
    );
  });
  test('update frequency', () => {
    const deDimensions = [{}, {}, {}, { id: 'FREQUENCY', values: [{ id: 'Q' }, { id: 'M' }] }];

    expect(updateDataquery(deDimensions, '...M', { FREQUENCY: ['Q'] })).toEqual('...Q');
  });
  test('update frequency without updating not displayed dim', () => {
    const deDimensions = [
      {},
      {},
      {},
      { id: 'FREQUENCY', values: [{ id: 'Q' }, { id: 'M' }] },
      { id: 'Dimension not displayed', display: false },
    ];

    expect(updateDataquery(deDimensions, '...M.NOT_DISPLAYED', { FREQUENCY: ['Q'] })).toEqual(
      '...Q.NOT_DISPLAYED',
    );
  });
  test('FREQ and display false value not removed', () => {
    const deDimensions = [
      {},
      {},
      {},
      { id: 'FREQUENCY' },
      { id: 'Dimension not displayed', display: false },
    ];

    expect(updateDataquery(deDimensions, '.REMOVE_ID..M.NOT_DISPLAYED', {})).toEqual('...M.');
  });
});
