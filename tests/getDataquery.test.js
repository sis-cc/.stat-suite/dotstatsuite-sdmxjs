import { describe, expect, test } from 'vitest';
import { getDataquery } from '../src';

const dimensions = [
  { id: '1', values: [{ id: '1_1' }, { id: '1_2' }, { id: '1_3' }] },
  { id: '2', values: [{ id: '2_1' }] },
  { id: '3', values: [{ id: '3_1' }, { id: '3_2' }] },
  { id: '4', values: [{ id: '4_1' }, { id: '4_2' }, { id: '4_3' }, { id: '4_4' }, { id: '4_5' }] },
  { id: '5', values: [] },
  {},
];

describe('getDataquery tests', () => {
  test('simple case', () => {
    expect(getDataquery(dimensions, { 1: ['1_2'], 4: ['4_1', '4_3', '4_4'] })).toEqual(
      '1_2...4_1+4_3+4_4..',
    );
  });
  test('no selection', () => {
    expect(getDataquery(dimensions, {})).toEqual('.....');
  });
  test('invalid values', () => {
    expect(
      getDataquery(dimensions, {
        1: ['1_1', 'fake'],
        unknown: ['bla', 'bla'],
        3: ['3_2', '3_1'],
        4: [],
        5: ['hi'],
        6: undefined,
      }),
    ).toEqual('1_1..3_2+3_1...');
  });
});
