import { describe, expect, test } from 'vitest';
import { getTimePeriodDimension } from '../src/getTimePeriodDimension';

describe('getTimePeriodDimension tests', () => {
  test('non results test', () => {
    const dimensions = [
      { id: 'test1', role: null },
      { id: 'test2', role: 'random' },
    ];
    expect(getTimePeriodDimension({ dimensions })).toEqual(undefined);
  });
});
