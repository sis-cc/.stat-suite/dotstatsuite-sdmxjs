import { describe, expect, test } from 'vitest';
import { parseAgencies } from '../src/parseAgencies';

describe('parseAgencies tests', () => {
  test('blank test', () => {
    expect(parseAgencies({ data: {} })).toEqual({});
  });
  test('basic test', () => {
    expect(
      parseAgencies({
        data: {
          agencySchemes: [
            {
              agencyID: 'S1',
              agencies: [{ id: 'A1', name: 'ag1' }, { id: 'A2' }],
            },
            {
              agencyID: 'S2',
              agencies: [{ id: 'A1', name: 'ag1' }],
            },
            { agencyID: 'S3' },
          ],
        },
      }),
    ).toEqual({
      'S1.A1': { code: 'A1', name: 'ag1', parent: 'S1', id: 'S1.A1' },
      'S1.A2': { code: 'A2', name: undefined, parent: 'S1', id: 'S1.A2' },
      'S2.A1': { code: 'A1', name: 'ag1', parent: 'S2', id: 'S2.A1' },
    });
  });
});
