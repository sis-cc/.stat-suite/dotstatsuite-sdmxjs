import { describe, expect, test } from 'vitest';
import { getUrn } from '../src/getUrn';

describe('getUrn tests', () => {
  test('blank test', () => {
    expect(getUrn({})).toEqual(undefined);
  });
  test('basic test', () => {
    expect(
      getUrn({
        id: 'A',
        links: [
          { rel: 'random', urn: 'first' },
          { rel: 'self', urn: 'correct' },
          { rel: 'self', urn: 'second' },
        ],
      }),
    ).toEqual('correct');
  });
});
