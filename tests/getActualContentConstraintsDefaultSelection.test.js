import { describe, expect, test } from 'vitest';
import { getActualContentConstraintsDefaultSelection } from '../src';

describe('getContentContraintsDefaultSelection', () => {
  test('should have only same value between selection and contentConstraints', () => {
    const selection = { PHI: ['PHI_1', 'PHI_4'], TAU: ['TAU_1', 'TAU_3'] };
    const contentConstraints = {
      PHI: new Set(['PHI_1', 'PHI_2', 'PHI_3']),
      HI: new Set(['HI_1', 'HI_2', 'HI_3']),
      TAU: new Set([]),
    };
    expect(getActualContentConstraintsDefaultSelection({ selection, contentConstraints })).toEqual({
      PHI: ['PHI_1'],
      TAU: [],
    });
  });
});
