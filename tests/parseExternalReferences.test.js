import { describe, expect, test } from 'vitest';
import { parseExternalReference } from '../src/';

describe('parseExternalReference tests', () => {
  test('blank test', () => {
    expect(parseExternalReference(null, 'any')).toEqual(null);
  });
  test('no externalReference', () => {
    const dataflow = {
      isExternalReference: false,
      links: [
        { rel: 'self', href: 'test/dataflow/test/test/test' },
        { rel: 'external', href: 'endpoint/dataflow/agencyId/code/version' },
      ],
    };
    expect(parseExternalReference(dataflow, 'dataflow')).toEqual(null);
  });
  test('no link', () => {
    const dataflow = {
      isExternalReference: true,
      links: [{ rel: 'self', href: 'test/dataflow/test/test/test' }],
    };
    expect(parseExternalReference(dataflow, 'dataflow')).toEqual(null);
  });
  test('wrong type', () => {
    const dataflow = {
      isExternalReference: true,
      links: [
        { rel: 'self', href: 'test/dataflow/test/test/test' },
        { rel: 'external', href: 'endpoint/dataflow/agencyId/code/version' },
      ],
    };
    expect(parseExternalReference(dataflow, 'codelist')).toEqual(null);
  });
  test('incorrect href', () => {
    const dataflow = {
      isExternalReference: true,
      links: [
        { rel: 'self', href: 'test/dataflow/test/test/test' },
        { rel: 'external', href: 'random' },
      ],
    };
    expect(parseExternalReference(dataflow, 'codelist')).toEqual(null);
  });
  test('basic test', () => {
    const dataflow = {
      isExternalReference: true,
      links: [
        { rel: 'self', href: 'test/dataflow/test/test/test' },
        { rel: 'external', href: 'endpoint/dataflow/agencyId/code/version' },
      ],
    };
    expect(parseExternalReference(dataflow, 'dataflow')).toEqual({
      datasource: { url: 'endpoint' },
      identifiers: { agencyId: 'agencyId', code: 'code', version: 'version' },
    });
  });
});
