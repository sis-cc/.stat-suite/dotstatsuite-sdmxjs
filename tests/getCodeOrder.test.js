import { describe, expect, test } from 'vitest';
import { getCodeOrder } from '../src';

describe('getCodeOrder', () => {
  test('simple cases', () => {
    expect(getCodeOrder({ id: 'code_id' })).toEqual(-1);
    expect(getCodeOrder({ id: 'code_id', annotations: [] })).toEqual(-1);
    expect(getCodeOrder({ id: 'code_id', annotations: [undefined] })).toEqual(-1);
    expect(getCodeOrder({ id: 'code_id', annotations: [{ type: 'ORDER', text: '21' }] })).toEqual(
      21,
    );
    expect(getCodeOrder({ id: 'code_id', annotations: [{ type: 'ORDER' }] })).toEqual(-1);
  });
});
