import { describe, expect, test } from 'vitest';
import { getDataFrequency } from '../src/getDataFrequency';

describe('getDataFrequency tests', () => {
  test('no results test', () => {
    expect(
      getDataFrequency({
        attributes: [
          { id: 'attr1', role: null, values: [] },
          { id: 'attr2', role: 'random', values: [{ id: 'val' }] },
        ],
        dimensions: [
          { id: 'dim1', role: null, values: [] },
          { id: 'dim2', role: 'random', values: [{ id: 'val' }] },
        ],
      }),
    ).toEqual(undefined);
  });
  test('basic result in dimensions', () => {
    expect(
      getDataFrequency({
        attributes: [
          { id: 'attr1', role: null, values: [] },
          { id: 'attr2', role: 'random', values: [{ id: 'val' }] },
        ],
        dimensions: [
          { id: 'FREQUENCY', role: null, values: [{ id: 'A' }] },
          { id: 'dim2', role: 'random', values: [{ id: 'val' }] },
        ],
      }),
    ).toEqual('A');
  });
  test('annual result in attribute', () => {
    expect(
      getDataFrequency({
        attributes: [
          { id: 'TIME_FORMAT', role: null, values: [{ id: 'P1Y' }] },
          { id: 'attr2', role: 'random', values: [{ id: 'val' }] },
        ],
        dimensions: [
          { id: 'dim1', role: null, values: [] },
          { id: 'dim2', role: 'random', values: [{ id: 'val' }] },
        ],
      }),
    ).toEqual('A');
  });
  test('semester result in attribute', () => {
    expect(
      getDataFrequency({
        attributes: [
          { id: 'TIME_FORMAT', role: null, values: [{ id: 'P6M' }] },
          { id: 'attr2', role: 'random', values: [{ id: 'val' }] },
        ],
        dimensions: [
          { id: 'dim1', role: null, values: [] },
          { id: 'dim2', role: 'random', values: [{ id: 'val' }] },
        ],
      }),
    ).toEqual('S');
  });
  test('quarter result in attribute', () => {
    expect(
      getDataFrequency({
        attributes: [
          { id: 'TIME_FORMAT', role: null, values: [{ id: 'P3M' }] },
          { id: 'attr2', role: 'random', values: [{ id: 'val' }] },
        ],
        dimensions: [
          { id: 'dim1', role: null, values: [] },
          { id: 'dim2', role: 'random', values: [{ id: 'val' }] },
        ],
      }),
    ).toEqual('Q');
  });
  test('monthly result in attribute', () => {
    expect(
      getDataFrequency({
        attributes: [
          { id: 'TIME_FORMAT', role: null, values: [{ id: 'P1M' }] },
          { id: 'attr2', role: 'random', values: [{ id: 'val' }] },
        ],
        dimensions: [
          { id: 'dim1', role: null, values: [] },
          { id: 'dim2', role: 'random', values: [{ id: 'val' }] },
        ],
      }),
    ).toEqual('M');
  });
  test('weekly result in attribute', () => {
    expect(
      getDataFrequency({
        attributes: [
          { id: 'TIME_FORMAT', role: null, values: [{ id: 'P7D' }] },
          { id: 'attr2', role: 'random', values: [{ id: 'val' }] },
        ],
        dimensions: [
          { id: 'dim1', role: null, values: [] },
          { id: 'dim2', role: 'random', values: [{ id: 'val' }] },
        ],
      }),
    ).toEqual('W');
  });
  test('daily result in attribute', () => {
    expect(
      getDataFrequency({
        attributes: [
          { id: 'TIME_FORMAT', role: null, values: [{ id: 'P1D' }] },
          { id: 'attr2', role: 'random', values: [{ id: 'val' }] },
        ],
        dimensions: [
          { id: 'dim1', role: null, values: [] },
          { id: 'dim2', role: 'random', values: [{ id: 'val' }] },
        ],
      }),
    ).toEqual('D');
  });
  test('minutely result in attribute', () => {
    expect(
      getDataFrequency({
        attributes: [
          { id: 'TIME_FORMAT', role: null, values: [{ id: 'PT1M' }] },
          { id: 'attr2', role: 'random', values: [{ id: 'val' }] },
        ],
        dimensions: [
          { id: 'dim1', role: null, values: [] },
          { id: 'dim2', role: 'random', values: [{ id: 'val' }] },
        ],
      }),
    ).toEqual('N');
  });
  test('biannual result from time period', () => {
    expect(
      getDataFrequency({
        attributes: [
          { id: 'attr1', role: null, values: [] },
          { id: 'attr2', role: 'random', values: [{ id: 'val' }] },
        ],
        dimensions: [
          {
            id: 'TIME_PERIOD',
            role: null,
            values: [
              { id: '2015-2016', start: '2015-01-01T00:00:00Z', end: '2016-12-31T23:59:59Z' },
            ],
          },
          { id: 'dim2', role: 'random', values: [{ id: 'val' }] },
        ],
      }),
    ).toEqual('A2');
  });
  test('annual result from time period', () => {
    expect(
      getDataFrequency({
        attributes: [
          { id: 'attr1', role: null, values: [] },
          { id: 'attr2', role: 'random', values: [{ id: 'val' }] },
        ],
        dimensions: [
          {
            id: 'TIME_PERIOD',
            role: null,
            values: [{ id: '2015', start: '2015-01-01T00:00:00Z', end: '2015-12-31T23:59:59Z' }],
          },
          { id: 'dim2', role: 'random', values: [{ id: 'val' }] },
        ],
      }),
    ).toEqual('A');
  });
  test('semester result from time period', () => {
    expect(
      getDataFrequency({
        attributes: [
          { id: 'attr1', role: null, values: [] },
          { id: 'attr2', role: 'random', values: [{ id: 'val' }] },
        ],
        dimensions: [
          {
            id: 'TIME_PERIOD',
            role: null,
            values: [{ id: '2015-S2', start: '2015-07-01T00:00:00Z', end: '2015-12-31T23:59:59Z' }],
          },
          { id: 'dim2', role: 'random', values: [{ id: 'val' }] },
        ],
      }),
    ).toEqual('S');
  });
  test('quarter result from time period', () => {
    expect(
      getDataFrequency({
        attributes: [
          { id: 'attr1', role: null, values: [] },
          { id: 'attr2', role: 'random', values: [{ id: 'val' }] },
        ],
        dimensions: [
          {
            id: 'TIME_PERIOD',
            role: null,
            values: [{ id: '2015-Q1', start: '2015-01-01T00:00:00Z', end: '2015-03-31T23:59:59Z' }],
          },
          { id: 'dim2', role: 'random', values: [{ id: 'val' }] },
        ],
      }),
    ).toEqual('Q');
  });
  test('monthly result from time period', () => {
    expect(
      getDataFrequency({
        attributes: [
          { id: 'attr1', role: null, values: [] },
          { id: 'attr2', role: 'random', values: [{ id: 'val' }] },
        ],
        dimensions: [
          {
            id: 'TIME_PERIOD',
            role: null,
            values: [{ id: '2015-01', start: '2015-01-01T00:00:00Z', end: '2015-01-31T23:59:59Z' }],
          },
          { id: 'dim2', role: 'random', values: [{ id: 'val' }] },
        ],
      }),
    ).toEqual('M');
  });
  test('weekly result from time period', () => {
    expect(
      getDataFrequency({
        attributes: [
          { id: 'attr1', role: null, values: [] },
          { id: 'attr2', role: 'random', values: [{ id: 'val' }] },
        ],
        dimensions: [
          {
            id: 'TIME_PERIOD',
            role: null,
            values: [
              { id: '2015-W01', start: '2015-01-01T00:00:00Z', end: '2015-01-07T23:59:59Z' },
            ],
          },
          { id: 'dim2', role: 'random', values: [{ id: 'val' }] },
        ],
      }),
    ).toEqual('W');
  });
  test('daily result from time period', () => {
    expect(
      getDataFrequency({
        attributes: [
          { id: 'attr1', role: null, values: [] },
          { id: 'attr2', role: 'random', values: [{ id: 'val' }] },
        ],
        dimensions: [
          {
            id: 'TIME_PERIOD',
            role: null,
            values: [
              { id: '2015-01-01', start: '2015-01-01T00:00:00Z', end: '2015-01-01T23:59:59Z' },
            ],
          },
          { id: 'dim2', role: 'random', values: [{ id: 'val' }] },
        ],
      }),
    ).toEqual('D');
  });
  test('hourly result from time period', () => {
    expect(
      getDataFrequency({
        attributes: [
          { id: 'attr1', role: null, values: [] },
          { id: 'attr2', role: 'random', values: [{ id: 'val' }] },
        ],
        dimensions: [
          {
            id: 'TIME_PERIOD',
            role: null,
            values: [
              {
                id: '2015-01-01T01:00:00',
                start: '2015-01-01T00:00:00Z',
                end: '2015-01-01T00:59:59Z',
              },
            ],
          },
          { id: 'dim2', role: 'random', values: [{ id: 'val' }] },
        ],
      }),
    ).toEqual('H');
  });
  test('minutely result from time period', () => {
    expect(
      getDataFrequency({
        attributes: [
          { id: 'attr1', role: null, values: [] },
          { id: 'attr2', role: 'random', values: [{ id: 'val' }] },
        ],
        dimensions: [
          {
            id: 'TIME_PERIOD',
            role: null,
            values: [
              {
                id: '2015-01-01T01:01:00',
                start: '2015-01-01T00:00:00Z',
                end: '2015-01-01T00:00:59Z',
              },
            ],
          },
          { id: 'dim2', role: 'random', values: [{ id: 'val' }] },
        ],
      }),
    ).toEqual('N');
  });
});
