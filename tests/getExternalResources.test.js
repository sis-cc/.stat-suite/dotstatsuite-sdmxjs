import { describe, expect, test } from 'vitest';
import { getExternalResources } from '../src';

const structure = {
  meta: { contentLanguages: ['en'] },
  data: {
    dataflows: [
      {
        annotations: [
          {},
          { type: 'random' },
          { type: 'EXT_RESOURCE' },
          { type: 'EXT_RESOURCE', text: 'insufficient_data' },
          { type: 'EXT_RESOURCE', text: 'Heres the label|and here is the link' },
          { type: 'EXT_RESOURCE', text: 'such|strong|format' },
        ],
      },
    ],
  },
};

describe('getExternalResources test', () => {
  test('complete case', () => {
    expect(getExternalResources(structure)).toEqual([
      {
        id: 'EXT_RESOURCE_0',
        img: undefined,
        label: 'Heres the label',
        link: 'and here is the link',
      },
      {
        id: 'EXT_RESOURCE_1',
        img: 'format',
        label: 'such',
        link: 'strong',
      },
    ]);
  });
});
