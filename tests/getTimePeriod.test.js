import { describe, expect, test } from 'vitest';
import { getTimePeriod } from '../src/getTimePeriod';

const structure = (annotations) => ({
  data: {
    conceptSchemes: [
      {
        concepts: [
          {
            id: 'T_P',
            links: [{ rel: 'self', urn: 'URN/T_P' }],
            name: 'Time Period',
          },
        ],
      },
    ],
    dataStructures: [
      {
        dataStructureComponents: {
          dimensionList: {
            timeDimensions: [{ id: 'TIME', conceptIdentity: 'URN/T_P' }],
          },
        },
      },
    ],
    dataflows: [{ annotations }],
  },
});

const annotations = [{ title: 'TIME,FREQ', type: 'NOT_DISPLAYED' }];

describe('getTimePeriod tests', () => {
  test('simple case', () => {
    expect(getTimePeriod(structure())).toEqual({
      id: 'TIME',
      label: 'Time Period',
      display: true,
    });
  });
  test('not displayed', () => {
    expect(getTimePeriod(structure(annotations))).toEqual({
      id: 'TIME',
      label: 'Time Period',
      display: false,
    });
  });
});
