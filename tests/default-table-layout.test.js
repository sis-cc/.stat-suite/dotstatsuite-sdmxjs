import { describe, expect, test } from 'vitest';
import { getDefaultTableLayout } from '../src';

describe('dataflow', () => {
  test('simple case', () => {
    const structure = {
      data: {
        dataflows: [
          {
            annotations: [
              { title: 'DIM1,DIM3', type: 'LAYOUT_ROW' },
              { title: 'DIM2', type: 'LAYOUT_COLUMN' },
              { title: 'DIM5', type: 'LAYOUT_ROW_SECTION' },
              { title: 'DIM4', type: 'LAYOUT_ROW' },
            ],
          },
        ],
      },
    };
    const expected = {
      header: ['DIM2'],
      rows: ['DIM1', 'DIM3', 'DIM4'],
      sections: ['DIM5'],
    };
    expect(getDefaultTableLayout(structure)).toEqual(expected);
  });
});
