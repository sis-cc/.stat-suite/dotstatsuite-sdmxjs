import { describe, expect, test } from 'vitest';
import { getSdmxFormat } from '../src/getSdmxFormat';
import { DEFAULT_FORMATS, FILE_FORMAT, URN_FORMAT } from '../src/constants/headers';

describe('format tests', () => {
  test('default', () => {
    expect(getSdmxFormat({})).toEqual(`${DEFAULT_FORMATS.structure.json};${URN_FORMAT}`);
  });
  test('unknown structure format', () => {
    expect(getSdmxFormat({ format: 'random', type: 'dataflow' })).toEqual(
      `${DEFAULT_FORMATS.structure.json};${URN_FORMAT}`,
    );
  });
  test('unknown data format', () => {
    expect(getSdmxFormat({ format: 'random', type: 'data' })).toEqual(
      `${DEFAULT_FORMATS.data.json};${URN_FORMAT}`,
    );
  });
  test('csv data', () => {
    expect(getSdmxFormat({ format: 'csv', type: 'data' })).toEqual(
      `${DEFAULT_FORMATS.data.csv};${URN_FORMAT};labels=code`,
    );
  });
  test('as file', () => {
    expect(getSdmxFormat({ asFile: true })).toEqual(
      `${DEFAULT_FORMATS.structure.json};${URN_FORMAT};${FILE_FORMAT}`,
    );
  });
  test('without urn', () => {
    expect(getSdmxFormat({ type: 'dataflow', withUrn: false })).toEqual(
      DEFAULT_FORMATS.structure.json,
    );
  });
  test('datasource custom header format', () => {
    expect(
      getSdmxFormat({
        format: 'json',
        type: 'dataflow',
        datasource: { headers: { structure: { json: 'customHeaderFormat' } } },
      }),
    ).toEqual(`customHeaderFormat;urn=true`);
  });
  test('datasource incorrect custom header format', () => {
    expect(
      getSdmxFormat({
        format: 'xml',
        type: 'dataflow',
        datasource: { headers: { structure: { json: 'customHeaderFormat' } } },
      }),
    ).toEqual(`${DEFAULT_FORMATS.structure.xml};${URN_FORMAT}`);
  });
});
