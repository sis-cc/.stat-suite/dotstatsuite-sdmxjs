import { describe, expect, test } from 'vitest';
import { parseArtefactRelatives } from '../src';

describe('parseArtefactRelatives tests', () => {
  test('blank test', () => {
    expect(
      parseArtefactRelatives({ sdmxJson: { data: {} }, artefactId: null, locale: null }),
    ).toEqual([]);
  });
  test('basic test', () => {
    const sdmxJson = {
      data: {
        dataflows: [
          {
            name: 'Dataflow',
            agencyID: 'A',
            id: 'DF',
            isFinal: true,
            version: '1',
          },
        ],
      },
    };
    expect(parseArtefactRelatives({ sdmxJson, artefactId: 'datastructure:A:DSD(1)' })).toEqual([
      {
        annotations: [],
        label: 'Dataflow',
        sdmxId: 'A:DF(1)',
        code: 'DF',
        type: 'dataflow',
        version: '1',
        agencyId: 'A',
        isFinal: true,
      },
    ]);
  });
  test('basic test', () => {
    const sdmxJson = {
      data: {
        dataflows: [
          {
            name: 'Dataflow',
            agencyID: 'A',
            id: 'DF',
            isFinal: true,
            version: '1',
          },
          {
            annotations: [{ title: 'SDMX_ANNOT' }],
            agencyID: 'A',
            id: 'DF',
            isFinal: false,
            version: '2',
          },
        ],
        dataStructures: [
          {
            names: {},
            agencyID: 'A',
            id: 'DSD',
            version: 1,
          },
        ],
      },
    };
    expect(parseArtefactRelatives({ sdmxJson, artefactId: 'datastructure:A:DSD(1)' })).toEqual([
      {
        annotations: [],
        label: 'Dataflow',
        sdmxId: 'A:DF(1)',
        code: 'DF',
        type: 'dataflow',
        version: '1',
        agencyId: 'A',
        isFinal: true,
      },
      {
        annotations: [{ title: 'SDMX_ANNOT' }],
        label: undefined,
        sdmxId: 'A:DF(2)',
        code: 'DF',
        type: 'dataflow',
        version: '2',
        agencyId: 'A',
        isFinal: false,
      },
    ]);
  });
  test('no relatives test', () => {
    const sdmxJson = {
      data: {
        dataflows: [
          {
            name: 'Dataflow',
            agencyID: 'A',
            id: 'DF',
            isFinal: true,
            version: '1',
          },
        ],
      },
    };
    expect(parseArtefactRelatives({ sdmxJson, artefactId: 'dataflow:A:DF(1)' })).toEqual([]);
  });
});
