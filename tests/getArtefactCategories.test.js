import { describe, expect, test } from 'vitest';
import { getArtefactCategories } from '../src/getArtefactCategories';

const data = {
  categorisations: [
    { id: 'C', source: 'sdmx.urn=Ag:Df(1.0)', target: 'sdmx.urn=A:S(0).C' }, // non existing scheme
    { id: 'Z', source: 'sdmx.urn=Ag:Df(1.0)', target: 'sdmx.urn=Ag:Sc(1.0).Z' }, // non existing category
    { id: 'Cat1', source: 'sdmx.urn=Ag:Df(1.0)', target: 'sdmx.urn=Ag:Sc(1.0).Cat1.Z' }, // non existing children
    { id: 'Cat1', source: 'sdmx.urn=Ag:Df(1.0)', target: 'sdmx.urn=Ag:Sc(1.0).Cat1.Cat11' },
    { id: 'Cat', source: 'sdmx.urn=Ag:Df(1.0)', target: 'sdmx.urn=Ag:Sc(2.0).Cat' },
    { id: 'Cat2', source: 'sdmx.urn=Ag:Df(2.0)', target: 'sdmx.urn=Ag:Sc(2.0).Cat2' },
  ],
  categorySchemes: [
    {
      agencyID: 'Ag',
      id: 'Sc',
      name: 'Scheme',
      version: '1.0',
      categories: [
        {
          id: 'Cat1',
          name: 'Category 1',
          categories: [{ id: 'Cat11' }],
        },
      ],
    },
    {
      agencyID: 'Ag',
      id: 'Sc',
      name: 'Scheme2.0',
      version: '2.0',
      categories: [
        {
          id: 'Cat',
          name: 'Category',
          categories: [],
        },
        {
          id: 'Cat2',
          name: 'Category 2',
          categories: [],
        },
      ],
    },
  ],
};

describe('getArtefactCategories tests', () => {
  test('blank test', () => {
    expect(getArtefactCategories({ artefactId: null, data: {} })).toEqual([]);
  });
  test('complete test', () => {
    expect(getArtefactCategories({ artefactId: 'Ag:Df(1.0)', data })).toEqual([
      [
        { id: 'Sc', name: 'Scheme' },
        { id: 'Cat1', name: 'Category 1' },
      ],
      [
        { id: 'Sc', name: 'Scheme' },
        { id: 'Cat1', name: 'Category 1' },
        { id: 'Cat11', name: undefined },
      ],
      [
        { id: 'Sc', name: 'Scheme2.0' },
        { id: 'Cat', name: 'Category' },
      ],
    ]);
  });
});
