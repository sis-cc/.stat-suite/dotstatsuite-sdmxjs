import { describe, expect, test } from 'vitest';
import { getArtefactsListRequestArgs } from '../src';
import { DEFAULT_FORMATS } from '../src/constants/headers';

describe('getArtefactsListRequestArgs tests', () => {
  test('basic test with category', () => {
    const props = {
      datasource: { url: 'endpoint' },
      agencyId: 'AG',
      category: {
        scheme: { code: 'SCH', agencyId: 'SCH_AG', version: 'SCH_V' },
        hierarchicalCode: 'CAT',
      },
      type: 'TYPE',
      version: 'V',
      locale: 'LOC',
      withUrn: false,
    };

    const expected = {
      url: 'endpoint/categoryscheme/SCH_AG/SCH/SCH_V/CAT',
      params: {
        detail: 'allcompletestubs',
        references: 'TYPE',
      },
      headers: {
        Accept: DEFAULT_FORMATS.structure.json,
        'Accept-Language': 'LOC',
      },
      parsing: { type: 'TYPE', fromCategory: true, agencyIds: [], space: { url: 'endpoint' } },
    };
    expect(getArtefactsListRequestArgs(props)).toEqual(expected);
  });
  test('basic test without category', () => {
    const props = {
      datasource: { url: 'endpoint' },
      agencyId: 'AG',
      type: 'TYPE',
      version: 'V',
      locale: 'LOC',
      withUrn: false,
    };

    const expected = {
      url: 'endpoint/TYPE/AG/all/V',
      params: {
        detail: 'allcompletestubs',
      },
      headers: {
        Accept: DEFAULT_FORMATS.structure.json,
        'Accept-Language': 'LOC',
      },
      parsing: { fromCategory: false, type: 'TYPE', agencyIds: [], space: { url: 'endpoint' } },
    };
    expect(getArtefactsListRequestArgs(props)).toEqual(expected);
  });
});
