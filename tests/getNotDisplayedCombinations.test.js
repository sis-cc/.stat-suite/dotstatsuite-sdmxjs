import { describe, expect, test } from 'vitest';
import { getNotDisplayedCombinations } from '../src/getNotDisplayedCombinations';

describe('getNotDisplayedCombinations tests', () => {
  test('not provided case', () => {
    const annotation = { title: '' };
    expect(getNotDisplayedCombinations(annotation)).toEqual({});
  });
  test('all dimensions have combined values to hide', () => {
    const annotation = { title: 'd0=(v0+v1),d1=(v0+v1+v2),d2=(v0)' };
    const expected = {
      d0: ['v0', 'v1'],
      d1: ['v0', 'v1', 'v2'],
      d2: ['v0'],
    };
    expect(getNotDisplayedCombinations(annotation)).toEqual(expected);
  });
  test('should return dimensions that have combined values only', () => {
    const annotation = { title: 'd0=(v0+v1,d1=(v0+v1)+v2,d2=(v0+v1),d3=,d4=v1' };

    const expected = {
      d1: ['v0', 'v1'],
      d2: ['v0', 'v1'],
    };
    expect(getNotDisplayedCombinations(annotation)).toEqual(expected);
  });
  test('should return empty object', () => {
    const annotation = { title: 'd0,d1=,d2=v0,d3=v0+,d4=v0+v1,d5=(v0,d6=v1+),d5=()' };

    expect(getNotDisplayedCombinations(annotation)).toEqual({});
  });
});
