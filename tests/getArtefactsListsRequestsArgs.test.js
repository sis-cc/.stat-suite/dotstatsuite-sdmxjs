import { describe, expect, test } from 'vitest';
import { getArtefactsListsRequestsArgs } from '../src';
import { DEFAULT_FORMATS } from '../src/constants/headers';

describe('getArtefactsListsRequestsArgs tests', () => {
  test('basic test', () => {
    const props = {
      categories: [
        {
          scheme: { agencyId: 'SCH1_AG', code: 'SCH1', version: 'SCH1_V' },
          hierarchicalCode: 'CAT1',
        },
        {
          scheme: { agencyId: 'SCH2_AG', code: 'SCH2', version: 'SCH2_V' },
          hierarchicalCode: 'CAT2',
        },
      ],
      spaces: { a: { id: 'a', endpoint: 'endpoint1' }, b: { id: 'b', endpoint: 'endpoint2' } },
      locale: 'LOC',
      types: ['TYPE1', 'TYPE2'],
      versions: ['V'],
      withUrn: false,
    };
    const expected = [
      {
        url: 'endpoint1/categoryscheme/SCH1_AG/SCH1/SCH1_V/CAT1',
        params: {
          detail: 'allcompletestubs',
          references: 'TYPE1',
        },
        headers: {
          Accept: DEFAULT_FORMATS.structure.json,
          'Accept-Language': 'LOC',
        },
        parsing: {
          type: 'TYPE1',
          fromCategory: true,
          agencyIds: [],
          space: { id: 'a', url: 'endpoint1', endpoint: 'endpoint1' },
        },
      },
      {
        url: 'endpoint1/categoryscheme/SCH1_AG/SCH1/SCH1_V/CAT1',
        params: {
          detail: 'allcompletestubs',
          references: 'TYPE2',
        },
        headers: {
          Accept: DEFAULT_FORMATS.structure.json,
          'Accept-Language': 'LOC',
        },
        parsing: {
          type: 'TYPE2',
          fromCategory: true,
          agencyIds: [],
          space: { id: 'a', url: 'endpoint1', endpoint: 'endpoint1' },
        },
      },
      {
        url: 'endpoint1/categoryscheme/SCH2_AG/SCH2/SCH2_V/CAT2',
        params: {
          detail: 'allcompletestubs',
          references: 'TYPE1',
        },
        headers: {
          Accept: DEFAULT_FORMATS.structure.json,
          'Accept-Language': 'LOC',
        },
        parsing: {
          type: 'TYPE1',
          fromCategory: true,
          agencyIds: [],
          space: { id: 'a', url: 'endpoint1', endpoint: 'endpoint1' },
        },
      },
      {
        url: 'endpoint1/categoryscheme/SCH2_AG/SCH2/SCH2_V/CAT2',
        params: {
          detail: 'allcompletestubs',
          references: 'TYPE2',
        },
        headers: {
          Accept: DEFAULT_FORMATS.structure.json,
          'Accept-Language': 'LOC',
        },
        parsing: {
          type: 'TYPE2',
          fromCategory: true,
          agencyIds: [],
          space: { id: 'a', url: 'endpoint1', endpoint: 'endpoint1' },
        },
      },
      {
        url: 'endpoint2/categoryscheme/SCH1_AG/SCH1/SCH1_V/CAT1',
        params: {
          detail: 'allcompletestubs',
          references: 'TYPE1',
        },
        headers: {
          Accept: DEFAULT_FORMATS.structure.json,
          'Accept-Language': 'LOC',
        },
        parsing: {
          type: 'TYPE1',
          fromCategory: true,
          agencyIds: [],
          space: { id: 'b', url: 'endpoint2', endpoint: 'endpoint2' },
        },
      },
      {
        url: 'endpoint2/categoryscheme/SCH1_AG/SCH1/SCH1_V/CAT1',
        params: {
          detail: 'allcompletestubs',
          references: 'TYPE2',
        },
        headers: {
          Accept: DEFAULT_FORMATS.structure.json,
          'Accept-Language': 'LOC',
        },
        parsing: {
          type: 'TYPE2',
          fromCategory: true,
          agencyIds: [],
          space: { id: 'b', url: 'endpoint2', endpoint: 'endpoint2' },
        },
      },
      {
        url: 'endpoint2/categoryscheme/SCH2_AG/SCH2/SCH2_V/CAT2',
        params: {
          detail: 'allcompletestubs',
          references: 'TYPE1',
        },
        headers: {
          Accept: DEFAULT_FORMATS.structure.json,
          'Accept-Language': 'LOC',
        },
        parsing: {
          type: 'TYPE1',
          fromCategory: true,
          agencyIds: [],
          space: { id: 'b', url: 'endpoint2', endpoint: 'endpoint2' },
        },
      },
      {
        url: 'endpoint2/categoryscheme/SCH2_AG/SCH2/SCH2_V/CAT2',
        params: {
          detail: 'allcompletestubs',
          references: 'TYPE2',
        },
        headers: {
          Accept: DEFAULT_FORMATS.structure.json,
          'Accept-Language': 'LOC',
        },
        parsing: {
          type: 'TYPE2',
          fromCategory: true,
          agencyIds: [],
          space: { id: 'b', url: 'endpoint2', endpoint: 'endpoint2' },
        },
      },
    ];

    expect(getArtefactsListsRequestsArgs(props)).toEqual(expected);
  });
  test('incompatible categories and space', () => {
    const props = {
      categories: {
        a: { scheme: { agencyId: 'A', code: 'S', version: 'V' }, code: 'C', spaceId: 'c' },
      },
      spaces: {
        s: { id: 's', endpoint: 'endpoint' },
      },
      types: ['t'],
      versions: ['latest'],
      locale: 'L',
      agencies: { ag: { code: 'AG', id: 'SDMX.AG' } },
      withUrn: false,
    };

    const expected = [
      {
        url: 'endpoint/t/AG/all/latest',
        params: { detail: 'allcompletestubs' },
        headers: {
          Accept: DEFAULT_FORMATS.structure.json,
          'Accept-Language': 'L',
        },
        parsing: {
          type: 't',
          fromCategory: false,
          agencyIds: ['AG'],
          space: { id: 's', url: 'endpoint', endpoint: 'endpoint' },
        },
      },
    ];

    expect(getArtefactsListsRequestsArgs(props)).toEqual(expected);
  });
  test('category and agency', () => {
    const props = {
      categories: {
        a: { scheme: { agencyId: 'A', code: 'S', version: 'V' }, code: 'C', spaceId: 's' },
      },
      spaces: {
        s: { id: 's', endpoint: 'endpoint' },
      },
      types: ['t'],
      versions: ['latest'],
      locale: 'L',
      agencies: { ag: { code: 'AG', id: 'SDMX.AG' } },
    };

    const expected = [
      {
        url: 'endpoint/categoryscheme/A/S/V/C',
        params: { detail: 'allcompletestubs', references: 't' },
        headers: {
          Accept: DEFAULT_FORMATS.structure.json,
          'Accept-Language': 'L',
        },
        parsing: {
          type: 't',
          fromCategory: true,
          agencyIds: ['AG'],
          space: { id: 's', url: 'endpoint', endpoint: 'endpoint' },
        },
      },
    ];
  });
});
