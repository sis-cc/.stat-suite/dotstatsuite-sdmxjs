import { describe, expect, test } from 'vitest';
import { getDataflowDescription } from '../src/';

describe('getDataflowDescription', () => {
  test('present description', () => {
    const structure = { data: { dataflows: [{ id: 'Df', description: 'description' }] } };
    expect(getDataflowDescription(structure)).toEqual('description');
  });
  test('absent description', () => {
    const structure = { data: { dataflows: [{ id: 'Df' }] } };
    expect(getDataflowDescription(structure)).toEqual(null);
  });
});
