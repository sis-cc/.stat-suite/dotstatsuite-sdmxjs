## mocks

#### registry

> store requests to be able to update mocks if NSI is updated (and thus the SDMX format)

* dataflow-OECD.CFE-DF_OUTBOUND_TOURISM-5.0: http://nsi-stable-siscc.redpelicans.com/rest/dataflow/OECD.CFE/DF_OUTBOUND_TOURISM/5.0?references=all&detail=referencepartial
