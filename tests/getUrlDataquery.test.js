import { describe, expect, test } from 'vitest';
import { getUrlDataquery } from '../src/getUrlDataquery';
import { ALL_IDENTIFIER } from '../src/constants/url';

describe('getUrlDataquery tests', () => {
  test('missing dataquery', () => {
    expect(getUrlDataquery(undefined)).toEqual(ALL_IDENTIFIER);
  });
  test('empty dataquery 1', () => {
    expect(getUrlDataquery('')).toEqual(ALL_IDENTIFIER);
  });
  test('empty dataquery 2', () => {
    expect(getUrlDataquery('....')).toEqual(ALL_IDENTIFIER);
  });
  test('regular dataquery', () => {
    const dataquery = '..some_value..';
    expect(getUrlDataquery(dataquery)).toEqual(dataquery);
  });
});
