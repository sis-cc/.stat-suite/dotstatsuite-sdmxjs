import { describe, expect, test } from 'vitest';
import { parseCategorisations } from '../src/parseCategorisations';

describe('parseCategorisations tests', () => {
  test('blank test', () => {
    expect(parseCategorisations({})).toEqual({});
  });
  test('complete cases', () => {
    expect(
      parseCategorisations({
        data: {
          categorisations: [
            { source: 'urn=Ag:Df1(1.0)', target: 'blablabla=Ag:Sch(1.0).Cat' },
            { source: 'urn=Ag:Df1(1.0)' },
            { target: 'withoutSource' },
            { source: 'urn=Ag:Df1(1.0)', target: 'something' },
            { source: 'urn=Ag:Df1(2.0)', target: 'sdmx.urn.=Ag:Sch(11).Par1.Par2.Child' },
            { source: 'urn=Ag:Df1(1.0)', target: 'blablabla=Ag:Sch(1.0).Cat2' },
            { source: 'urn=ParAg.ChAg:DF(1)', target: 'urn=ParAg.ChAg:Sch(1).Cat' },
            { source: 'urn=OECD.SOC:SDD@IDD(2.0)', target: 'urn=OECD:OECDCS1(1.0).SOC' },
          ],
        },
      }),
    ).toEqual({
      'Ag:Df1(1.0)': [
        { schemeId: 'Ag:Sch(1.0)', path: 'Cat' },
        { schemeId: 'Ag:Sch(1.0)', path: 'Cat2' },
      ],
      'Ag:Df1(2.0)': [{ schemeId: 'Ag:Sch(11)', path: 'Par1.Par2.Child' }],
      'ParAg.ChAg:DF(1)': [{ schemeId: 'ParAg.ChAg:Sch(1)', path: 'Cat' }],
      'OECD.SOC:SDD@IDD(2.0)': [{ schemeId: 'OECD:OECDCS1(1.0)', path: 'SOC' }],
    });
  });
});
