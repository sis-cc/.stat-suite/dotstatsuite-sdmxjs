import { describe, expect, test } from 'vitest';
import { withFlatHierarchy } from '../src';

describe('withFlatHierarchy tests', () => {
  test('no values', () => {
    expect(withFlatHierarchy({})).toEqual({ values: [] });
  });
  test('no hierarchy', () => {
    expect(withFlatHierarchy({ values: [{ id: 0 }, { id: 1 }, { id: 2 }] })).toEqual({
      values: [
        { id: 0, parents: [] },
        { id: 1, parents: [] },
        { id: 2, parents: [] },
      ],
    });
  });
  test('hierarchy', () => {
    expect(
      withFlatHierarchy({
        values: [
          { id: 1 },
          { id: 20, parent: 2 },
          { id: 10, parent: 1 },
          { id: 2 },
          { id: 110, parent: 11 },
          { id: 30, parent: 3 },
          { id: 11, parent: 1 },
          { id: 21, parent: 2 },
          { id: 111, parent: 11 },
          { id: 12, parent: 1 },
        ],
      }),
    ).toEqual({
      values: [
        { id: 1, parents: [] },
        { id: 10, parent: 1, parents: [1] },
        { id: 11, parent: 1, parents: [1] },
        { id: 110, parent: 11, parents: [1, 11] },
        { id: 111, parent: 11, parents: [1, 11] },
        { id: 12, parent: 1, parents: [1] },
        { id: 2, parents: [] },
        { id: 20, parent: 2, parents: [2] },
        { id: 21, parent: 2, parents: [2] },
        { id: 30, parent: 3, parents: [] },
      ],
    });
  });
});
