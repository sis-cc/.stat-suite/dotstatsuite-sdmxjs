import { describe, expect, test } from 'vitest';
import { parseNoDisplayAnnotation } from '../src/parseNoDisplayAnnotation';

describe('parseNoDisplayAnnotation tests', () => {
  test('no annotation', () => {
    const structure = {
      data: {
        dataflows: [
          {
            annotations: [{ type: 'RANDOM_1' }, { type: 'RANDOM_2' }, { type: 'RANDOM_3' }],
          },
        ],
      },
    };
    expect(parseNoDisplayAnnotation(structure)).toEqual({});
  });
  test('dim without values, and dim with one or multi values', () => {
    const structure = {
      data: {
        dataflows: [
          {
            annotations: [
              { type: 'RANDOM_1' },
              { type: 'RANDOM_2' },
              { type: 'NOT_DISPLAYED', title: 'D1,D2=VAL1+VAL2,D3=,D4=VAL3' },
              { type: 'RANDOM_3' },
            ],
          },
        ],
      },
    };
    const expected = {
      D1: { id: 'D1' },
      D2: { id: 'D2', values: { VAL1: 'VAL1', VAL2: 'VAL2' } },
      D3: { id: 'D3' },
      D4: { id: 'D4', values: { VAL3: 'VAL3' } },
    };
    expect(parseNoDisplayAnnotation(structure)).toEqual(expected);
  });
});
