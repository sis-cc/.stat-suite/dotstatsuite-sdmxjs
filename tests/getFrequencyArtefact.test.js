import { describe, expect, test } from 'vitest';
import { getFrequencyArtefact } from '../src/getFrequencyArtefact';

describe('getFrequencyArtefact tests', () => {
  test('no results test', () => {
    expect(
      getFrequencyArtefact({
        attributes: [
          { id: 'att1', role: null },
          { id: 'att2', role: 'random' },
        ],
        dimensions: [
          { id: 'dim1', role: null },
          { id: 'dim2', role: 'random' },
        ],
      }),
    ).toEqual(undefined);
  });
  test('attribute result and values parsing test', () => {
    expect(
      getFrequencyArtefact({
        attributes: [
          { id: 'att1', role: null },
          { id: 'att2', role: 'random' },
          {
            id: 'TIME_FORMAT',
            values: [{ id: 'P1Y' }, { id: 'P6M' }, { id: 'P1D' }, { id: 'unknown' }],
          },
        ],
        dimensions: [
          { id: 'dim1', role: null },
          { id: 'dim2', role: 'random' },
        ],
      }),
    ).toEqual({
      id: 'TIME_FORMAT',
      isAttribute: true,
      values: [{ id: 'A' }, { id: 'S' }, { id: 'D' }],
    });
  });
  test('attribute does not have values', () => {
    expect(
      getFrequencyArtefact({
        attributes: [
          { id: 'att1', role: null },
          { id: 'att2', role: 'random' },
          { id: 'TIME_FORMAT', values: [] },
        ],
        dimensions: [
          { id: 'dim1', role: null },
          { id: 'dim2', role: 'random' },
        ],
      }),
    ).toEqual(undefined);
  });
  test('attribute does not have supported values', () => {
    expect(
      getFrequencyArtefact({
        attributes: [
          { id: 'att1', role: null },
          { id: 'att2', role: 'random' },
          { id: 'TIME_FORMAT', values: [{ id: 'wrong', label: 'should not appear' }] },
        ],
        dimensions: [
          { id: 'dim1', role: null },
          { id: 'dim2', role: 'random' },
        ],
      }),
    ).toEqual(undefined);
  });
});
