import { describe, expect, test } from 'vitest';
import { parseActualContentConstraints } from '../src/';

describe('parseActualContentConstraints', () => {
  const structure = {
    data: {
      contentConstraints: [
        {
          type: 'Actual',
          cubeRegions: [
            {
              isIncluded: true,
              keyValues: [
                {
                  id: 'COU',
                  values: ['AUS', 'CAN', 'NZL'],
                },
                {
                  id: 'FREQ',
                  values: ['A'],
                },
              ],
            },
          ],
        },
      ],
    },
  };

  test('content constraints', () => {
    expect(parseActualContentConstraints()(structure)).toEqual({
      COU: new Set(['AUS', 'CAN', 'NZL']),
      FREQ: new Set(['A']),
    });
  });

  test('content constraints end period undefined', () => {
    expect(
      parseActualContentConstraints()({
        data: {
          contentConstraints: [
            {
              type: 'Actual',
              cubeRegions: [
                {
                  isIncluded: true,
                  keyValues: [
                    {
                      id: 'TIME_PERIOD',
                      timeRange: {
                        startPeriod: {
                          period: '1990-01-01T00:00:00Z',
                          isInclusive: false,
                        },
                      },
                    },
                  ],
                },
              ],
            },
          ],
        },
      }),
    ).toEqual({
      TIME_PERIOD: {
        boundaries: ['1990-01-01T00:00:00Z', undefined],
        includingBoundaries: [false, undefined],
      },
    });
  });

  test('content constraints period fully undefined', () => {
    expect(
      parseActualContentConstraints()({
        data: {
          contentConstraints: [
            {
              type: 'Actual',
              cubeRegions: [
                {
                  isIncluded: true,
                  keyValues: [
                    {
                      id: 'TIME_PERIOD',
                    },
                  ],
                },
              ],
            },
          ],
        },
      }),
    ).toEqual({
      TIME_PERIOD: {
        boundaries: [undefined, undefined],
        includingBoundaries: [undefined, undefined],
      },
    });
  });

  test('no content constraints', () => {
    expect(parseActualContentConstraints()({ data: {} })).toEqual(null);
    expect(parseActualContentConstraints()({ data: { contentConstraints: [] } })).toEqual(null);
    expect(
      parseActualContentConstraints()({ data: { contentConstraints: [{ type: 'toto' }] } }),
    ).toEqual(null);
    expect(
      parseActualContentConstraints()({ data: { contentConstraints: [{ type: 'Actual' }] } }),
    ).toEqual({});
  });

  test('should reject constraints with more keys than the limit if specified', () => {
    expect(parseActualContentConstraints({ limit: 1 })(structure)).toEqual({
      FREQ: new Set(['A']),
    });
  });
});
