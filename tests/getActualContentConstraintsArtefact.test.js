import { describe, expect, test } from 'vitest';
import { getActualContentConstraintsArtefact } from '../src';

describe('getActualContentConstraintsArtefact tests', () => {
  test('complete test', () => {
    const sdmxJson = {
      data: {
        contentConstraints: [
          {
            annotations: [{ id: 'constraint0' }],
            validTo: '1950-01-01T00:00:00Z',
            type: 'Actual',
          },
          {
            annotations: [{ id: 'constraint1' }],
            validFrom: '1950-01-01T00:00:00Z',
            validTo: '1975-01-01T00:00:00Z',
            type: 'Actual',
          },
          {
            annotations: [{ id: 'constraint2' }],
            validFrom: '1975-01-01T00:00:00Z',
            validTo: '2050-01-01T00:00:00Z',
            type: 'Actual',
          },
          {
            annotations: [{ id: 'constraint3' }],
            validFrom: '2050-01-01T00:00:00Z',
            type: 'Actual',
          },
        ],
      },
    };
    expect(getActualContentConstraintsArtefact(sdmxJson)).toEqual({
      annotations: [{ id: 'constraint2' }],
      validFrom: '1975-01-01T00:00:00Z',
      validTo: '2050-01-01T00:00:00Z',
      type: 'Actual',
    });
  });
  test('actual typo test', () => {
    const sdmxJson = {
      data: {
        contentConstraints: [
          {
            annotations: [{ id: 'constraint0' }],
            validTo: '1950-01-01T00:00:00Z',
            type: 'actual',
          },
          {
            annotations: [{ id: 'constraint1' }],
            validFrom: '1950-01-01T00:00:00Z',
            validTo: '1975-01-01T00:00:00Z',
            type: 'actual',
          },
          {
            annotations: [{ id: 'constraint2' }],
            validFrom: '1975-01-01T00:00:00Z',
            validTo: '2050-01-01T00:00:00Z',
            type: 'actual',
          },
          {
            annotations: [{ id: 'constraint3' }],
            validFrom: '2050-01-01T00:00:00Z',
            type: 'actual',
          },
        ],
      },
    };
    expect(getActualContentConstraintsArtefact(sdmxJson)).toEqual({
      annotations: [{ id: 'constraint2' }],
      validFrom: '1975-01-01T00:00:00Z',
      validTo: '2050-01-01T00:00:00Z',
      type: 'actual',
    });
  });
  test('should be resilient to no contentConstraints', () => {
    expect(getActualContentConstraintsArtefact()).toEqual(undefined);
  });
});
