import fs from 'fs';
import { join } from 'path';
import { getDirname, copyCb } from './utils.js';

const __dirname = getDirname();

const _public = 'public';
fs.rm(_public, { recursive: true, force: true }, (err) => {
  if (err) return console.error(`Error deleting ${_public}:`, err);
  console.log(`${_public} is deleted`);

  fs.cp(
    join(__dirname, 'docs/.vitepress/index.md'),
    join(__dirname, 'docs/index.md'),
    { recursive: false },
    copyCb('index.md'),
  );

  fs.cp(
    join(__dirname, 'README.md'),
    join(__dirname, 'docs/readme.md'),
    { recursive: false },
    copyCb('README.md'),
  );
});
