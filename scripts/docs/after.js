import fs from 'fs';
import { join } from 'path';
import { getDirname, copyCb } from './utils.js';

const __dirname = getDirname();

fs.cp(
  join(__dirname, 'docs/.vitepress/assets'),
  join(__dirname, 'public/assets'),
  { recursive: true },
  copyCb('assets'),
);
