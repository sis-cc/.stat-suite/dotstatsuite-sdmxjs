import { dirname, join } from 'path';
import { fileURLToPath } from 'url';

export const getDirname = () => {
  // Get __dirname equivalent for ES6 modules
  const __filename = fileURLToPath(import.meta.url);
  const __dirname = join(dirname(__filename), '../../');

  return __dirname;
};

export const copyCb = (file) => (err) => {
  if (err) return console.error(`Error copying ${file}:`, err);
  console.log(`${file} is copied`);
};
