# dotstatsuite-sdmxjs

A library that collects sdmx parsers with proper unit tests and documentation.

Currently used in [data-explorer](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-explorer) and [data-lifecycle-manager](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager) applications.

## Prerequisites

[Node.js](http://nodejs.org/), see package.json for version

## Setup

* `yarn` to install dependencies and devDependencies

## Test

* `yarn test` to run the tests with coverage
* `yarn test:watch` to run the tests during dev

## Lint

* `yarn lint` to lint all the code with the possibility to fix errors automatically, including prettier rules

_note: automatically run against staged files upon commit_

## Build

* `yarn build` to build the lib in2 formats (esm and cjs)

## Documentation

* `yarn docs:lint` to lint comments in all the code following jsdoc standard
* `yarn docs:generate` to geneerate the documentation api.md file from source code
* `yarn docs:build` to build all required static files for documentation website
* `yarn docs:preview` to launch a dev server to preview built documentation
* `yarn docs` to prepare docs from A to Z (before script, geenerate doc, build website, after script)

_note: run docs before checking documentation website with preview_

## Technical stack

- ESM compliant: ECMAScript modules are [the official standard](https://tc39.es/ecma262/#sec-modules) format to package JavaScript code for reuse. Modules are defined using a variety of [import](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import) and [export](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/export) statements.
- UMD compliant: UMD (Universal Module Definition) is a popular pattern in JavaScript for writing modules that work across different module-loading systems, such as CommonJS, AMD, and as a global variable for direct browser use. This allows a single JavaScript module to be compatible with multiple JavaScript environments, like Node.js, browser scripts, and AMD loaders.
- Powered by [Vite](https://vite.dev/):
Vite is a modern build tool for frontend JavaScript applications, designed to provide fast development and optimized production builds. Created by Evan You, the creator of Vue.js, Vite focuses on addressing performance issues that traditional bundlers (like Webpack) may encounter, especially for large projects.
- Bundled with [Rollup](https://rollupjs.org/) (no babel nor webpack): Rollup is a JavaScript module bundler, commonly used for bundling libraries or applications with a focus on efficient, optimized output. Unlike other bundlers (like Webpack) which are often used for complex web applications, Rollup is especially well-suited for bundling JavaScript libraries, where minimal, tree-shaken code is crucial. It outputs clean, optimized ES modules (ESM), making it an excellent choice for modern JavaScript projects, especially when targeting various environments, including browsers and Node.js.
- Tested with [Vitest](https://vitest.dev/) (no jest): Vitest is a modern testing framework specifically designed to work seamlessly with the Vite build tool, although it can be used independently as well. Created by the Vite team, Vitest provides a fast, developer-friendly testing environment that supports Vite’s ecosystem, allowing for a streamlined setup and enhanced performance, especially for projects that already use Vite. It has quickly become popular for testing in Vue, React, and other frontend frameworks that benefit from Vite's speed and efficiency.
- Documented by [Documentation.js](https://documentation.js.org/) following [JSDoc](https://jsdoc.app/) standard: Documentation.js is a documentation generator for JavaScript projects, designed to simplify creating and maintaining high-quality documentation. It parses comments directly from the source code, supporting JSDoc-style comments, and produces documentation in various formats, including HTML, Markdown, and JSON.
- Documentation website powered by [VitePress](https://vitepress.dev/): VitePress is a static site generator powered by Vite and Vue, specifically designed for building documentation sites. It’s built by the Vue team and is the successor to VuePress, designed to offer a lighter, faster, and simpler experience using Vite’s fast dev server and modern bundling capabilities. VitePress is optimized for markdown-based content, making it an ideal choice for project documentation, technical blogs, and knowledge bases.
- Linted & beautiful with Eslint and Prettier