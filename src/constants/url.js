export const ALL_IDENTIFIER = 'all';

export const DATAQUERY_VALUES_SEPARATOR = '+';
export const DATAQUERY_DIMENSIONS_SEPARATOR = '.';
