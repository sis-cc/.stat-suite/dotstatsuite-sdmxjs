import * as R from 'ramda';

export const STRUCTURE_ATTRIBUTE_LIST_PATH = [
  'data',
  'dataStructures',
  0,
  'dataStructureComponents',
  'attributeList',
];

export const STRUCTURE_ATTRIBUTES_PATH = R.append(
  'attributes',
  STRUCTURE_ATTRIBUTE_LIST_PATH,
);
