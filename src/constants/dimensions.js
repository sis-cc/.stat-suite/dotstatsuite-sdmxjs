import * as R from 'ramda';

export const STRUCTURE_DIMENSION_LIST_PATH = [
  'data',
  'dataStructures',
  0,
  'dataStructureComponents',
  'dimensionList',
];

export const STRUCTURE_DIMENSIONS_PATH = R.append(
  'dimensions',
  STRUCTURE_DIMENSION_LIST_PATH,
);

export const STRUCTURE_TIME_DIMENSION_PATH = R.concat(
  STRUCTURE_DIMENSION_LIST_PATH,
  ['timeDimensions', 0],
);

export const FREQUENCY_IDS = ['FREQ', 'FREQUENCY'];

export const TIME_PERIOD_ROLE = 'TIME_PERIOD';
export const TIME_PERIOD_ID = 'TIME_PERIOD';

export const REF_AREA_ROLES = ['GEO', 'REF_AREA'];
export const REF_AREA_IDS = [
  'REF_AREA',
  'COUNTRY',
  'LOCATION',
  'REGION',
  'REFERENCE_AREA',
];
