export const SUPPORTED_FORMAT = {
  P1Y: 'A',
  P6M: 'S',
  P3M: 'Q',
  P1M: 'M',
  P7D: 'W',
  P1D: 'D',
  PT1M: 'N',
};

export const FREQUENCY_INTERVALS = {
  A2: { years: 2, months: 0, days: 0, hours: 0, minutes: 0, seconds: 0 },
  A: { years: 1, months: 0, days: 0, hours: 0, minutes: 0, seconds: 0 },
  S: { years: 0, months: 6, days: 0, hours: 0, minutes: 0, seconds: 0 },
  Q: { years: 0, months: 3, days: 0, hours: 0, minutes: 0, seconds: 0 },
  M: { years: 0, months: 1, days: 0, hours: 0, minutes: 0, seconds: 0 },
  W: { years: 0, months: 0, days: 7, hours: 0, minutes: 0, seconds: 0 },
  D: { years: 0, months: 0, days: 1, hours: 0, minutes: 0, seconds: 0 },
  H: { years: 0, months: 0, days: 0, hours: 1, minutes: 0, seconds: 0 },
  N: { years: 0, months: 0, days: 0, hours: 0, minutes: 1, seconds: 0 },
};

export const FREQUENCY_DIMENSION_IDS = ['FREQ', 'FREQUENCY'];
export const FREQUENCY_DIMENSION_ROLES = ['FREQ'];
export const FREQUENCY_ATTRIBUTE_IDS = ['TIME_FORMAT'];
