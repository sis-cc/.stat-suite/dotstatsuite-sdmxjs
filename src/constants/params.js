export const OVERVIEW_DETAIL_PARAM = 'allcompletestubs';
// useful in order to lighten response size for a list of several artefacts, i.e. for codelists query, only codelists details returned, not the codes

export const PARTIAL_REF_DETAIL_PARAM = 'referencepartial';
// useful feature very new, allowing to only get relevant content, i.e. for a DSD query, for referenced codelists, only the actual codes used will be returned, except of all

export const PARENTS_REF_PARAM = 'parents';
// for all artefacts matching the query, returns the artefacts refencing these,

export const PARENTS_AND_SIBLINGS_REF_PARAM = 'parentsandsiblings';
// for all artefacts matching the query, returns the artefacts refencing these, and the  other artefacts refenced by them

export const CHILDREN_REF_PARAM = 'children';
// for all artefacts matching the query, returs as well all the artefacts referenced by these

export const DESCENDANTS_REF_PARAM = 'descendants';
// for all artefacts matching the query, returs as well all the artefacts referenced reccursively (children of children)

export const ALL_REF_PARAM = 'all';
// combination of descendants plus parents and siblings

export const DIMOBS_PARAM_KEY = 'dimensionAtObservation';
export const START_PERIOD_PARAM_KEY = 'startPeriod';
export const END_PERIOD_PARAM_KEY = 'endPeriod';
export const LASTNOBS_PARAM_KEY = 'lastNObservations';
export const LAST_N_PERIODS_PARAM_KEY = 'lastNPeriods';

export const DATA_REQUEST_PARAMS_KEYS = [
  DIMOBS_PARAM_KEY,
  START_PERIOD_PARAM_KEY,
  END_PERIOD_PARAM_KEY,
  LASTNOBS_PARAM_KEY,
  LAST_N_PERIODS_PARAM_KEY,
];

export const DIMOBS_PARAM = { key: DIMOBS_PARAM_KEY, value: 'AllDimensions' };
// mandatory for data parsing
