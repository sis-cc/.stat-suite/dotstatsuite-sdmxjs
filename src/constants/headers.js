export const DEFAULT_FORMATS = {
  data: {
    csv: 'application/vnd.sdmx.data+csv',
    json: 'application/vnd.sdmx.data+json;version=1.0.0-wd',
    xml: 'application/vnd.sdmx.genericdata+xml;version=2.1',
  },
  structure: {
    json: 'application/vnd.sdmx.structure+json;version=1.0',
    xml: 'application/vnd.sdmx.structure+xml;version=2.1',
  },
};

export const FORMAT_SEPARATOR = ';';

export const FILE_FORMAT = 'file=true';
export const ZIP_FORMAT = 'zip=true';
export const URN_FORMAT = 'urn=true';
