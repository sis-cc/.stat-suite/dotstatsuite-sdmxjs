export const AGENCY = 'agency';
export const AGENCY_SCHEME = 'agencyscheme';
export const ALL = 'all';
export const ATTACHMENT_CONSTRAINT = 'attachmentconstraint';
export const AVAILABLE_CONSTRAINT = 'availableconstraint';
export const CATEGORISATION = 'categorisation';
export const CATEGORY_SCHEME = 'categoryscheme';
export const CODELIST = 'codelist';
export const CONCEPT_SCHEME = 'conceptscheme';
export const CONTENT_CONSTRAINT = 'contentconstraint';
export const DATA = 'data';
export const DATAFLOW = 'dataflow';
export const DATAFLOWS = 'dataflows';
export const DATA_CONSUMER_SCHEME = 'dataconsumerscheme';
export const DATA_PROVIDER_SCHEME = 'dataproviderscheme';
export const DATA_STRUCTURE = 'datastructure';
export const HIERARCHICAL_CODELIST = 'hierarchicalcodelist';
export const METADATAFLOW = 'metadataflow';
export const METADATA_STRUCTURE = 'metadatastructure';
export const ORGANISATION_SCHEME = 'organisationscheme';
export const PROCESS = 'process';
export const PROVISION_AGREEMENT = 'provisionagreement';
export const REPORTING_TAXONOMY = 'reportingtaxonomy';
export const STRUCTURE = 'structure';
export const STRUCTURE_SET = 'structureset';

export const CATEGORY_SCHEME_RESULTS = 'categorySchemes';
export const CATEGORY_RESULTS = 'categories';

export const LISTS = {
  agencyscheme: 'agencySchemes',
  categoryscheme: 'categorySchemes',
  codelist: 'codelists',
  conceptscheme: 'conceptSchemes',
  contentconstraint: 'contentConstraints',
  dataflow: 'dataflows',
  datastructure: 'dataStructures',
};
