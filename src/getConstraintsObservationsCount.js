/**
 * @description
 * A function that takes a sdmx json message and returns the total observations of a dataflow from its
 * content constraints annotations, or undefined if non present.
 *
 * @name getConstraintsObservationsCount
 * @function
 * @public
 * @param {Object} SDMX json
 * @return {number} 50
 *
 * @example
 * const sdmxJson = {
 *   data: {
 *     contentConstraints: [{
 *       annotations: [{ id: 'obs_count', type: 'sdmx_metrics', title: '50' }],
 *       type: 'Actual',
 *     }]
 *   }
 * };
 * SDMXJS.getConstraintsObservationsCount(sdmxJson); //=> 50
 */

import * as R from 'ramda';
import { getActualContentConstraintsArtefact } from './getActualContentConstraintsArtefact';

export const getConstraintsObservationsCount = R.pipe(
  getActualContentConstraintsArtefact,
  R.propOr([], 'annotations'),
  R.find(
    (annot) =>
      R.propEq('sdmx_metrics', 'type', annot) &&
      R.propEq('obs_count', 'id', annot),
  ),
  R.prop('title'),
  (value) => Number(value),
  R.when(isNaN, R.always(undefined)),
);
