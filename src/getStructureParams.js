/*
 * A function that takes a set of arguments (see getRequestArgs), and returns the corresponding
 * structure request params.
 *
 * @private
 * @param {Object}
 * @return {Object}
 *
 * @example
 * getStructureParams({ identifiers: { agencyId: 'A', code: 'all', version: 'latest' }, overview: true });
 * //=> { detail: allstubs, references: 'all' }
 * const identifiers = { agencyId: 'A', code: 'I', version: 'latest' };
 * getStructureParams({ identifiers }); //=> { references: 'all' }
 * getStructureParams({ identifiers, datasource: { supportsReferencePartial: true } });
 * //=> { references: 'all', detail: 'referencepartial' }
 */
import * as R from 'ramda';
import {
  ALL_REF_PARAM,
  OVERVIEW_DETAIL_PARAM,
  PARTIAL_REF_DETAIL_PARAM,
  PARENTS_AND_SIBLINGS_REF_PARAM,
  PARENTS_REF_PARAM,
  CHILDREN_REF_PARAM,
} from './constants/params';

const getReferences = (args) => {
  if (R.prop('withParents', args)) {
    return { references: PARENTS_REF_PARAM };
  }
  if (R.prop('withParentsAndSiblings', args)) {
    return { references: PARENTS_AND_SIBLINGS_REF_PARAM };
  }
  if (R.prop('withChildren', args)) {
    return { references: CHILDREN_REF_PARAM };
  }
  if (R.propOr(true, 'withReferences', args)) {
    return { references: ALL_REF_PARAM };
  }
  return {};
};

const getDetails = (args) => {
  if (R.prop('overview', args)) {
    return { detail: OVERVIEW_DETAIL_PARAM };
  }
  if (
    R.path(['datasource', 'supportsReferencePartial'], args) &&
    R.prop('withPartialReferences', args)
  ) {
    return { detail: PARTIAL_REF_DETAIL_PARAM };
  }
  return {};
};

export const getStructureParams = (args) => {
  const customParams = R.propOr({}, 'params', args);
  const defaultParams = R.converge(R.mergeRight, [getReferences, getDetails])(
    args,
  );
  return R.mergeLeft(customParams, defaultParams);
};
