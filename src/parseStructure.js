import * as R from 'ramda';
import { getDefaultDataParams } from './getDefaultDataParams';
import { getDefaultSelection } from './getDefaultSelection';
import { getDefaultTableLayout } from './getDefaultTableLayout';
import getAttributes from './getAttributes';
import getDimensions from './getDimensions';
import { getDataflowName } from './getDataflowName';
import { getTimePeriod } from './getTimePeriod';
import { getExternalResources } from './getExternalResources';
import parseActualContentConstraints from './parseActualContentConstraints';
import { parseMicrodataAnnotation } from './parseMicrodataAnnotation';
import { constrainDimensions } from './constrainDimensions';

export const parseStructure = (structure) => {
  let contentConstraints = parseActualContentConstraints()(structure);

  // find microdata dimension id, if exists rely on CC to restrict microdata dimension values
  const microdataDimensionId = parseMicrodataAnnotation()(structure);
  if (microdataDimensionId)
    contentConstraints[microdataDimensionId] = new Set(['_T']);

  return {
    contentConstraints,
    dimensions: R.pipe(getDimensions, (dimensions) =>
      constrainDimensions(dimensions, contentConstraints),
    )(structure),
    attributes: getAttributes(structure),
    params: getDefaultDataParams(structure),
    selection: getDefaultSelection(structure),
    layout: getDefaultTableLayout(structure),
    timePeriod: getTimePeriod(structure),
    name: getDataflowName(structure),
    externalResources: getExternalResources(structure),
    microdataDimensionId,
  };
};
