/**
 * @description
 * Wraps a call to the `parseActualContentConstraints` function with deprecation warning.
 *
 * This function is deprecated since v5.5.4. Use `parseActualContentConstraints` directly.
 *
 * @name getActualContentConstraints
 * @function
 * @public
 * @param {Object} [options={}] - Configuration options to pass to `parseActualContentConstraints`.
 *
 * @returns {Function} A function that takes a `structure` parameter and calls
 * `parseActualContentConstraints` on it after displaying a deprecation warning.
 *
 */

import parseActualContentConstraints from './parseActualContentConstraints';

export default (options = {}) =>
  (structure) => {
    // eslint-disable-next-line no-console
    console.warn(
      '[deprecation warning]: getActualContentConstraints has been renamed to parseActualContentConstraints',
    );
    parseActualContentConstraints(options)(structure);
  };
