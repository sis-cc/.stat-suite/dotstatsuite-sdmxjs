/**
 * @description
 * This function is a simplification of getCodeLists
 * See 'getCodeLists' definition for more specifications.
 *
 * @name getAttributes
 * @function
 * @public
 * @param path {Object}
 * @return {Array}
 *
 * @example
 * SDMXJS.Attributes(structure);
 */

import { STRUCTURE_ATTRIBUTES_PATH } from './constants/attributes';
import getCodeLists from './getCodeLists';

export default getCodeLists(STRUCTURE_ATTRIBUTES_PATH);
