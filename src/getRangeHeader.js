/*
 * A function that takes a set of arguments (see getRequestArgs) and returns the parsed
 * Range header in case of a data request.
 *
 * @private
 * @param {Object}
 * @return {Object}
 *
 * @example
 * getRangeHeader({}); //=> {}
 * getRangeHeader({ type: 'data' }); //=> {}
 * getRangeHeader({ type: 'other', range: [0, 24] }); //=> {}
 * getRangeHeader({ type: 'data', range: [0, 24] }); //=> { x-range: 'values=0-24' }
 * getRangeHeader({ type: 'data', range: 25 }); //=> { x-range: 'values=0-24' }
 */
import * as R from 'ramda';
import { isDataRequest } from './isDataRequest';

export const getRangeHeader = (args) => {
  const rangeArg = R.prop('range', args);
  if (
    R.isNil(rangeArg) ||
    R.not(isDataRequest(args)) ||
    R.not(R.path(['datasource', 'hasRangeHeader']))
  ) {
    return {};
  }

  const rangeValue = R.pipe(
    R.when(R.pipe(R.is(Array), R.not), R.of(Array)),
    R.when(
      R.pipe(R.length, R.equals(1)),
      R.pipe(R.prepend('0'), R.over(R.lensIndex(1), R.dec)),
    ),
    R.converge((start, end) => `values=${start}-${end}`, [R.head, R.last]),
  )(rangeArg);

  return { 'x-range': rangeValue };
};
