import * as R from 'ramda';

export const getArtefactTypeFromListType = (listType) =>
  R.pipe(R.toLower, R.dropLast(1))(listType);
