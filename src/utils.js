import * as R from 'ramda';
import { FREQUENCY_IDS } from './constants/dimensions';

export const setParam = (param) => R.assoc(param.key, param.value);

export const isFrequency = R.pipe(
  R.prop('id'),
  R.flip(R.includes)(FREQUENCY_IDS),
);

export const cartesian = R.reduce(
  (acc, array) =>
    R.unnest(R.map((x) => R.map((y) => R.append(y, x), array), acc)),
  [[]],
);
