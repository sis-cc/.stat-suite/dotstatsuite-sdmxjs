/**
 * @description
 * A function that takes a lists of parsed dimensions and parsed attributes, and returns the current Frequency value.
 * This function will search the Frequency Artefact from the dimensions and attributes (see `getFrequencyArtefact`) to returns the
 * id of the first value.
 * If no Frequency Artefact can be found, the function will then search the Time Period Dimension (see `getTimePeriodDimension`), and then
 * will deduce the Frequency value from parsing the period interval of the first Time Period value, given its `start` and `end` properties.
 *
 * @public
 * @name getDataFrequency
 * @param {Object}
 * @return {string}
 *
 * @example
 * const dimensions = [
 *   { id: 'INDICATOR', role: 'SUBJECT', values: [{ id: 'IND1' }, { id: 'IND2' }] },
 *   { id: 'COU', role: 'GEO', values: [{ id: 'GER' }, { id: 'FRA' }] },
 *   { id: 'TIME_PERIOD', role: null, values: [{ id: '2020', start: '2020-01-01T00:00:00Z', end: '2020-12-31T23:59:59Z' }] },
 * ];
 * const attributes = [
 *   { id: 'UNIT_MEASURE', values: [] },
 *   { id: 'TIME_FORMAT', values: [{ id: 'P1Y' }] }
 * ];
 * SDMXJS.getDataFrequency({ dimensions, attributes }); //=> 'A'
 * SDMXJS.getDataFrequency({ dimensions, attributes: [] }); //=> 'A'
 */
import * as R from 'ramda';
import intervalToDuration from 'date-fns/intervalToDuration';
import addSeconds from 'date-fns/addSeconds';
import addHours from 'date-fns/addHours';
import subHours from 'date-fns/subHours';
import { getFrequencyArtefact } from './getFrequencyArtefact';
import { getTimePeriodDimension } from './getTimePeriodDimension';
import { FREQUENCY_INTERVALS } from './constants/frequency';

export const getDataFrequency = ({ attributes, dimensions }) => {
  const frequencyArtefact = getFrequencyArtefact({ attributes, dimensions });
  if (!R.isNil(frequencyArtefact)) {
    return R.pipe(
      R.propOr([], 'values'),
      R.head,
      R.prop('id'),
    )(frequencyArtefact);
  }
  const timePeriod = getTimePeriodDimension({ dimensions });
  if (R.isNil(timePeriod)) {
    return undefined;
  }
  return R.pipe(
    R.path(['values', 0]),
    ({ start, end }) => {
      // dirty way to handle the possibility of a switch to summer or winter time during the interval ...
      const baseEnd = addSeconds(new Date(end), 1);
      const endMinusOneHour = subHours(baseEnd, 1);
      const endPlusOneHour = addHours(baseEnd, 1);
      return R.reduce(
        (acc, endDate) => {
          const interval = intervalToDuration({
            start: new Date(start),
            end: endDate,
          });
          const matchIntervals = R.pickBy(
            R.whereEq(interval),
            FREQUENCY_INTERVALS,
          );
          return { ...acc, ...matchIntervals };
        },
        {},
        [baseEnd, endMinusOneHour, endPlusOneHour],
      );
    },
    R.keys,
    R.head,
  )(timePeriod);
};
