/*
 * A function that takes the dataflow level default annotation, and returns the
 * default constraints set whithin it.
 * Only first annotation of type DEFAULT considered.
 *
 * @private
 * @param {Object}
 * @return {Object}
 *
 * @example
 * const structure = {
 *   data: { dataflows: [{
 *     annotations: [
 *       { type: 'RANDOM' },
 *       { type: 'DEFAULT', title: 'DIM3=CODE1+CODE2,DIM6=CODE,TIME_PERIOD_START=2013-01,TIME_PERIOD_END=2018-12,LASTNOBSERVATIONS' },
 *       { type: 'DEFAULT', title: 'IGNORED=IGNORED' }
 *     ]
 *   }] }
 * };
 *
 * SDMXJS.parseDefaultAnnotation(structure);
 * //=>  {
 * //      DIM3: 'CODE1+CODE2',
 * //      DIM6: 'CODE',
 * //      startPeriod: '2013-01',
 * //      endPeriod: '2018-12',
 * //      lastNObservations: '1'
 * //    };
 */
import * as R from 'ramda';
import {
  DATAFLOW_ANNOTATIONS_PATH,
  DEFAULT_ANNOT_TYPE,
  DEFAULT_PARAMS,
  DEFAULT_LASTNOBS_VAL,
} from './constants/annotations';
import { LASTNOBS_PARAM_KEY } from './constants/params';

export const parseDefaultAnnotation = (structure) =>
  R.pipe(
    R.pathOr([], DATAFLOW_ANNOTATIONS_PATH),
    R.find(R.propEq(DEFAULT_ANNOT_TYPE, 'type')),
    R.ifElse(R.isNil, R.always({}), R.identity),
    R.propOr('', 'title'),
    R.split(','),
    R.reduce((acc, entry) => {
      const splitEntry = R.pipe(
        R.split('='),
        R.ifElse(
          R.pipe(R.head, R.flip(R.has)(DEFAULT_PARAMS)),
          R.over(R.lensIndex(0), R.flip(R.prop)(DEFAULT_PARAMS)),
          R.identity,
        ),
        R.ifElse(
          R.allPass([
            R.pipe(R.length, R.equals(1)),
            R.pipe(R.head, R.equals(LASTNOBS_PARAM_KEY)),
          ]),
          R.append(DEFAULT_LASTNOBS_VAL),
          R.identity,
        ),
      )(entry);
      return {
        ...acc,
        [R.head(splitEntry)]: R.last(splitEntry),
      };
    }, {}),
  )(structure);
