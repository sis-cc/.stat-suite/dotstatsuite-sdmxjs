/*
 * A function that given the proper type ans identifiers
 * will return the formated sdmxQuery
 *
 * @public
 * @param {Object}
 * @return {string}
 *
 * @example
 * const args = {
 *   type: dataflow,
 *   identifiers: { agencyId: 'OECD', code: 'DF', version: '1.0' },
 * };
 *
 * SDMXJS.getStructureRequestQuery(args);
 * //=> 'dataflow/OECD/DF/1.0',
 */
import * as R from 'ramda';

export const getStructureRequestQuery = ({ type, identifiers }) => {
  const { agencyId, code, version = '', subcode } = identifiers;
  return `${type}/${agencyId}/${code}/${version}${R.isNil(subcode) ? '' : `/${subcode}`}`;
};
