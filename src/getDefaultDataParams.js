/**
 * @description
 * A function that takes a dataflow structure and returns the default params
 * to be used for a sdmx-data request. This information is parsed inside the
 * dataflow level default annotation, see parseDefaultAnnotation.
 *
 * @public
 * @name getDefaultDataParams
 * @param {Object}
 * @return {Object}
 *
 * @example
 * const structure = {
 *   data: { dataflows: [{
 *     annotations: [
 *       { type: 'RANDOM' },
 *       { type: 'DEFAULT', title: 'DIM3=CODE1+CODE2,DIM6=CODE,TIME_PERIOD_START=2013-01,TIME_PERIOD_END=2018-12,LASTNOBSERVATIONS' },
 *       { type: 'DEFAULT', title: 'IGNORED=IGNORED' }
 *     ]
 *   }] }
 * };
 *
 * SDMXJS.parseDefaultAnnotation(structure);
 * //=>  {
 * //      startPeriod: '2013-01',
 * //      endPeriod: '2018-12',
 * //      lastNObservations: '1'
 * //    };
 */
import * as R from 'ramda';
import { parseDefaultAnnotation } from './parseDefaultAnnotation';
import { parseDataParams } from './parseDataParams';

export const getDefaultDataParams = (structure) =>
  R.pipe(parseDefaultAnnotation, parseDataParams)(structure);
