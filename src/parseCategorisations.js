import * as R from 'ramda';

export const parseCategorisations = ({ data = {} }) =>
  R.pipe(
    R.propOr([], 'categorisations'),
    R.reduce((acc, cat) => {
      const artefactUrn = R.propOr('', 'source', cat);
      const matchArtefact = artefactUrn.match(/=([\w@_.]+:[\w@_]+\([\d.]+\))$/);
      if (R.isNil(matchArtefact)) {
        return acc;
      }
      const categoryUrn = R.propOr('', 'target', cat);
      const matchCategory = categoryUrn.match(
        /=([\w@_.]+:[\w@_]+\([\d.]+\)).([\w@_.]+)$/,
      );
      if (R.isNil(matchCategory)) {
        return acc;
      }
      const [, artefactId] = matchArtefact;
      const [, schemeId, path] = matchCategory;

      return R.over(
        R.lensProp(artefactId),
        R.ifElse(
          R.isNil,
          R.always([{ schemeId, path }]),
          R.append({ schemeId, path }),
        ),
      )(acc);
    }, {}),
  )(data);
