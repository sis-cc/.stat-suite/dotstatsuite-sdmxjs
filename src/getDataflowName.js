/**
 * @description
 * A function that takes a structure and will return the corresponding name of the dataflow, or the id if not found.
 *
 * @public
 * @name getDataflowName
 * @param {Object}
 * @param {string}
 * @return {Object}
 *
 * @example
 * const structure1 = {
 *   data: {
 *     dataflows: [{
 *       id: 'DF1',
 *       name: 'dataflow_name',
 *     }]
 *   }
 * };
 *
 * const structure2 = {
 *   data: {
 *     dataflows: [{
 *       id: 'DF2',
 *     }]
 *   }
 * };
 *
 * SDMXJS.getDataflowName(structure1); //=> 'dataflow_name'
 * SDMXJS.getDataflowName(structure2); //=> '[DF2]'
 */
import * as R from 'ramda';

export const getDataflowName = (structure) => {
  const dataflow = R.pathOr({}, ['data', 'dataflows', 0], structure);
  return R.prop('name')(dataflow);
};
