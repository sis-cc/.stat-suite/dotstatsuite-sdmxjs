/**
 * @description
 * A parser that given an SDMX Artefacts Trees Response (like Category Schemes) and parsing options,
 * will parse the hierarchical trees into a flat list in an object;
 *
 * @public
 * @name parseTreeResultsAsList
 * @param {Object}
 * @return {Object}
 *
 * @example
 * const sdmxJson = {
 *   data: {
 *     schemes: [{
 *       id: '2',
 *       names: { ar: 'L2' },
 *       agencyID: 'SIS-CC',
 *       version: 'x.x',
 *       items: [
 *         { id: '2A' },
 *         { id: '2B', name: 'L2VB' },
 *       ],
 *     }],
 *   }
 * };
 *
 * const parsedTree = SDMXJS.parseTreeResultsAsList({ lists: 'schemes', listValues: 'items' })(sdmxJson);
 * //=> {
 * //     'SIS-CC:2(x.x)': { id: 'SIS-CC:2(x.x)', code: '2', label: '[2]', agencyId: 'SIS-CC', version: 'x.x' },
 * //     'SIS-CC:2(x.x).2A': { id: 'SIS-CC:2(x.x).2A', code: '2A', label: '[2A]', agencyId: undefined, version: undefined, parentId: 'SIS-CC:2(x.x)', topParentId: 'SIS-CC:2(x.x)' },
 * //     'SIS-CC:2(x.x).2B': { id: 'SIS-CC:2(x.x).2B', code: '2B', label: 'L2VB', agencyId: undefined, version: undefined, parentId: 'SIS-CC:2(x.x)', topParentId: 'SIS-CC:2(x.x)' },
 * //    }
 */
import * as R from 'ramda';

export const parseTreeResultsAsList =
  ({ lists, listValues }) =>
  (sdmxJson) => {
    const getNodesData = ({ parentId, parentCode, topParentId }) =>
      R.reduce((acc, node) => {
        const label = R.prop('name')(node);
        const code = R.prop('id', node);
        const agencyId = R.prop('agencyID', node);
        const version = R.prop('version', node);
        const hierarchicalCode = R.when(
          R.always(parentCode && parentId !== topParentId),
          R.concat(`${parentCode}.`),
        )(code);
        const id = R.when(
          R.always(parentId),
          R.always(`${parentId}.${code}`),
        )(`${agencyId}:${code}(${version})`);

        const nodeData = R.pipe(
          R.when(R.always(parentId), R.assoc('parentId', parentId)),
          R.when(R.always(topParentId), R.assoc('topParentId', topParentId)),
        )({ agencyId, code, hierarchicalCode, id, label, version });

        const childNodes = getNodesData({
          parentId: id,
          parentCode: hierarchicalCode,
          topParentId: R.when(R.isNil, R.always(id))(topParentId),
        })(R.propOr([], listValues, node));

        return R.pipe(R.assoc(id, nodeData), R.mergeRight(childNodes))(acc);
      }, {});

    return R.pipe(R.pathOr([], ['data', lists]), getNodesData({}))(sdmxJson);
  };
