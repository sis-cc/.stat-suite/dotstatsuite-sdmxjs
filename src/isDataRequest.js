/*
 * A function that takes some request args and returns if this correspond to a data request or not.
 *
 * @private
 * @param {Object}
 * @return {Boolean}
 *
 * @example
 * SDMXJS.isDataRequest({ type: 'data' }); //=> true
 * SDMXJS.isDataRequest({ type: 'other' }); //=> false
 */
import * as R from 'ramda';
import { DATA, AVAILABLE_CONSTRAINT } from './constants/types';

export const isDataRequest = (args) =>
  R.or(
    R.propEq(DATA, 'type', args),
    R.propEq(AVAILABLE_CONSTRAINT, 'type', args),
  );
