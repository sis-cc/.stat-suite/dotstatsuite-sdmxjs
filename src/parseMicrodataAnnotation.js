/*
 * returns the microdata dimension id from a dataflow structure
 * type of microdata annotation is configurable, default value is DRILLDOWN
 * /!\ only the first annotation is considered.
 *
 * @private
 * @param {string} {Object}
 * @return {string}
 *
 * @example
 * const structure = {
 *   data: { dataflows: [{
 *     annotations: [
 *       { type: 'RANDOM' },
 *       { type: 'DRILLDOWN', title: 'MD_DIM' },
 *       { type: 'DRILLDOWN', title: 'IGNORED' },
 *     ]
 *   }] }
 * };
 *
 * SDMXJS.parseMicrodataAnnotation('DRILLDOWN')(structure);
 * SDMXJS.parseMicrodataAnnotation()(structure);
 * //=> 'MD_DIM'
 */

import * as R from 'ramda';
import { DATAFLOW_ANNOTATIONS_PATH, MICRODATA } from './constants/annotations';

export const parseMicrodataAnnotation = (type) => (structure) =>
  R.pipe(
    R.pathOr([], DATAFLOW_ANNOTATIONS_PATH),
    R.find(R.propEq(R.defaultTo(MICRODATA, type), 'type')),
    R.ifElse(R.isNil, R.always({}), R.identity),
    R.prop('title'),
  )(structure);
