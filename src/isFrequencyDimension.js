/**
 * @description
 * A function that takes a parsed dimension and returns if it corresponds to the Frequency Dimension or not.
 * The dimension can be recognised by having `FREQ` as `role`, or by having `FREQ` or `FREQUENCY` as `id`.
 *
 * @public
 * @name isFrequencyDimension
 * @param {Object}
 * @return {Boolean}
 *
 * @example
 * SDMXJS.isFrequencyDimension({ id: 'DIM', role: 'FREQ', values: [] }); //=> true
 * SDMXJS.isFrequencyDimension({ id: 'FREQUENCY', role: null, values: [] }); //=> true
 * SDMXJS.isFrequencyDimension({ id: 'INDICATOR', role: 'SUBJECT', values: [] }); //=> false
 */
import * as R from 'ramda';
import {
  FREQUENCY_DIMENSION_IDS,
  FREQUENCY_DIMENSION_ROLES,
} from './constants/frequency';

export const isFrequencyDimension = (dimension) => {
  const hasFrequencyRole = R.includes(
    R.prop('role', dimension),
    FREQUENCY_DIMENSION_ROLES,
  );
  if (hasFrequencyRole) {
    return true;
  }
  return R.includes(R.prop('id', dimension), FREQUENCY_DIMENSION_IDS);
};
