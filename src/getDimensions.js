/**
 * @description
 * This function is a simplification of getCodeLists
 * See 'getCodeLists' definition for more specifications.
 *
 * @name getDimensions
 * @function
 * @public
 * @param {Object}
 * @return {Array}
 *
 * @example
 * SDMXJS.getDimensions(structure);
 */

import { STRUCTURE_DIMENSIONS_PATH } from './constants/dimensions';
import getCodeLists from './getCodeLists';

export default getCodeLists(STRUCTURE_DIMENSIONS_PATH);
