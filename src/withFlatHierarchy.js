/**
 * @description
 * A function that takes a dimension and sort the values regarding their hierarchical order
 * (ordered children right after the parent). All children with missing parent will be ordered
 * at the root level.
 * Each child will be enriched with the ordered list of all of its parent ids under the
 * 'parents' key.
 *
 * @name withFlatHierarchy
 * @public
 * @param {Object}
 * @return {Object}
 *
 * @example
 * const dimension = {
 *   id: 'DIM',
 *   values: [
 *     { id: 1 },
 *     { id: 20, parent: 2 },
 *     { id: 10, parent: 1 },
 *     { id: 2 },
 *     { id: 110, parent: 11 },
 *     { id: 30, parent: 3 },
 *     { id: 11, parent: 1 },
 *     { id: 21, parent: 2 },
 *     { id: 111, parent: 11 },
 *     { id: 12, parent: 1 },
 *   ],
 * };
 *
 * SDMXJS.withFlatHierarchy(dimension);
 * //=> {
 * //=>   id: 'DIM',
 * //=>   values: [
 * //=>     { id: 1, parents: [] },
 * //=>     { id: 10, parent: 1, parents: [1] },
 * //=>     { id: 11, parent: 1, parents: [1] },
 * //=>     { id: 110, parent: 11, parents: [1, 11] },
 * //=>     { id: 111, parent: 11, parents: [1, 11] },
 * //=>     { id: 12, parent: 1, parents: [1] },
 * //=>     { id: 2, parents: [] },
 * //=>     { id: 20, parent: 2, parents: [2] },
 * //=>     { id: 21, parent: 2, parents: [2] },
 * //=>     { id: 30, parent: 3, parents: [] }
 * //=>   ],
 * //=> }
 */
import * as R from 'ramda';

export const withFlatHierarchy = (dimension) => {
  const indexedValues = R.indexBy(
    R.prop('id'),
    R.propOr([], 'values', dimension),
  );
  const grouped = R.pipe(
    R.propOr([], 'values'),
    R.groupBy(
      R.pipe(
        R.prop('parent'),
        R.when(
          R.anyPass([R.isNil, R.pipe(R.flip(R.has)(indexedValues), R.not)]),
          R.always('root'),
        ),
      ),
    ),
  )(dimension);

  const reccur = (parents) =>
    R.pipe(
      R.propOr([], R.last(parents)),
      R.map(
        R.pipe(R.assoc('parents', R.tail(parents)), (value) =>
          R.prepend(value, reccur(R.append(R.prop('id', value), parents))),
        ),
      ),
    )(grouped);

  const leveledValues = reccur(['root']);

  return R.assoc('values', R.flatten(leveledValues), dimension);
};
