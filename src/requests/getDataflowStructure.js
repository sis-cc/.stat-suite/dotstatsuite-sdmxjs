/**
 * @description
 * A function that given a set of arguments, returns a Promise, performing
 * a corresponding SDMX Dataflow Structure Request (see getRequestArgs).
 *
 * @name getDataflowStructure
 * @public
 * @param {Object}
 * @return {Promise}
 *
 * @example
 * const args = {
 *  ...
 * }; // see getRequestArgs for args definition. No need to specify 'type'
 *
 * SDMXJS.getDataflowStructure(args)
 *   .then(data => successCallback(data))
 *   .catch(error => errorCallback(error));
 */

import axios from 'axios';
import { DATAFLOW } from '../constants/types';
import { getRequestArgs } from '../getRequestArgs';

export const getDataflowStructure = (args) => {
  const { url, headers, params } = getRequestArgs({
    ...args,
    type: DATAFLOW,
  });

  return axios.get(url, { headers, params }).then((res) => res.data);
};
