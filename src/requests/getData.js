/**
 * @description
 * A function that given a set of arguments, returns a Promise, performing
 * a corresponding SDMX Data Request (see getRequestArgs).
 *
 * @public
 * @name getData
 * @param {Object}
 * @return {Promise}
 *
 * @example
 * const args = {
 *  ...
 * }; // see getRequestArgs for args definition. No need to specify 'type'
 *
 * SDMXJS.getData(args)
 *   .then(data => successCallback(data))
 *   .catch(error => errorCallback(error));
 */

import axios from 'axios';
import { DATA } from '../constants/types';
import { getRequestArgs } from '../getRequestArgs';

export const getData = (args) => {
  const { url, headers, params } = getRequestArgs({
    ...args,
    type: DATA,
  });

  return axios.get(url, { headers, params });
};
