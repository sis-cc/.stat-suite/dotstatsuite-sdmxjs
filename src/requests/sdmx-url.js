/**
 * @description
 * A function that given a set of arguments, returns corresponding SDMX Url.
 * Useful to give the SDMX source of set of data.
 *
 * @public
 * @name getSDMXUrl
 * @param {Object}
 * @return {string}
 *
 * @example
 * const args = {
 *  ...
 * }; // see getRequestArgs for args definition.
 *
 * SDMXJS.getData(args)
 *   .then(data => successCallback(data))
 *   .catch(error => errorCallback(error));
 */
import qs from 'qs';
import { getRequestArgs } from '../getRequestArgs';

export const getSDMXUrl = (args) => {
  const { url, params } = getRequestArgs(args);

  return `${url}${qs.stringify(params, { addQueryPrefix: true })}`;
};
