/**
 * @description
 * A function that given a set of arguments, returns a Promise, performing
 * a corresponding SDMX Data file Request (see getRequestArgs).
 *
 * @public
 * @name getDataFile
 * @param {Object}
 * @return {Promise}
 *
 * @example
 * const args = {
 *  ...
 * }; // see getRequestArgs for args definition. No need to specify 'type'
 *
 * SDMXJS.getDataFile(args)
 *   .then(blob => successCallback(blob))
 *   .catch(error => errorCallback(error));
 */

import axios from 'axios';
import { DATA } from '../constants/types';
import { getRequestArgs } from '../getRequestArgs';

export const getDataFile = (args) => {
  const { url, headers, params } = getRequestArgs({
    ...args,
    asFile: true,
    type: DATA,
  });

  return axios.get(url, { headers, params, responseType: 'blob' });
};
