/**
 * @description
 * A function that takes a parsed attribute and returns if it corresponds to the Frequency Attribute or not.
 * The attribute can be recognised by having `TIME_FORMAT` as `id`.
 *
 * @public
 * @name isFrequencyAttribute
 * @param {Object}
 * @return {Boolean}
 *
 * @example
 * SDMXJS.isFrequencyAttribute({ id: 'TIME_FORMAT', values: [] }); //=> true
 * SDMXJS.isFrequencyAttribute({ id: 'UNIT_MEASURE', values: [] }); //=> false
 */
import * as R from 'ramda';
import { FREQUENCY_ATTRIBUTE_IDS } from './constants/frequency';

export const isFrequencyAttribute = (attribute) =>
  R.includes(R.prop('id', attribute), FREQUENCY_ATTRIBUTE_IDS);
