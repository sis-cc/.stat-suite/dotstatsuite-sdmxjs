/*
 * A function that takes a set of arguments (see getRequestArgs) and returns the corresponding
 * data request params.
 *
 * Only considers 'startPeriod', 'endPeriod', 'lastNObservations' entries in 'params'.
 * Will always returns 'dimensionAtObservation' set to 'AllDimensions'.
 *
 * @private
 * @param {Object}
 * @return {Object}
 *
 * @example
 * getDataParams({}); //=> { dimensionAtObservation: 'AllDimensions' }
 * getDataParams({ params: { startPeriod: 'period', lastNObservations: 0 } });
 * //=> { dimensionAtObservation: 'AllDimensions', startPeriod: 'period' }
 */
import * as R from 'ramda';
import { DIMOBS_PARAM } from './constants/params';
import { parseDataParams } from './parseDataParams';
import { setParam } from './utils';

export const getDataParams = (args) =>
  R.pipe(
    R.propOr({}, 'params'),
    R.pickBy(R.pipe(R.isNil, R.not)),
    parseDataParams,
    R.when(
      R.always(!R.propOr(false, 'agnostic')(args)),
      setParam(DIMOBS_PARAM),
    ),
  )(args);
