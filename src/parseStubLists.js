/**
 * @description
 * A parser that given an SDMX Artefacts data structure query response, will parse the overall artefacts
 * (without sub values details, no codes for codelists for instance), and return them
 * grouped by type.
 *
 * @name parseStubLists
 * @public
 * @param {Object}
 * @return {Object}
 *
 * @example
 * const sdmxJson = {
 *   data: {
 *     dataflows: [
 *       { id: 'DF1', version: 'V1', agencyID: 'A1', name: 'dataflow1', isFinal: false },
 *       { id: 'DF2', version: 'V2', agencyID: 'A2', isFinal: true },
 *     ],
 *   }
 * };
 *
 * const parsedDataflows = SDMXJS.parseStubLists(sdmxJson);
 * //=>{
 * //   'dataflow': [
 * //      { code: 'DF1', label: 'dataflow1', isFinal: false, agencyId: 'A1', sdmxId: 'A1:DF1(V1)', type: 'dataflow', version: 'V1' },
 * //      { code: 'DF2', label: '[DF2]', isFinal: true, agencyId: 'A2', sdmxId: 'A2:DF2(V2)', type: 'dataflow', version: 'V2' }
 * //    ],
 * //  };
 */
import * as R from 'ramda';

const getType = (listType) => R.pipe(R.dropLast(1), R.toLower)(listType);

export const parseStubLists = (sdmxJson) =>
  R.pipe(R.propOr({}, 'data'), (lists) =>
    R.reduce(
      (acc, key) => {
        const type = getType(key);
        const artefacts = R.map(
          (artefact) => {
            const annotations = R.propOr([], 'annotations', artefact);
            const agencyId = artefact.agencyID;
            const code = artefact.id;
            const version = artefact.version;
            const isFinal = artefact.isFinal;
            const label = R.prop('name')(artefact);
            const sdmxId = `${agencyId}:${code}(${version})`;
            const links = artefact.links ? artefact.links : null;
            return {
              annotations,
              agencyId,
              code,
              version,
              isFinal,
              label,
              sdmxId,
              type,
              links,
            };
          },
          R.propOr([], key, lists),
        );
        return R.assoc(type, artefacts, acc);
      },
      {},
      R.keys(lists),
    ),
  )(sdmxJson);
