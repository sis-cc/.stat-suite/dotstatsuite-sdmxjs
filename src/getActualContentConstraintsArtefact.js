/**
 * @description
 * A function that takes a structure and returns parsed content constraints
 *
 * Options to configure the parser:
 * - limit: reject CCs that have more values than the limit
 * - no limit: no rejection
 *
 * @name getActualContentConstraintsArtefact
 * @function
 * @public
 * @param {Object} structure
 * @return {Object} content constraints [{ type: "Actual", ... }, { type: "Allowed", ... }]
 * @example
 * SDMXJS.getActualContentConstraintsArtefact(structure);
 *
 */

import * as R from 'ramda';
import { STRUCTURE_CONTENT_CONSTRAINTS_PATH } from './constants/content-constraints';
import { compareAsc } from 'date-fns';

export const getActualContentConstraintsArtefact = R.pipe(
  R.pathOr([], STRUCTURE_CONTENT_CONSTRAINTS_PATH),
  R.find((constraint) => {
    if (R.isNil(constraint)) return false;
    if (R.toLower(constraint.type) !== 'actual') {
      return false;
    }
    const now = new Date();
    if (!R.isNil(constraint.validTo)) {
      if (compareAsc(now, new Date(constraint.validTo)) !== -1) {
        return false;
      }
    }
    if (!R.isNil(constraint.validFrom)) {
      if (compareAsc(now, new Date(constraint.validFrom)) !== 1) {
        return false;
      }
    }
    return true;
  }),
);
