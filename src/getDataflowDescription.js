/**
 * @description
 * A function that takes a structure and will return the description of the dataflow.
 *
 * @public
 * @name getDataflowDescription
 * @param {Object}
 * @param {string}
 * @return {Object}
 *
 * @example
 * const structure = {
 *   data: {
 *     dataflows: [{
 *       id: 'DF',
 *       description: 'description'
 *     }]
 *   },
 * };
 * SDMXJS.getDataflowDescription(structure, 'en'); //=> 'en_dataflow'
 */
import * as R from 'ramda';

export const getDataflowDescription = (structure) => {
  const dataflow = R.pathOr({}, ['data', 'dataflows', 0], structure);
  return R.propOr(null, 'description', dataflow);
};
