/**
 * @description
 * A function that takes a parsed content constraints and parsed defaultSelection that return the default selection available by content constraints
 *
 * @name getActualContentConstraintsDefaultSelection
 * @public
 * @param options {Object}
 * @return {Object}
 * @example
 * const selection = { PHI: ['PHI_1', 'PHI_4'], TAU: ['TAU_1', 'TAU_3'] };
 * const contentConstraints = {
 *   PHI: new Set(['PHI_1', 'PHI_2', 'PHI_3']),
 *   HI: new Set(['HI_1', 'HI_2', 'HI_3']),
 *   TAU: new Set([]),
 * };
 * SDMXJS.getActualContentConstraintsDefaultSelection({ contentConstraints, selection });
 * return { PHI: [PHI_1], TAU: [TAU_1, TAU_3] }
 */
import * as R from 'ramda';

export default ({ contentConstraints, selection }) =>
  R.pipe(
    R.pick(R.keys(contentConstraints)),
    R.mapObjIndexed((val, id) => {
      const setIds = R.prop(id)(contentConstraints);
      return R.filter((vId) => setIds.has(vId))(val);
    }),
  )(selection);
