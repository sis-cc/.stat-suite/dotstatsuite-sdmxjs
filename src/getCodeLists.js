/**
 * @description
 * A function that takes a dataflow sdmx-json structure and returns the parsed
 * codelists of the dataflow
 *
 * The codelists are defined in dimensionList, attributesList etc... entry.
 * In each dimension a codelist is referenced by its urn, giving the values,
 * as well as a concept, giving the dimension name.
 *
 * Each values will be parsed and sorted regarding the reference of an
 * annotation of type 'ORDER'.
 *
 * rejectNoDataValues arg is useful to avoid dealing with big empty codelists
 * should not be used with attributes
 * cannot be applied easily for each value because of hierarchies
 *
 * @name getCodeLists
 * @public
 * @function
 * @param {Object}
 * @return {Array}
 *
 * @example
 * const structure = {
 *   data: {
 *     codelists: [
 *       {
 *         links: [{ rel: 'self', urn: 'CL_1' }],
 *         codes: [
 *           { id: '1.1', name: '1v1' },
 *           { id: '1.2', name: '1v2', parent: '1.1' }
 *         ]
 *       },
 *       {
 *         links: [{ rel: 'self', urn: 'CL_2' }],
 *         codes: [
 *           { id: '2.1', annotations: [{ text: '1', type: 'ORDER' }] },
 *           { id: '2.2', annotations: [{ text: '0', type: 'ORDER' }] }
 *         ]
 *       }
 *     ],
 *     conceptSchemes: [
 *       { concepts: [{ name: { en: 'dim1' }, links: [{ rel: 'self', urn: 'CPT_1' }] }] },
 *       { concepts: [{ links: [{ rel: 'self', urn: 'CPT_2' }] }] }
 *     ],
 *     dataStructures: [{
 *       dataStructureComponents: {
 *         dimensionList: {
 *           dimensions: [{
 *             id: 'D1',
 *             conceptIdentity: 'CPT_1',
 *             localRepresentation: { enumeration: 'CL_1' }
 *           },{
 *             id: 'D2',
 *             conceptIdentity: 'CPT_2',
 *             conceptRoles: ['ROLE'],
 *             localRepresentation: { enumeration: 'CL_2' }
 *           }]
 *         }
 *       }
 *     }]
 *   }
 * };
 *
 * SDMXJS.getCodeLists(pathToList)(structure);
 * //=> [
 * //    {
 * //      id: 'D1', index: 0, label: 'dim1', role: undefined,
 * //      hasData: true, // if contentConstraints with related id
 * //      values: [
 * //        { id: '1.1', label: '1v1', order: 0, parentId: undefined, position: 0, hasData: true,},
 * //        { id: '1.2', label: '1v2', order: 0, parentId: '1.1', position: 1 }
 * //      ]
 * //    },
 * //    {
 * //      id: 'D2', index: 1, label: '[D2]', role: 'ROLE',
 * //      values: [
 * //        { id: '2.2', label: '[2.2]', order: 0, parentId: undefined, position: 1 },
 * //        { id: '2.1', label: '[2.1]', order: 1, parentId: undefined, position: 0 }
 * //      ]
 * //    },
 * //   ]
 */

import * as R from 'ramda';
import { NO_DISPLAY_ANNOT_TYPE } from './constants/annotations';
import { parseNoDisplayAnnotation } from './parseNoDisplayAnnotation';
import { getConcepts } from './getConcepts';
import { getUrn } from './getUrn';
import { getCodeOrder } from './getCodeOrder';

const getDisplayValues = ({
  hasNoDisplayAnnotation,
  artefactId,
  noDisplayCodes,
  code,
}) =>
  R.allPass([
    R.complement(hasNoDisplayAnnotation),
    R.pipe(
      R.prop('id'),
      R.flip(R.append)([artefactId, 'values']),
      R.flip(R.complement(R.hasPath))(noDisplayCodes),
    ),
  ])(code);

const getDisplay = ({
  hasNoDisplayAnnotation,
  codelist,
  artefact,
  artefactId,
  noDisplayCodes,
}) =>
  R.allPass([
    R.always(R.complement(hasNoDisplayAnnotation)(codelist)),
    R.always(R.complement(hasNoDisplayAnnotation)(artefact)),
    R.anyPass([
      R.complement(R.has)(artefactId),
      R.hasPath([artefactId, 'values']),
    ]),
  ])(noDisplayCodes);

export default (pathToList) => (structure) => {
  const keyedCodelists = R.pipe(
    R.pathOr([], ['data', 'codelists']),
    R.indexBy(getUrn),
  )(structure);

  const hasNoDisplayAnnotation = (node) =>
    R.pipe(
      R.propOr([], 'annotations'),
      R.indexBy(R.prop('type')),
      R.has(NO_DISPLAY_ANNOT_TYPE),
    )(node);

  const keyedConcepts = getConcepts(structure);

  const noDisplayCodes = parseNoDisplayAnnotation(structure);

  return R.pipe(
    R.pathOr([], pathToList),
    R.addIndex(R.map)((artefact, artefactIndex) => {
      const codelist = R.pipe(
        R.path(['localRepresentation', 'enumeration']),
        R.flip(R.propOr({}))(keyedCodelists),
      )(artefact);

      const artefactId = R.prop('id', artefact);
      const values = R.pipe(
        R.propOr([], 'codes'),
        R.addIndex(R.map)((code, position) => ({
          id: R.prop('id', code),
          label: R.prop('name')(code),
          description: R.prop('description', code),
          order: getCodeOrder(code),
          parentId: R.prop('parent', code),
          position,
          display: getDisplayValues({
            hasNoDisplayAnnotation,
            artefactId,
            noDisplayCodes,
            code,
          }),
        })),
        R.sortWith([R.ascend(R.prop('order')), R.ascend(R.prop('position'))]),
      )(codelist);

      const concept = R.pipe(
        R.prop('conceptIdentity'),
        R.flip(R.propOr({}))(keyedConcepts),
      )(artefact);

      return {
        id: artefactId,
        description: R.prop('description', codelist),
        index: artefactIndex,
        role: R.pipe(R.propOr([], 'conceptRoles'), R.head)(artefact),
        values,
        display: getDisplay({
          hasNoDisplayAnnotation,
          codelist,
          artefact,
          artefactId,
          noDisplayCodes,
        }),
        label: R.prop('name', concept),
      };
    }),
  )(structure);
};
