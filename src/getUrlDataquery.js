/*
 * A function that takes a dataquery returns the corresponding url entry.
 * The purpose of this function is to prevent some empty dataquerys like '..' to mess with the sdmx data call.
 * Therefore, for any empty dataquery value passed, 'all' value will be returned instead. Same for null or undefined value.
 *
 * @private
 * @param {string}
 * @return {string}
 *
 * @example
 * SDMXJS.getUrlDataquery(''); //=> 'all'
 * SDMXJS.getUrlDataquery('.....'); //=> 'all'
 * SDMXJS.getUrlDataquery(undefined); //=> 'all'
 * SDMXJS.getUrlDataquery('..some_selection..'); //=> '..some_selection..'
 */
import * as R from 'ramda';
import {
  ALL_IDENTIFIER,
  DATAQUERY_DIMENSIONS_SEPARATOR,
} from './constants/url';

export const getUrlDataquery = (dataquery) =>
  R.pipe(
    R.when(R.isNil, R.always('')),
    R.when(
      R.pipe(
        R.replace(new RegExp(`\\${DATAQUERY_DIMENSIONS_SEPARATOR}`, 'g'), ''),
        R.isEmpty,
      ),
      R.always(ALL_IDENTIFIER),
    ),
  )(dataquery);
