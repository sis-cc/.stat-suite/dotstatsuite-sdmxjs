/*
 * A function that takes a dataflow structure and returns its concepts indexed by their sdmx urn, since they are always referenced by their urn.
 *
 * @public
 * @param {Object}
 * @return {Object}
 *
 * @example
 * const structure = {
 *   data: {
 *     conceptSchemes = [
 *       { concepts: [{ id: 'A', links: [{ rel: 'self', urn: 'URN_A' }] }, { id: 'B', links: [{ rel: 'self', urn: 'URN_B' }] }] },
 *       { concepts: [{ id: 'C', links: [{ rel: 'self', urn: 'URN_C' }] }] },
 *     ]
 *   }
 * };
 * SDMXJS.getConcepts(structure);
 * //=> {
 * //=>   A: { id: 'A', links: [{ urn: 'URN_A' }] },
 * //=>   B: { id: 'B', links: [{ urn: 'URN_B' }] },
 * //=>   C: { id: 'C', links: [{ urn: 'URN_C' }] }
 * //=> }
 */
import * as R from 'ramda';
import { getUrn } from './getUrn';

export const getConcepts = (structure) =>
  R.pipe(
    R.pathOr([], ['data', 'conceptSchemes']),
    R.map(R.propOr([], 'concepts')),
    R.unnest,
    R.indexBy(getUrn),
  )(structure);
