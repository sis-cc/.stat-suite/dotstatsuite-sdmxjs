/**
 * @description
 * A function that takes a parsed dimension and returns if it corresponds to the Reference Area Dimension or not.
 * The dimension can be recognised by having `GEO` or `REF_AREA` as `role`, or by having `REF_AREA`, `COUNTRY`, `LOCATION`, `REGION` or `REFERENCE_AREA` as `id`.
 *
 * @public
 * @name isRefAreaDimension
 * @param {Object}
 * @return {Boolean}
 *
 * @example
 * SDMXJS.isRefAreaDimension({ id: 'DIM', role: 'GEO', values: [] }); //=> true
 * SDMXJS.isRefAreaDimension({ id: 'LOCATION', role: null, values: [] }); //=> true
 * SDMXJS.isRefAreaDimension({ id: 'INDICATOR', role: 'SUBJECT', values: [] }); //=> false
 */
import * as R from 'ramda';
import { REF_AREA_IDS, REF_AREA_ROLES } from './constants/dimensions';

export const isRefAreaDimension = (dimension) => {
  const hasRefAreaDimensionRole = R.includes(
    R.prop('role', dimension),
    REF_AREA_ROLES,
  );
  if (hasRefAreaDimensionRole) {
    return true;
  }
  return R.includes(R.prop('id', dimension), REF_AREA_IDS);
};
