import * as R from 'ramda';

export const getExternalResources = (structure) => {
  return R.pipe(
    R.pathOr([], ['data', 'dataflows', 0, 'annotations']),
    R.reduce((acc, annotation) => {
      if (
        R.pipe(
          R.prop('type'),
          R.complement(R.equals)('EXT_RESOURCE'),
        )(annotation)
      ) {
        return acc;
      }
      const split = R.pipe(
        R.prop('text'),
        R.when(R.isNil, R.always('')),
        R.split('|'),
      )(annotation);
      if (R.length(split) < 2) {
        return acc;
      }
      const parse = R.pipe(R.take(3), ([label, link, img]) => ({
        label,
        link,
        img,
      }))(split);
      return R.append(
        {
          id: `EXT_RESOURCE_${R.length(acc)}`,
          ...parse,
        },
        acc,
      );
    }, []),
  )(structure);
};
