/**
 * @description
 * previous name: getActualContentConstraints
 *
 * A function that takes a structure and returns parsed content constraints
 *
 * Options to configure the parser:
 * - limit: reject CCs that have more values than the limit
 * - no limit: no rejection
 *
 * @name parseActualContentConstraints
 * @function
 * @public
 * @param {Object} options { options = { limit } }
 * @return {Object} structure { DIM: set(['id1', 'id2']), TIME_PERIOD: { ... } }
 * @example
 * SDMXJS.parseActualContentConstraints(options)(structure);
 *
 * if no path ['data', 'contentConstraints'] return null
 * if contentConstraints or does not have type 'Actual' return null
 * else return { DIM: set(['id1', 'id2']), TIME_PERIOD: { ... } }
 *
 */

import * as R from 'ramda';
import { CONTENT_CONSTRAINTS_TIME_PERIOD } from './constants/content-constraints';
import { getActualContentConstraintsArtefact } from './getActualContentConstraintsArtefact';

const aboveLimit = (limit) => R.pipe(R.length, R.lt(limit));

const getTimePeriod = R.converge(
  (start, end, startInclusive, endInclusive) => ({
    boundaries: [start, end],
    includingBoundaries: [startInclusive, endInclusive],
  }),
  [
    R.path(['startPeriod', 'period']),
    R.path(['endPeriod', 'period']),
    R.path(['startPeriod', 'isInclusive']),
    R.path(['endPeriod', 'isInclusive']),
  ],
);

const getSet = ({ limit }) =>
  R.reduce((acc, { id, values = [], timeRange = {} }) => {
    if (R.equals(id, CONTENT_CONSTRAINTS_TIME_PERIOD)) {
      return R.assoc(id, getTimePeriod(timeRange), acc);
    }
    if (R.either(R.isEmpty, aboveLimit(limit))(values)) return acc;
    return R.assoc(id, new Set(values), acc);
  }, {});

const getSetOfvalues = (options) =>
  R.reduce((acc, { isIncluded, keyValues = [] }) => {
    if (!isIncluded) return acc;
    return R.mergeRight(getSet(options)(keyValues), acc);
  }, {});

const parseContentConstraints = (options) => (contentConstraints) =>
  R.pipe(
    R.propOr([], 'cubeRegions'),
    getSetOfvalues(options),
  )(contentConstraints);

export default (options = {}) =>
  R.pipe(
    getActualContentConstraintsArtefact,
    R.ifElse(R.isNil, R.always(null), parseContentConstraints(options)),
  );
