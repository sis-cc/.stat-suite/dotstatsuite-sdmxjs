/**
 * @description
 * A function that retrieves the order of a codelist value through its annotations.
 * returns -1 if not found.

 * @name getCodeOrder
 * @public
 * @function
 * @param {string}
 * @param {Object}
 * @return {Integer}
 *
 * @example
 * SDMXJS.getCodeOrder({ id: 'code_id', annotations: [] }); //=> -1
 * SDMXJS.getCodeOrder({ id: 'code_id', annotations: [{ type: 'ORDER', text: '21' }] }); //=> 21
 */

import * as R from 'ramda';

export const getCodeOrder = (data) => {
  if (R.isNil(data)) return -1;
  if (R.isNil(data.annotations)) return -1;
  if (!R.is(Array, data.annotations)) return -1;
  const annotation = R.find((a) => {
    if (R.isNil(a)) return false;
    return a.type === 'ORDER';
  }, data.annotations);
  if (R.isNil(annotation)) return -1;
  const value = R.prop('text', annotation);
  if (R.isNil(value)) return -1;
  return Number(value);
};
