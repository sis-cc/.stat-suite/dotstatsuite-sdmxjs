import * as R from 'ramda';
import {
  DATAFLOW_ANNOTATIONS_PATH,
  NO_DISPLAY_ANNOT_TYPE,
} from './constants/annotations';

export const parseNoDisplayAnnotation = (structure) =>
  R.pipe(
    R.pathOr([], DATAFLOW_ANNOTATIONS_PATH),
    R.find(R.propEq(NO_DISPLAY_ANNOT_TYPE, 'type')),
    R.when(R.isNil, R.always({})),
    R.propOr('', 'title'),
    R.split(','),
    R.reduce((acc, entry) => {
      if (R.isEmpty(entry)) {
        return acc;
      }
      const parse = R.split('=', entry);
      const id = R.head(parse);
      const values = R.ifElse(
        R.pipe(R.length, R.equals(1)),
        R.always(null),
        R.pipe(
          R.last,
          R.split('+'),
          R.reject(R.isEmpty),
          R.indexBy(R.identity),
        ),
      )(parse);
      return R.pipe(
        R.set(R.lensProp(id), { id }),
        R.when(
          R.always(!R.isNil(values) && !R.isEmpty(values)),
          R.set(R.lensPath([id, 'values']), values),
        ),
      )(acc);
    }, {}),
  )(structure);
