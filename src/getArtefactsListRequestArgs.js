/**
 * @description
 * An utilitary function that given a set of selected SDMX datasource,
 * artefact type, category, agency and version, will return the request options
 * (see getRequestArgs) to be applied in order to retrieve
 * the corresponding set of artefacts.
 *
 * @public
 * @name getArtefactsListRequestArgs
 * @param {Object}
 * @return {Object}
 *
 * @example
 * const selection = {
 *   datasource: { url: 'my_endpoint' },
 *   agencyId: 'SDMX',
 *   category: { scheme: { agencyId: 'SDMX', code: 'SCH', version: '1.0' }, code: 'CAT' },
 *   type: 'dataflow',
 *   version: 'latest'
 * };
 *
 * const { url, header, params } = SDMXJS.getArtefactsListRequestArgs(selection);
 *
 * return axios.get(url, { headers, params }).then( ... );
 */
import * as R from 'ramda';
import { getRequestArgs } from './getRequestArgs';
import { ALL, CATEGORY_SCHEME } from './constants/types';

export const getArtefactsListRequestArgs = ({
  category,
  type,
  agencyId,
  version,
  ...rest
}) =>
  R.pipe(
    R.ifElse(
      R.isNil,
      R.always({
        identifiers: { agencyId, code: ALL, version },
        type,
        withReferences: false,
      }),
      R.always({
        identifiers: {
          ...R.propOr({}, 'scheme', category),
          subcode: R.prop('hierarchicalCode', category),
        },
        params: { references: type },
        type: CATEGORY_SCHEME,
      }),
    ),
    R.mergeRight(R.assoc('overview', true, rest)),
    getRequestArgs,
    R.assoc('parsing', {
      space: rest.datasource,
      type,
      agencyIds: R.propOr([], 'agencyIds', rest),
      fromCategory: !R.isNil(category),
    }),
  )(category);
