import * as R from 'ramda';

const parseNumber = R.when(R.complement(R.isNil), Number);

export const parseDataRange = (response) =>
  R.pipe(
    R.pathOr('', ['headers', 'content-range']),
    R.match(/values (\d+)-(\d+)\/(\d+)/),
    R.when(R.isNil, R.always([])),
    R.slice(1, 4),
    R.map(parseNumber),
    ([start, end, total]) => {
      if (R.any(R.anyPass([R.isNil, isNaN]))([start, end, total])) {
        return {};
      }
      const count = end - start + 1;
      return { count, total };
    },
  )(response);
