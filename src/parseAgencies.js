/**
 * @description
 * A function that will deduce a flat list of agencies given
 * a set of SDMX-JSON agencSchemes structure message
 *
 * @public
 * @name parseAgencies
 * @param {Object}
 * @return {Object}
 *
 *@example
 * const params = {
 *  data: {
 *   agencySchemes: [{
 *     agencyID: 'S1',
 *     agencies: [
 *       { id: 'A1', name: 'ag1' },
 *       { id: 'A2' }
 *     ]
 *   },{
 *     agencyID: 'S2',
 *     agencies: [{ id: 'A1', name: 'ag1' }]
 *   }]
 *  }
 * });
 *
 * SDMXJS.parseAgencies(params);
 * //=> {
 * //     'S1.A1': { code: 'A1', name: 'ag1', parent: 'S1', id: 'S1.A1' },
 * //     'S1.A2': { code: 'A2', name: '[A2]', parent: 'S1', id: 'S1.A2' },
 * //     'S2.A1': { code: 'A1', name: 'ag1', parent: 'S2', id: 'S2.A1' }
 * //   };
 */
import * as R from 'ramda';

export const parseAgencies = ({ data }) =>
  R.reduce(
    (agencies, scheme) => {
      const parent = scheme.agencyID;
      const children = R.map(
        (agency) => {
          const code = agency.id;
          const id = `${parent}.${code}`;
          const name = R.prop('name')(agency);
          return { id, code, name, parent };
        },
        R.propOr([], 'agencies', scheme),
      );
      return R.mergeRight(agencies, R.indexBy(R.prop('id'), children));
    },
    {},
    R.propOr([], ['agencySchemes'], data),
  );
