/*
 * A function that takes a dataflow structure and returns the time period id and localised label.
 *
 * @public
 * @param {Object}
 * @return {Object}
 *
 * @example
 * const structure = {
 *   data: {
 *     conceptSchemes: [{
 *       concepts: [{
 *         id: 'T_P',
 *         links: [{ rel: 'self', urn: 'URN/T_P' }],
 *         name: 'Time Period'
 *       }]
 *     }],
 *     dataStructures: [{
 *       dataStructureComponents: {
 *         dimensionList: {
 *           timeDimensions: [{ id: 'TIME', conceptIdentity: 'URN/T_P' }]
 *         }
 *       }
 *     }]
 *   }
 * };
 * SDMXJS.getTimePeriod(structure); //=> { id: 'TIME', label: 'Time Period', display: true }
 */
import * as R from 'ramda';
import { getConcepts } from './getConcepts';
import { parseNoDisplayAnnotation } from './parseNoDisplayAnnotation';

export const getTimePeriod = (structure) => {
  const timeDimension = R.pathOr(
    {},
    [
      'data',
      'dataStructures',
      0,
      'dataStructureComponents',
      'dimensionList',
      'timeDimensions',
      0,
    ],
    structure,
  );

  const timeDimensionConceptUrn = R.prop('conceptIdentity', timeDimension);

  if (R.isNil(timeDimensionConceptUrn)) {
    return null;
  }

  const concepts = getConcepts(structure);

  const timeDimensionConcept = R.prop(timeDimensionConceptUrn, concepts);
  const displayAnnotations = parseNoDisplayAnnotation(structure);
  const id = R.prop('id', timeDimension);
  return {
    id,
    label: R.prop('name')(timeDimensionConcept),
    display: R.not(R.has(id, displayAnnotations)),
  };
};
