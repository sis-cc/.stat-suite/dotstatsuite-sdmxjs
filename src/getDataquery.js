/**
 * @description
 * A curried function that takes a set of dimensions and a selection and returns
 * the corresponding SDMX dataquery.
 *
 * @public
 * @name getDataquery
 * @param {Array}
 * @param {Object}
 * @return {string}
 *
 * @example
 * const dimensions = [
 *   { id: 'ALPHA' },
 *   { id: 'GAMMA' },
 *   { id: 'PHI' },
 *   { id: 'EPSILON' },
 *   { id: 'TAU' },
 * ];
 *
 * const selection = { PHI: ['PHI_1'], TAU: ['TAU_1', 'TAU_3'] };
 *
 * SDMXJS.getDataquery(dimensions, selection);
 * //=> '..PHI_1..TAU_1+TAU_3'
 */
import * as R from 'ramda';
import {
  DATAQUERY_DIMENSIONS_SEPARATOR,
  DATAQUERY_VALUES_SEPARATOR,
} from './constants/url';

const getDataquery = (dimensions, selections) =>
  R.pipe(
    R.map((dim) =>
      R.pipe(
        R.propOr([], 'values'),
        R.indexBy(R.prop('id')),
        R.pick(R.propOr([], R.prop('id', dim), selections)),
        R.values,
        R.map(R.prop('id')),
        R.join(DATAQUERY_VALUES_SEPARATOR),
      )(dim),
    ),
    R.join(DATAQUERY_DIMENSIONS_SEPARATOR),
  )(dimensions);

export default R.curry(getDataquery);
