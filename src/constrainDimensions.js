/**
 * @description
 * A function that add a prop (by accessorName) true or false
 * regarding your dimension and contentConstraints
 *
 * without contentConstraints the accessorName will not appear in your returned list
 *
 * @name constrainDimensions
 * @return {Array}
 * @public
 * @example
 * const contentConstraints = { D1: [1.1], TAU: [TAU_1, TAU_3] };
 * const dimensions = [
 *   {
 *     id: 'D1', index: 0, label: 'dim1', role: undefined,
 *     values: [
 *       { id: '1.1', label: '1v1', order: 0, parentId: undefined, position: 0, hasData: true,},
 *       { id: '1.2', label: '1v2', order: 0, parentId: '1.1', position: 1 }
 *     ]
 *   },
 *   {
 *     id: 'D2', index: 1, label: '[D2]', role: 'ROLE',
 *     values: [
 *       { id: '2.2', label: '[2.2]', order: 0, parentId: undefined, position: 1 },
 *       { id: '2.1', label: '[2.1]', order: 1, parentId: undefined, position: 0 }
 *     ]
 *   },
 * ];
 * constrainDimensionValues(dimensions, contentConstraints)
 * return [
 *   {
 *     id: 'D1', index: 0, label: 'dim1', role: undefined,
 *     hasData: true, // if contentConstraints with related id
 *     values: [
 *       { id: '1.1', label: '1v1', order: 0, parentId: undefined, position: 0, hasData: true}, // <= hasData added
 *       { id: '1.2', label: '1v2', order: 0, parentId: '1.1', position: 1 }
 *     ]
 *   },
 *   {
 *     id: 'D2', index: 1, label: '[D2]', role: 'ROLE',
 *     values: [
 *       { id: '2.2', label: '[2.2]', order: 0, parentId: undefined, position: 1 },
 *       { id: '2.1', label: '[2.1]', order: 1, parentId: undefined, position: 0 }
 *     ]
 *   },
 * ]
 */

import * as R from 'ramda';

const constrainDimensionValues = (
  dimension,
  contentConstraints,
  accessorName,
) => {
  const dimensionConstraints = R.prop(dimension.id, contentConstraints);
  const isNotConstrained = R.isNil(dimensionConstraints);

  const indexedConstraints = isNotConstrained
    ? null
    : R.indexBy(R.identity, dimensionConstraints);

  return R.pipe(
    R.over(
      R.lensProp('values'),
      R.map((val) =>
        R.assoc(
          accessorName,
          isNotConstrained || R.has(val.id, indexedConstraints),
          val,
        ),
      ),
    ),
    R.assoc(accessorName, isNotConstrained || !R.isEmpty(dimensionConstraints)),
  )(dimension);
};

export const constrainDimensions = (
  dimensions,
  contentConstraints,
  accessorName = 'hasData',
) => {
  return R.map(
    (dimension) =>
      constrainDimensionValues(dimension, contentConstraints, accessorName),
    dimensions,
  );
};
