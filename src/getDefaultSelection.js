/**
 * @description
 * A function that takes a dataflow sdmx-json structure and returns the default
 * dimensions selection defined whithin it.
 *
 * This data is retrieved by mapping the dimensions list with a possible annotation
 * defined at the dataflow level of type 'DEFAULT'.
 *
 * disclaimer: this function doesn't map the codes provided in the annotation with the ones
 * defined in each dimension codelist, and assume them to be correct.
 *
 * @public
 * @name getDefaultSelection
 * @param {Object}
 * @return {string}
 *
 * @example
 * const structure = {
 *   data: {
 *     dataStructures: [{
 *       dataStructureComponents: {
 *         dimensionList: {
 *           dimensions: [
 *             { id: 'ALPHA' },
 *             { id: 'GAMMA' },
 *             { id: 'PHI' },
 *             { id: 'EPSILON' },
 *             { id: 'TAU' },
 *           ]
 *         }
 *       }
 *     }],
 *     dataflows: [{
 *       annotations: [
 *         { type: 'DEFAULT', title: 'TAU=TAU_1+TAU_3,PHI=PHI_1,TIME_PERIOD_START=2013-01' },
 *       ]
 *     }]
 *   }
 * };
 *
 * SDMXJS.getDefaultDataquery(structure); //=> { PHI: [PHI_1], TAU: [TAU_1, TAU_3] }
 */
import * as R from 'ramda';
import { STRUCTURE_DIMENSIONS_PATH } from './constants/dimensions';
import { DATAQUERY_VALUES_SEPARATOR } from './constants/url';
import { parseDefaultAnnotation } from './parseDefaultAnnotation';

export const getDefaultSelection = (structure) => {
  const dimensionsIds = R.pipe(
    R.pathOr([], STRUCTURE_DIMENSIONS_PATH),
    R.map(R.prop('id')),
  )(structure);

  return R.pipe(
    parseDefaultAnnotation,
    R.pick(dimensionsIds),
    R.mapObjIndexed(R.split(DATAQUERY_VALUES_SEPARATOR)),
  )(structure);
};
