/**
 * @description
 * A function that given a set of arguments, returns a Promise, performing
 * a corresponding SDMX CategoryScheme Structure Request (see getRequestArgs)
 * and return the corresponding parsed categories.
 *
 * @public
 * @name getCategories
 * @param {Object}
 * @return {Promise}
 *
 * @example
 * const args = {
 *  ...
 * }; // see getRequestArgs for args definition. No need to specify 'type'
 *
 * SDMXJS.getCategories(args)
 *   .then(data => successCallback(data))
 *   .catch(error => errorCallback(error));
 */
import * as R from 'ramda';
import axios from 'axios';
import {
  ALL,
  CATEGORY_SCHEME,
  CATEGORY_SCHEME_RESULTS,
  CATEGORY_RESULTS,
} from './constants/types';
import { getRequestArgs } from './getRequestArgs';
import { parseTreeResultsAsList } from './parseTreeResultsAsList';

export const getCategories = (args) => {
  const { url, headers, params } = getRequestArgs(
    R.pipe(
      R.assoc('type', CATEGORY_SCHEME),
      R.assoc('identifiers', { agencyId: ALL, code: ALL, version: ALL }),
    )(args),
  );

  return axios.get(url, { headers, params }).then((res) =>
    parseTreeResultsAsList({
      lists: CATEGORY_SCHEME_RESULTS,
      listValues: CATEGORY_RESULTS,
    })(res.data),
  );
};
