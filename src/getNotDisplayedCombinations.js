/**
 * @description
 * A function that will extract a set of combination of values (ids)
 * listed inside brackets for a given dimension from an annotation
 *
 * @public
 * @name getNotDisplayedCombinations
 * @param {Object}
 * @return {Object}
 *
 *@example
 * const annotation = { title: 'DIM1=(ID1+ID2),DIM2=(ID1+ID2+ID3),DIM3=ID1+ID2' }
 *
 * SDMXJS.getNotDisplayedCombinations(params);
 * //=> {
 * //     DIM1: ['ID1', 'ID2'],
 * //     DIM2: ['ID1', 'ID2', 'ID3'],
 * //   };
 */

import * as R from 'ramda';

export const getNotDisplayedCombinations = (annotation) =>
  R.pipe(
    R.prop('title'),
    R.split(','),
    R.reduce((acc, dimension) => {
      const [dim, value] = dimension.split('=');
      const values = R.ifElse(
        R.test(/\((.*?)\)/g),
        R.pipe(
          R.match(/\((.*?)\)/g),
          R.head,
          R.replace(/[()]/g, ''),
          R.split('+'),
        ),
        R.always(''),
      )(value);
      return R.head(values) ? { ...acc, [dim]: values } : acc;
    }, {}),
  )(annotation);
