/**
 * @description
 * An utilitary function that returns the "urn property of a given, out of the links property".
 *
 * @private
 * @name getUrn
 * @return {string}
 *
 * @example
 * const artefact = {
 *   id: 'A',
 *   names: { en: 'My Artefact' },
 *   links: [
 *     { rel: 'whatever', urn: 'some urn' },
 *     { rel: 'self', urn: 'my.artefact.urn' }
 *   ]
 * };
 * SDMXJS.getUrn(artefact); //=> 'my.artefact.urn'
 */
import * as R from 'ramda';

export const getUrn = (artefact) =>
  R.pipe(
    R.propOr([], 'links'),
    R.find(R.propEq('self', 'rel')),
    R.when(R.complement(R.isNil), R.prop('urn')),
  )(artefact);
