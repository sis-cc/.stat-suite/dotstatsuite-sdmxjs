/**
 * @description
 * A function that given a dataflow structure returns the external reference definition, or null if none
 *
 * @public
 * @name getDataflowExternalReference
 * @param {Object}
 * @return {Object}
 *
 * @example
 * const structure = {
 *   data: {
 *     dataflows: [{
 *       id: 'MY_DF',
 *       agencyID: 'AG',
 *       version: '5.0',
 *       isExternalReference: true,
 *       links: [
 *         { rel: 'external', href: 'endpoint/dataflow/agencyId/code/version' },
 *       ],
 *     }],
 *   }
 * };
 *
 * SDMXJS.getDataflowExternalReference(structure);
 * //=>  {
 * //      datasource: { url: 'endpoint' },
 * //      identifiers: { agencyId: 'agencyId', code: 'code', version: 'version' }
 * //    };
 */
import * as R from 'ramda';
import { parseExternalReference } from './parseExternalReference';

export const getDataflowExternalReference = (structure) => {
  const dataflowDefinition = R.pipe(
    R.pathOr([], ['data', 'dataflows']),
    R.head,
  )(structure);
  return parseExternalReference(dataflowDefinition, 'dataflow');
};
