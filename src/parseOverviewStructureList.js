/**
 * @description
 * A parser that given an SDMX Artefacts List Response and parsing options (properties from the request),
 * will parse the overall artefacts (without sub values details, no codes for codelists for instance),
 * and return them in the form of an object.
 *
 * @public
 * @name parseOverviewStructureList
 * @param {Object}
 * @return {Object}
 *
 * @example
 * const sdmxJson = {
 *   data: {
 *     dataflows: [
 *       { id: 'DF1', version: 'V1', agencyID: 'A1', name: 'dataflow1', isFinal: false },
 *       { id: 'DF2', version: 'V2', agencyID: 'A2', isFinal: true },
 *     ],
 *   },
 * };
 *
 * const parseOptions = { space: { id: 'sp' }, type: 'dataflow' };
 *
 * const parsedDataflows = SDMXJS.parseOverviewStructureList(parseOptions)(sdmxJson);
 * //=>{
 * //   'sp:dataflow:A1:DF1(V1)': {
 * //      code: 'DF1', label: 'dataflow1', isFinal: false, agencyId: 'A1', sdmxId: 'A1:DF1(V1)',
 * //      space: { id: 'sp' }, id: 'sp:dataflow:A1:DF1(V1)', type: 'dataflow', version: 'V1'
 * //    },
 * //    'sp:dataflow:A2:DF2(V2)': {
 * //      code: 'DF2', label: '[DF2]', isFinal: true, agencyId: 'A2', sdmxId: 'A2:DF2(V2)',
 * //      space: { id: 'sp' }, id: 'sp:dataflow:A2:DF2(V2)', type: 'dataflow', version: 'V2'
 * //    },
 * //  };
 */
import * as R from 'ramda';
import { LISTS } from './constants/types';

export const parseOverviewStructureList =
  ({ agencyIds, fromCategory, space, type }) =>
  (sdmxJson) => {
    return R.pipe(
      R.prop('data'),
      R.ifElse(
        R.always(type === 'all'),
        R.values,
        R.pipe(R.prop(LISTS[type]), (list) => [list]),
      ),
      R.reduce((acc, list) => {
        const parsedList = R.reduce(
          (acc, artefact) => {
            const agencyId = artefact.agencyID;
            const code = artefact.id;
            const version = artefact.version;
            const isFinal = artefact.isFinal;
            const label = R.prop('name')(artefact);
            const sdmxId = `${agencyId}:${code}(${version})`;
            const id = `${space.id}:${type}:${sdmxId}`;

            return {
              ...acc,
              [id]: {
                agencyId,
                code,
                version,
                type,
                isFinal,
                label,
                sdmxId,
                id,
                space,
              },
            };
          },
          {},
          list,
        );

        return R.mergeRight(acc, parsedList);
      }, {}),
      R.when(
        R.always(fromCategory && !R.isEmpty(agencyIds)),
        R.filter(R.pipe(R.prop('agencyId'), R.flip(R.includes)(agencyIds))),
      ),
    )(sdmxJson);
  };
