/**
 * @description
 * A function that will parse the list of hierarchical categories
 * attached to a given artefact from an SDMX_JSON categorisations and
 * category schemes structure message.
 *
 * @public
 * @name getArtefactCategories
 * @param {Object}
 * @return {Array}
 *
 *@example
 * const params = {
 *  artefactId: 'Ag:Df(1.0)',
 *  data: {
 *   categorisations: [
 *     { source: 'sdmx.urn=Ag:Df(1.0)', target: 'sdmx.urn=Ag:Sc(1.0).Cat' },
 *   ],
 *   categorySchemes: [{
 *     agencyID: 'Ag',
 *     id: 'Sc',
 *     version: '1.0',
 *     categories: [{
 *       id: 'Cat',
 *       name: 'Category',
 *     }]
 *   }]
 * }
 *});
 *
 * SDMXJS.getArtefactCategories(params);
 * //=> [{ id: 'Cat', name: 'Category' }]
 */
import * as R from 'ramda';
import { parseCategorisations } from './parseCategorisations';

export const getArtefactCategories = ({ artefactId, data = {} }) => {
  const categorisations = parseCategorisations({ data });

  const categorySchemes = R.indexBy(
    R.pipe(
      R.props(['agencyID', 'id', 'version']),
      ([agency, code, version]) => `${agency}:${code}(${version})`,
    ),
  )(R.propOr([], 'categorySchemes', data));
  const getHierarchicalCategories = ({ categories, ids }) => {
    if (R.isEmpty(categories) || R.isEmpty(ids)) {
      return [];
    }
    const id = R.head(ids);
    const category = R.find(R.propEq(id, 'id'), categories);
    if (R.isNil(category)) {
      return [];
    }
    const children = getHierarchicalCategories({
      categories: R.propOr([], 'categories', category),
      ids: R.tail(ids),
    });
    return R.prepend({ id, name: R.prop('name')(category) }, children);
  };

  return R.reduce(
    (acc, { schemeId, path }) => {
      const scheme = R.prop(schemeId, categorySchemes);
      if (R.isNil(scheme)) {
        return acc;
      }
      const categories = getHierarchicalCategories({
        categories: scheme.categories,
        ids: R.split('.', path),
      });
      if (R.isEmpty(categories)) {
        return acc;
      }
      const name = R.prop('name')(scheme);
      return [...acc, R.prepend({ id: scheme.id, name }, categories)];
    },
    [],
    R.propOr([], artefactId, categorisations),
  );
};
