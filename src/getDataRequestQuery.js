/*
 * A function that given the proper type, identifiers
 * and dataquery will return the formated data sdmxQuery
 *
 * @public
 * @param {Object}
 * @return {string}
 *
 * @example
 * const args = {
 *   type: dataflow, //can also be 'availableconstraint'
 *   identifiers: { agencyId: 'OECD', code: 'DF', version: '1.0' },
 *   dataquery: '...'
 * };
 *
 * SDMXJS.getDataRequestQuery(args);
 * //=> 'data/OECD,DF,1.0/all',
 */
import { getUrlDataquery } from './getUrlDataquery';

export const getDataRequestQuery = ({ type, identifiers, dataquery }) => {
  const { agencyId, code, version = '' } = identifiers;
  const dq = getUrlDataquery(dataquery);
  return `${type}/${agencyId},${code},${version}/${dq}`;
};
