/**
 * @description
 * A function that given a structure query result, will parse and returns the flat list of all returned artefacts except for the one given in reference through an id.
 * The id must follow the following shape: "[artefactType]:[artefactAgency]:[artefactCode]([artefactVersion])".
 *
 * @public
 * @name parseArtefactRelatives
 * @param {Object}
 * @return {Array}
 *
 *@example
 * const sdmxJson = {
 *   data: {
 *     dataflows: [
 *       {
 *         name: 'Dataflow',
 *         agencyID: 'A',
 *         id: 'DF',
 *         isFinal: true,
 *         version: '1'
 *       },
 *       {
 *         agencyID: 'A',
 *         id: 'DF',
 *         isFinal: false,
 *         version: '2'
 *       }
 *     ],
 *     dataStructures: [
 *       {
 *         names: {},
 *         agencyID: 'A',
 *         id: 'DSD',
 *         version: 1
 *       }
 *     ]
 *   }
 * };
 *
 * SDMXJS.parseArtefactRelatives({ sdmxJson, artefactId: 'datastructure:A:DSD(1)' });
 * //=> [
 * //      { label: 'Dataflow', sdmxId: 'A:DF(1)', code: 'DF', type: 'dataflow', version: '1', agencyId: 'A', isFinal: true },
 * //      { label: '[DF]', sdmxId: 'A:DF(2)', code: 'DF', type: 'dataflow', version: '2', agencyId: 'A', isFinal: false }
 * //   ];
 */
import * as R from 'ramda';
import { getArtefactTypeFromListType } from './getArtefactTypeFromList';

export const parseArtefactRelatives = ({ sdmxJson, artefactId }) =>
  R.pipe(
    R.propOr({}, 'data'),
    R.mapObjIndexed((list, listType) => {
      const type = getArtefactTypeFromListType(listType);
      return R.reduce(
        (acc, artefact) => {
          const label = R.prop('name')(artefact);
          const version = artefact.version;
          const agencyId = artefact.agencyID;
          const code = artefact.id;
          const isFinal = artefact.isFinal;
          const sdmxId = `${agencyId}:${code}(${version})`;
          if (`${type}:${sdmxId}` === artefactId) {
            return acc;
          }
          const annotations = R.propOr([], 'annotations', artefact);
          return R.append(
            {
              annotations,
              label,
              version,
              agencyId,
              code,
              isFinal,
              sdmxId,
              type,
            },
            acc,
          );
        },
        [],
        list,
      );
    }),
    R.values,
    R.unnest,
  )(sdmxJson);
