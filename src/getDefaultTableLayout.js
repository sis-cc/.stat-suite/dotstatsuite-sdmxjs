/**
 * @description
 * A function that takes a dataflow structure and returns the default Table Layout
 * props. See SDMXTable in 'dotstatsuite-ui-components'. This layout is taken from
 * specific dataflow level annotations.
 *
 * @public
 * @name getDefaultTableLayout
 * @param {Object}
 * @return {Object}
 *
 * @example
 *
 * const structure = {
 *   data: { dataflows: [{
 *     annotations: [
 *       { title: 'DIM1,DIM3', type: 'LAYOUT_ROW'},
 *       { title: 'DIM2', type: 'LAYOUT_COLUMN'},
 *       { title: 'DIM5', type: 'LAYOUT_ROW_SECTION' },
 *       { title: 'DIM4', type: 'LAYOUT_ROW'},
 *     ]
 *   }] }
 * };
 *
 * SDMXJS.getDefaultTableLayout(structure);
 * //=>{
 * //    header: ['DIM2'],
 * //    rows: ['DIM1', 'DIM3', 'DIM4'],
 * //    sections: ['DIM5']
 * //  }
 */
import * as R from 'ramda';
import {
  DATAFLOW_ANNOTATIONS_PATH,
  LAYOUT_ANNOTATIONS_TYPES,
  LAYOUT_KEYS,
} from './constants/annotations';

export const getDefaultTableLayout = (structure) =>
  R.pipe(
    R.pathOr([], DATAFLOW_ANNOTATIONS_PATH),
    R.reduce((acc, annotation) => {
      if (!R.includes(annotation.type, LAYOUT_ANNOTATIONS_TYPES)) {
        return acc;
      }
      const ids = R.pipe(R.propOr('', 'title'), R.split(','))(annotation);

      const layoutKey = R.prop(annotation.type, LAYOUT_KEYS);
      const currentIds = R.propOr([], layoutKey, acc);

      return {
        ...acc,
        [layoutKey]: [...currentIds, ...ids],
      };
    }, {}),
  )(structure);
