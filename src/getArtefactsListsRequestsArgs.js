/**
 * @description
 * An utilitary function that given a set of selected SDMX datasources,
 * artefact types, categories, agencies and versions, will return the list
 * of request options (see getRequestArgs) to be applied in order to retrieve
 * the all sets of artefacts matching this selection.
 *
 * @name getArtefactsListsRequestsArgs
 * @public
 * @param {Object}
 * @return {Array}
 *
 * @example
 * const selection = {
 *   spaces: { ... },
 *   agencies: { ... },
 *   categories: { ... },
 *   types: [],
 *   versions: []
 * };
 *
 * const requestsArgs = SDMXJS.getArtefactsListsRequestsArgs(selection);
 *
 * R.forEach(
 *   ({ url, header, params }) => {
 *     axios.get(url, { headers, params }).then( ... );
 *   },
 *   requestsArgs
 * )
 */
import * as R from 'ramda';
import { getArtefactsListRequestArgs } from './getArtefactsListRequestArgs';
import { ALL } from './constants/types';
import { cartesian } from './utils';

const agencyCodeParser = (agency) =>
  R.startsWith('SDMX', R.prop('id', agency))
    ? R.prop('code', agency)
    : R.prop('id', agency);

export const getArtefactsListsRequestsArgs = ({
  agencies,
  categories,
  spaces,
  locale,
  types,
  versions,
  ...rest
}) => {
  const nothingIsAll = R.when(R.isEmpty, R.always([ALL]));
  const nothingIsNothing = R.when(R.isEmpty, R.always([null]));
  const selection = R.evolve(
    {
      agencyIds: R.pipe(R.values, R.map(agencyCodeParser)),
      types: nothingIsAll,
      categories: R.values,
      spaces: R.pipe(
        R.values,
        R.map((sp) => ({ ...sp, url: sp.endpoint })),
      ),
    },
    { spaces, agencyIds: agencies, categories, types, versions },
  );

  return R.pipe(
    R.map((space) => {
      const categories = R.pipe(
        R.filter(
          R.pipe(R.prop('spaceId'), R.anyPass([R.equals(space.id), R.isNil])),
        ),
        nothingIsNothing,
      )(selection.categories);

      return R.pipe(R.map(R.prepend(space)))(
        cartesian([
          categories,
          selection.types,
          nothingIsAll(selection.agencyIds),
          selection.versions,
        ]),
      );
    }),
    R.unnest,
    R.map(([datasource, category, type, agencyId, version]) =>
      getArtefactsListRequestArgs({
        agencyIds: selection.agencyIds,
        datasource,
        category,
        type,
        agencyId,
        version,
        locale,
        ...rest,
      }),
    ),
  )(selection.spaces);
};
