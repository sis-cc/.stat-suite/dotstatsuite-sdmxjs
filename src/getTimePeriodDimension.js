/**
 * @description
 * A function that takes a list of parsed dimensions and returns the Time Period one or `undefined` if none.
 * This methods rely on `isTimePeriodDimension` method, check it to know the rules applied for retrieval.
 *
 * @public
 * @name getTimePeriodDimension
 * @param {Object}
 * @return {Object}
 *
 * @example
 * const dimensions = [
 *   { id: 'INDICATOR', role: 'SUBJECT', values: [{ id: 'IND1' }, { id: 'IND2' }] },
 *   { id: 'COU', role: 'GEO', values: [{ id: 'GER' }, { id: 'FRA' }] },
 *   { id: 'F', role: 'FREQ', values: [{ id: 'A' }] },
 *   { id: 'TIME_PERIOD', role: null, values: [{ id: '2020' }, { id: '2021' }] },
 * ];
 * SDMXJS.getTimePeriodDimension({ dimensions }); //=> { id: 'TIME_PERIOD', role: null, values: [{ id: '2020' }, { id: '2021' }] }
 */
import * as R from 'ramda';
import { isTimePeriodDimension } from './isTimePeriodDimension';

export const getTimePeriodDimension = ({ dimensions }) =>
  R.findLast(isTimePeriodDimension, dimensions);
