/**
 * @description
 * A curried function that takes a set of dimensions and a current SDMX dataquery, and given
 * an object of dimension id as key and valueIds as selection, returns the updated dataquery.
 *
 * valueIds works like toggle, if the value is already inside the dataquery then it will be removed
 *
 * @public
 * @param {Array}
 * @param {string}
 * @param {string}
 * @param {string}
 * @name updateDataquery
 * @return {string}
 *
 * @example
 * const dimensions = [
 *   { id: 'ALPHA', values: [] },
 *   { id: 'GAMMA', values: [] },
 *   { id: 'PHI', values: [{ id: 'PHI_1' }, { id: 'PHI_2' }] },
 *   { id: 'EPSILON', values: [] },
 *   { id: 'TAU', values: [{ id: 'TAU_1' }, { id: 'TAU_2' }, { id: 'TAU_3' }]  },
 * ];
 *
 * const dataquery = '..PHI_1..TAU_1+TAU_3';
 *
 * SDMXJS.updateDataquery(dimensions, dataquery)({'PHI': ['PHI_2'], 'FOO': ['BAR'], 'TAU': ['TAU_3'] });
 * //=> '..PHI_1+PHI_2..TAU_1'
 */

import * as R from 'ramda';
import {
  DATAQUERY_VALUES_SEPARATOR,
  DATAQUERY_DIMENSIONS_SEPARATOR,
} from './constants/url';
import { isFrequency } from './utils';

const getJoinValuesIds =
  ({ current, valIds }) =>
  (dimension) => {
    if (R.isEmpty(valIds)) return '';
    const dimensionIndexedByValueId = R.pipe(
      R.propOr([], 'values'),
      R.indexBy(R.prop('id')),
    )(dimension);
    const getSanitizeValidIds = R.filter(
      R.flip(R.has)(dimensionIndexedByValueId),
    );
    return R.pipe(
      R.when(R.pipe(R.isNil, R.not), R.split(DATAQUERY_VALUES_SEPARATOR)),
      R.reject(R.isEmpty),
      getSanitizeValidIds,
      R.symmetricDifference(getSanitizeValidIds(valIds)),
      R.join(DATAQUERY_VALUES_SEPARATOR),
    )(current);
  };

const updateDataquery = (dimensions, dataquery, valIdsIndexByDimId) => {
  const splitDataquery = R.split(DATAQUERY_DIMENSIONS_SEPARATOR, dataquery);

  return R.pipe(
    R.addIndex(R.map)((dim, index) => {
      const current = R.nth(index, splitDataquery);
      const dimId = R.prop('id', dim);
      const valIds = R.prop(dimId)(valIdsIndexByDimId);
      if (isFrequency(dim)) {
        if (R.either(R.isNil, R.isEmpty)(valIds)) return current;
        return R.ifElse(
          R.pipe(R.prop('values'), R.find(R.propEq(R.head(valIds), 'id'))), // frequency valIds take only the first one
          R.always(R.head(valIds)), // for frequency, if valid new value, replace current
          R.always(current), // else keep current, not erased, no matter what
        )(dim);
      }
      if (R.either(R.isNil, R.isEmpty)(valIdsIndexByDimId)) return ''; // if not frequency and undefined or empty valIdsIndexByDimId, then erase
      return R.cond([
        [
          R.always(R.has(dimId, valIdsIndexByDimId)), // matching dimId
          getJoinValuesIds({ current, valIds }),
        ],
        [R.T, R.always(current)], // keep current
      ])(dim);
    }),
    R.join(DATAQUERY_DIMENSIONS_SEPARATOR),
  )(dimensions);
};

export default R.curry(updateDataquery);
