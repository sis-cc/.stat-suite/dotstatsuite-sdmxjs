/**
 * @description
 * A function that takes lists of parsed dimensions and parsed attributes, and returns the Frequency artefact or `undefined` if none.
 * If frequency can be found in both dimenions and attributes, the dimension one will be returned. This methods rely on `isFrequencyDimension`
 * and `isFrequencyAttribute` methods, check them to know the rules applied for retrieval. Since it is possible for an attribute to not have
 * any values, any attribute without values will be ignored.
 * If frequency attribute is returned, `isAttribute` set to `true` property will be added to the object, and all values will have their `id`
 * changed to the corresponding standard SDMX dimension value `id`, here are the following corresponding values:
 *
 * Attribute Value Id -> Dimension Value Id
 * `P1Y` -> `A`
 * `P6M` -> `S`
 * `P3M` -> `Q`
 * `P1M` -> `M`
 * `P7D` -> `W`
 * `P1D` -> `D`
 * `PT1M` -> `N`
 *
 * @public
 * @name getFrequencyArtefact
 * @param {Object}
 * @return {Object}
 *
 * @example
 * const dimensions1 = [
 *   { id: 'INDICATOR', role: 'SUBJECT', values: [{ id: 'IND1' }, { id: 'IND2' }] },
 *   { id: 'COU', role: 'GEO', values: [{ id: 'GER' }, { id: 'FRA' }] },
 *   { id: 'TIME_PERIOD', role: null, values: [{ id: '2020' }, { id: '2021' }] },
 * ];
 * const dimensions2 = [
 *   { id: 'INDICATOR', role: 'SUBJECT', values: [{ id: 'IND1' }, { id: 'IND2' }] },
 *   { id: 'COU', role: 'GEO', values: [{ id: 'GER' }, { id: 'FRA' }] },
 *   { id: 'F', role: 'FREQ', values: [{ id: 'A' }] },
 *   { id: 'TIME_PERIOD', role: null, values: [{ id: '2020' }, { id: '2021' }] },
 * ];
 * const attributes = [
 *   { id: 'UNIT_MEASURE', values: [] },
 *   { id: 'TIME_FORMAT', values: [{ id: 'P1Y' }] }
 * ];
 * SDMXJS.getFrequencyArtefact({ dimensions: dimensions1, attributes }); //=> { id: 'TIME_FORMAT', isAttribute: true, values: [{ id: 'A' }] }
 * SDMXJS.getFrequencyArtefact({ dimensions: dimensions2, attributes: [] }); //=> { id: 'F', role: 'FREQ', values: [{ id: 'A' }] }
 */
import * as R from 'ramda';
import { isFrequencyDimension } from './isFrequencyDimension';
import { isFrequencyAttribute } from './isFrequencyAttribute';
import { SUPPORTED_FORMAT } from './constants/frequency';

export const getFrequencyArtefact = ({ dimensions, attributes }) => {
  const frequencyDimension = R.find(isFrequencyDimension, dimensions);
  if (!R.isNil(frequencyDimension)) {
    return frequencyDimension;
  }
  const frequencyAttribute = R.find(isFrequencyAttribute, attributes);
  if (
    R.isNil(frequencyAttribute) ||
    R.pipe(R.propOr([], 'values'), R.isEmpty)(frequencyAttribute)
  ) {
    return undefined;
  }

  const timeFormatAttribute = R.pipe(
    R.over(
      R.lensProp('values'),
      R.reduce((acc, valueItem) => {
        const id = R.prop(valueItem.id, SUPPORTED_FORMAT);
        if (R.isNil(id)) return acc;
        return R.append(R.assoc('id', id, valueItem), acc);
      }, []),
    ),
    R.dissoc('index'),
    R.assoc('isAttribute', true),
  )(frequencyAttribute);

  if (R.isEmpty(timeFormatAttribute.values)) return undefined;
  return timeFormatAttribute;
};
