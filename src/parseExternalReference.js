/**
 * @description
 * A function that given an artefact and the according type, will retrieve and parse the external reference
 * of the artefact
 *
 * @public
 * @name parseExternalReference
 * @param {Object}
 * @param {string}
 * @return {Object}
 *
 * @example
 * const dataflow = {
 *   id: 'MY_DF',
 *   agencyID: 'AG',
 *   version: '5.0',
 *   isExternalReference: true,
 *   links: [
 *     { rel: 'external', href: 'endpoint/dataflow/agencyId/code/version' },
 *   ],
 * };
 *
 * SDMXJS.parseExternalReference(dataflow, 'dataflow');
 * //=>  {
 * //      datasource: { url: 'endpoint' },
 * //      identifiers: { agencyId: 'agencyId', code: 'code', version: 'version' }
 * //    };
 */
import * as R from 'ramda';

export const parseExternalReference = (artefact, type) => {
  if (R.not(R.prop('isExternalReference', artefact))) {
    return null;
  }
  const externalRef = R.pipe(
    R.propOr([], 'links'),
    R.find(R.propEq('external', 'rel')),
  )(artefact);
  if (R.isNil(externalRef)) {
    return null;
  }
  const href = externalRef.href;
  const [endpoint, identifiers] = R.split(`/${type}/`, href);
  if (R.any(R.isNil, [endpoint, identifiers])) {
    return null;
  }
  const [agencyId, code, version] = R.split('/', identifiers);
  if (R.any(R.isNil, [agencyId, code, version])) {
    return null;
  }
  return {
    datasource: { url: endpoint },
    identifiers: { agencyId, code, version },
  };
};
