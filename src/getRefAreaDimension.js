/**
 * @description
 * A function that takes a list of parsed dimensions and returns the Reference Area one or `undefined` if none.
 * This methods rely on `isRefAreaDimension` method, check it to know the rules applied for retrieval.
 *
 * @public
 * @name getRefAreaDimension
 * @param {Object}
 * @return {Object}
 *
 * @example
 * const dimensions = [
 *   { id: 'INDICATOR', role: 'SUBJECT', values: [{ id: 'IND1' }, { id: 'IND2' }] },
 *   { id: 'COU', role: 'GEO', values: [{ id: 'GER' }, { id: 'FRA' }] },
 *   { id: 'F', role: 'FREQ', values: [{ id: 'A' }] },
 *   { id: 'TIME_PERIOD', role: null, values: [{ id: '2020' }, { id: '2021' }] },
 * ];
 * SDMXJS.getRefAreaDimension({ dimensions }); //=> { id: 'COU', role: 'GEO', values: [{ id: 'GER' }, { id: 'FRA' }] }
 */
import * as R from 'ramda';
import { isRefAreaDimension } from './isRefAreaDimension';

export const getRefAreaDimension = ({ dimensions }) =>
  R.find(isRefAreaDimension, dimensions);
