/*
 * A function that takes a set of arguments (see getRequestArgs), and returns the corresponding sdmx format request.
 * The default sdmx formats can be override by custom ones in datasource 'headers' entry.
 * If 'format' not specified, 'json' default will be returned. 'labels' entry only taken in consideration for data csv.
 *
 * @private
 * @param {Object}
 * @return {Object}
 *
 * @example
 * getSdmxFormat({ type: 'data' }); //=> 'application/vnd.sdmx.data+json;version=1.0.0-wd'
 * getSdmxFormat({ type: 'data', format: 'csv' }); //=> 'application/vnd.sdmx.data+csv'
 * getSdmxFormat({ type: 'data', format: 'csv', asFile: true }); //=> 'application/vnd.sdmx.data+csv;file=true'
 * getSdmxFormat({ type: 'data', format: 'csv', asFile: true, labels: 'code' }); //=> 'application/vnd.sdmx.data+csv;file=true;labels=code'
 * getSdmxFormat({ type: 'dataflow' }); //=> 'application/vnd.sdmx.structure+json;version=1.0;urn=true'
 * getSdmxFormat({ type: 'dataflow', datasource: { headers: { structure: { json: 'customSdmxFormat' } } } }); //=> 'customSdmxFormat'
 */
import * as R from 'ramda';
import {
  DEFAULT_FORMATS,
  FILE_FORMAT,
  ZIP_FORMAT,
  URN_FORMAT,
} from './constants/headers';

export const getSdmxFormat = ({
  asFile,
  asZip,
  datasource,
  format,
  labels,
  type,
  withUrn = true,
}) => {
  const formats = R.pipe(
    R.propOr({}, 'headers'),
    R.mergeDeepRight(DEFAULT_FORMATS),
  )(datasource);
  const getFormat = R.ifElse(R.has(format), R.prop(format), R.prop('json'));

  const typeFormats = R.ifElse(
    R.equals('data'),
    R.identity,
    R.always('structure'),
  )(type);
  const defaultFormat = getFormat(R.prop(typeFormats, formats));

  const sdmxFormat = R.pathOr(
    defaultFormat,
    ['headers', typeFormats, format],
    datasource,
  );

  return R.pipe(
    R.when(R.always(!!withUrn), R.append(URN_FORMAT)),
    R.when(R.always(!!asFile), R.append(FILE_FORMAT)),
    R.when(R.always(!!asZip), R.append(ZIP_FORMAT)),
    R.when(
      R.always(typeFormats === 'data' && format === 'csv'),
      R.append(R.defaultTo('labels=code', labels)),
    ),
    R.join(';'),
  )([sdmxFormat]);
};
