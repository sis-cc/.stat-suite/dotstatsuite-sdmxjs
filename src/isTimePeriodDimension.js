/**
 * @description
 * A function that takes a parsed dimension and returns if it corresponds to the Frequency Dimension or not.
 * The dimension can be recognised by having `TIME_PERIOD` as `role`, or by having `TIME_PERIOD`, `TIME` or `YEAR` as `id`.
 *
 * @public
 * @name isTimePeriodDimension
 * @param {Object}
 * @return {Boolean}
 *
 * @example
 * SDMXJS.isTimePeriodDimension({ id: 'DIM', role: 'TIME_PERIOD', values: [] }); //=> true
 * SDMXJS.isTimePeriodDimension({ id: 'YEAR', role: null, values: [] }); //=> false
 * SDMXJS.isTimePeriodDimension({ id: 'INDICATOR', role: 'SUBJECT', values: [] }); //=> false
 */
import * as R from 'ramda';
import { TIME_PERIOD_ROLE, TIME_PERIOD_ID } from './constants/dimensions';

export const isTimePeriodDimension = R.anyPass([
  R.pipe(R.prop('role'), R.equals(TIME_PERIOD_ROLE)),
  R.propEq(TIME_PERIOD_ID, 'id'),
]);
