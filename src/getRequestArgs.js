/**
 * @description
 * A function that given a set of arguments, will return all
 * sdmx request options.
 *
 * The function will return the url, headers and params options
 *
 * @public
 * @name getRequestArgs
 * @param {Object}
 * @return {Object}
 *
 * @example
 * const args = {
 *   asFile: false,
 *   dataquery: '...',
 *   datasource: {
 *     url: 'http://mySDMXWebServiceEndpoint',
 *     hasRangeHeader: false,
 *     supportsReferencePartial: false,
 *   },
 *   format: undefined, // default is 'json'
 *   identifiers: { agencyId: 'OECD', code: 'DF', version: '1.0' },
 *   labels: undefined, // default 'code', ignored if not 'csv' format
 *   locale: 'en',
 *   params: {}, // can contain startPeriod, endPeriod or lastNObservations params
 *   range: undefined,
 *   type: 'data', // or artefact type for structure request, i.e. 'dataflow'
 * };
 *
 * SDMXJS.getRequestArgs(args);
 * //=> {
 * //=>   url: 'http://mySDMXWebServiceEndpoint/data/OECD,DF,1.0/all',
 * //=>   headers: {
 * //=>     Accept: 'application/vnd.sdmx.data+json;version=1.0.0-wd',
 * //=>     Accept-Language: 'en',
 * //=>   },
 * //=>   params: { dimensionAtObservation: 'AllDimensions' }
 * //=> }
 */
import * as R from 'ramda';
import { getDataParams } from './getDataParams';
import { getDataRequestQuery } from './getDataRequestQuery';
import { getRequestHeaders } from './getRequestHeaders';
import { getStructureParams } from './getStructureParams';
import { getStructureRequestQuery } from './getStructureRequestQuery';
import { isDataRequest } from './isDataRequest';

const getRequestQuery = ({ type, identifiers, dataquery }) =>
  R.ifElse(
    isDataRequest,
    getDataRequestQuery,
    getStructureRequestQuery,
  )({
    type,
    identifiers,
    dataquery,
  });

export const getRequestArgs = (args) => {
  const datasourceUrl = R.path(['datasource', 'url'], args);
  const url = `${datasourceUrl}/${getRequestQuery(args)}`;

  const headers = getRequestHeaders(args);

  const params = R.ifElse(
    isDataRequest,
    getDataParams,
    getStructureParams,
  )(args);

  return { url, headers, params };
};
