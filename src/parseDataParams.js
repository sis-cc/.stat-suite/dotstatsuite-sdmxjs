/*
 * A function that takes a set of params and filter them regarding
 * sdmx-data request compliancy.
 *
 * disclaimer: No parsing of period values neither 'dimensionAtObservation'.
 *
 * @private
 * @param {Object}
 * @return {Object}
 *
 *@example
 * const params = {
 *   random: 'random',
 *   dimensionAtObservation: 'AllDimensions',
 *   startPeriod: 'period',
 *   lastNObservations: '0'
 * };
 *
 * SDMXJS.parseDataParams(params);
 * //=>{
 * //    dimensionAtObservation: 'AllDimensions',
 * //    startPeriod: 'period'
 * //  }
 */
import * as R from 'ramda';
import {
  START_PERIOD_PARAM_KEY,
  END_PERIOD_PARAM_KEY,
  LASTNOBS_PARAM_KEY,
  DIMOBS_PARAM_KEY,
  LAST_N_PERIODS_PARAM_KEY,
} from './constants/params';

export const parseDataParams = (params) =>
  R.pipe(
    R.pick([
      START_PERIOD_PARAM_KEY,
      END_PERIOD_PARAM_KEY,
      LASTNOBS_PARAM_KEY,
      DIMOBS_PARAM_KEY,
      LAST_N_PERIODS_PARAM_KEY,
    ]),
    R.when(
      R.pipe(R.prop(LASTNOBS_PARAM_KEY), Number, R.equals(0)),
      R.dissoc(LASTNOBS_PARAM_KEY),
    ),
    R.when(
      R.pipe(R.prop(LAST_N_PERIODS_PARAM_KEY), Number, R.equals(0)),
      R.dissoc(LAST_N_PERIODS_PARAM_KEY),
    ),
  )(params);
