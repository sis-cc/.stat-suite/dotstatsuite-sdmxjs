/**
 * @description
 * A function that given a set of arguments, will return all
 * sdmx request header options.
 *
 * The function will return the url, headers and params options
 *
 * @public
 * @name getRequestHeaders
 * @param {Object}
 * @return {Object}
 *
 * @example
 * const args = {
 *   datasource: {
 *     hasRangeHeader: true,
 *   },
 *   format: undefined, // default is 'json'
 *   asFile: false,
 *   labels: undefined, // default 'code', ignored if not 'csv' format
 *   locale: 'en',
 *   range: 500,
 *   type: 'data', // or artefact type for structure request, i.e. 'dataflow'
 * };
 *
 * SDMXJS.getRequestHeaders(args);
 * //=> {
 * //=>   Accept: 'application/vnd.sdmx.data+json;version=1.0.0-wd',
 * //=>   Accept-Language: 'en',
 * //=>   x-range: 'values=0-499'
 * //=> }
 */
import * as R from 'ramda';
import { getSdmxFormat } from './getSdmxFormat';
import { getRangeHeader } from './getRangeHeader';

export const getRequestHeaders = ({
  datasource,
  type,
  locale,
  range,
  ...rest
}) =>
  R.pipe(
    R.assoc('Accept', getSdmxFormat({ datasource, type, ...rest })),
    (headers) => {
      const h = getRangeHeader({ type, datasource, range });
      return R.isEmpty(h)
        ? headers
        : R.assoc('x-range', R.prop('x-range', h), headers);
    },
  )({ 'Accept-Language': locale });
