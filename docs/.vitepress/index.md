---
layout: home

hero:
  name: '@sis-cc/dotstatsuite-sdmxjs'
  text: SDMX library for JavaScript
  tagline: A library that collects sdmx parsers with proper unit tests and documentation.
  image:
    src: assets/logo.png
    alt: sdmxjs
  actions:
    - theme: brand
      text: What is SDMX?
      link: https://sdmx.org
    - theme: alt
      text: View on NPM
      link: https://www.npmjs.com/package/@sis-cc/dotstatsuite-sdmxjs
    - theme: alt
      text: View on GitLab
      link: https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-sdmxjs

features:
  - icon:
      src: assets/vite.svg
    title: Powered by Vite
    details: Vite is a modern build tool for frontend JavaScript applications, designed to provide fast development and optimized production builds.
  - icon:
      src: assets/rollup.svg
    title: Bundled with Rollup
    details: Rollup is a JavaScript module bundler, commonly used for bundling libraries or applications with a focus on efficient, optimized output.
  - icon:
      src: assets/vitest.svg
    title: Tested with Vitest
    details: Vitest is a modern testing framework specifically designed to work seamlessly with the Vite build tool, although it can be used independently as well.
  - icon:
      src: assets/sdmx.svg
    title: Build for SDMX
    details: SDMX is an international standard used to facilitate the exchange, sharing, and dissemination of statistical data and metadata across organizations.
---