import { defineConfig } from 'vitepress';

export default defineConfig({
  base: '/.stat-suite/dotstatsuite-sdmxjs/',
  title: '@sis-cc/dotstatsuite-sdmxjs',
  description: 'SDMX library for JavaScript',
  ignoreDeadLinks: true,
  outDir: '../public',
  themeConfig: {
    logo: '/assets/logo.png',
    socialLinks: [
      { icon: 'github', link: 'https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-sdmxjs' },
      { icon: 'npm', link: 'https://www.npmjs.com/package/@sis-cc/dotstatsuite-sdmxjs' },
    ],
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Readme', link: 'readme' },
      { text: 'API', link: 'api' },
    ],
    search: {
      provider: 'local',
    },
    footer: {
      message: 'Released under the MIT License.',
      copyright: 'Copyright © OECD 2019-2024',
    },
  },
});
