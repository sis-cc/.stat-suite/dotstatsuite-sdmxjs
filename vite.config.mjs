import { resolve } from 'path';
import { defineConfig } from 'vite';
import commonjs from '@rollup/plugin-commonjs';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import { dependencies } from './package.json';

export default defineConfig({
  build: {
    minify: false,
    copyPublicDir: false,
    lib: {
      entry: resolve(__dirname, 'src/index.js'),
      name: '@sis-cc/dotstatsuite-sdmxjs',
    },
    rollupOptions: {
      external: Object.keys(dependencies),
      output: [
        {
          format: 'es',
          dir: 'dist/esm',
          entryFileNames: '[name].mjs',
          preserveModules: true,
          preserveModulesRoot: 'src',
          exports: 'named',
        },
        {
          format: 'cjs',
          dir: 'dist/cjs',
          entryFileNames: '[name].js',
          preserveModules: true,
          preserveModulesRoot: 'src',
          exports: 'auto',
        },
      ],
      plugins: [nodeResolve(), commonjs()],
    },
    target: 'es2015', // ideal target is esnext
  },
});
